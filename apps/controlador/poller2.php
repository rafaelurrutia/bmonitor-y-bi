<?php 

	class poller2 extends Control {
				
		private function validaPoller($class,$tipo = 'padre',$pidAnterior = null){
				
			$this->pid=getmypid();
			
			$this->tipo=$tipo;
			
			//$limpiaPoller = 'DELETE FROM `bsw_poller` WHERE `fecha_fin` IS NOT NULL AND ( ( tipo="hijo" AND `fecha_fin` IS NOT NULL)  OR  (tipo="padre" AND `fecha_fin` <   DATE_ADD(NOW(), INTERVAL  - 1 MINUTE)));';

			$result = $this->pollerActivo($class,$tipo);

			if($result == false) {
				
				$sql_inicial = 'INSERT INTO `bsw_poller` (`clase`, `pid_padre`, `pid_hijo`, `tipo`, `fecha_inicio`) VALUES';
				
				if($tipo == 'padre') {
					if($result == 0) {
						$insert = $sql_inicial." ('$class', $this->pid, 0, 'padre', NOW());";
						$this->conexion->query($insert);
					} else {
						print "Poller activo";
						exit;
					}
					
				} elseif ($tipo == 'hijo') {
					$insert = $sql_inicial." ('$class', $pidAnterior, $this->pid, 'hijo', NOW());";
					$this->conexion->query($insert);
				} else {
					$this->logs->error("Tipo de poller invalido: ",$tipo,'logs_poller');
				}	
			} else {
				$this->logs->debug("Error al consultar  poller activo",NULL,'logs_poller');
				exit;
			}
		}
		
		private function pollerActivo($class,$type = 'hijo') {
			$select_poller = "SELECT count(*) As Active FROM `bsw_poller` WHERE `tipo`='$type' AND `fecha_fin` IS NULL AND `clase`='$class';";
			$result = $this->conexion->queryFetch($select_poller);
			if($result) {
				return $result[0]['Active'];
			} else {
				return false;
			}
		}
		
		public function index()
		{
			$this->validaPoller("indexPoller");
			
			$LIMIT = $this->parametro->get('GET_MAX_POLLER',10);
			
			$activos = $this->pollerActivo("indexPoller");
			
			if($activos) {
				$LIMIT = $LIMIT - $activos;
			}
			
			if(($LIMIT == 0) || ( $LIMIT < 0)) {
				$this->logs->error("Poller inicial supero el maximo permitido",NULL,'logs_poller');
				exit;
			} else {
				$this->logs->debug("Iniciando poller con un limite de [$activos/$LIMIT] hilos",NULL,'logs_poller');
			}
			
			$sql_host = "SELECT profile.`id_item_profile` , host.`id_host`,  host.`dns`, host.`host` ,host.`ip_wan`,item.`id_item`, item.`description` ,item.`type_item`,config.feriados,
							config.horario,profile.`snmp_community`,profile.`snmp_port`,profile.`snmp_oid`, unix_timestamp(now()) as `utime`
						FROM `bm_host` host
						LEFT JOIN `bm_item_profile` profile USING(`id_host`)
						LEFT JOIN `bm_items` item ON profile.`id_item`=item.`id_item`
						LEFT JOIN `bsw_config` AS config ON config.groupid = host.`groupid`
						WHERE host.`borrado`=0 AND profile.`nextcheck`  < NOW() AND  ( profile.`status`='pendiente' OR profile.`status`='ok' ) AND item.`type_poller` != 'bsw_agent' LIMIT ".$LIMIT;
						
			$result_host = $this->conexion->queryFetch($sql_host);

			$totalHijos = 	count($result_host);
			
			//$base = $this->parametro->get("URL_BASE");
			
			$urlInicial = URL_BASE_FULL.'poller2/process';
			
			if($totalHijos >= 1){
				$this->logs->debug("[POLLER_INDEX] Cargando un total de $totalHijos hilos",NULL,'logs_poller');
				foreach ($result_host as $key => $host) {				
					$post = array();
					$post['hilo'] = $key;
					$post['hiloType'] = 'start';
					$post['pidPadre'] = $this->pid;
					$post['pidType'] = 'indexPoller';
					$post['ip'] = $host['ip_wan'];
					$post['id_host'] = $host['id_host'];
					$post['host'] = $host['host'];
					$post['dns'] = $host['dns'];
					$post['itemid'] = $host['id_item'];
					$post['utime'] = $host['utime'];
					$post['snmp_community'] = $host['snmp_community'];
					$post['snmp_port'] = $host['snmp_port'];
					$post['snmp_oid'] = $host['snmp_oid'];
					$post['type_item'] = $host['type_item'];
					$post['description'] = $host['description'];
					
					$this->curl->cargar($urlInicial,$host['id_item_profile'],$post);	
					$this->logs->debug("[POLLER_INDEX] Hilo numero $key , cargado",NULL,'logs_poller');
				}
	
				$this->curl->ejecutar();
			} else {
				$this->logs->debug("[POLLER_INDEX] No hay host disponibles",NULL,'logs_poller');
				 //$queryReset="update items set lastlogsize=0 where lastlogsize=1 and (error='' or error is null)  and nextcheck < unix_timestamp(now())-60*5";
				 //$this->conexion->query($queryReset);
			}
		}
		
		public function retry()
		{
			$this->validaPoller("indexPoller");
			
			$LIMIT = $this->parametro->get('GET_MAX_POLLER',10);
			
			$activos = $this->pollerActivo("indexPollerRetry");
			
			if($activos) {
				$LIMIT = $LIMIT - $activos;
			}
			
			if(($LIMIT == 0) || ( $LIMIT < 0)) {
				$this->logs->error("Poller inicial supero el maximo permitido",NULL,'logs_poller');
				exit;
			} else {
				$this->logs->debug("Iniciando poller con un limite de [$activos/$LIMIT] hilos",NULL,'logs_poller');
			}
			
			$sql_host = "SELECT profile.`id_item_profile` , host.`id_host`,  host.`dns`, host.`host` ,host.`ip_wan`,item.`id_item`, item.`description` ,item.`type_item`,config.feriados,
							config.horario,profile.`snmp_community`,profile.`snmp_port`,profile.`snmp_oid`, unix_timestamp(now()) as `utime`
						FROM `bm_host` host
						LEFT JOIN `bm_item_profile` profile USING(`id_host`)
						LEFT JOIN `bm_items` item ON profile.`id_item`=item.`id_item`
						LEFT JOIN `bsw_config` AS config ON config.groupid = host.`groupid`
						WHERE host.`borrado`=0 AND profile.`nextcheck`  < NOW() AND ( profile.`status`='retry' OR profile.`status`='error' )  AND item.`type_poller` != 'bsw_agent' LIMIT ".$LIMIT;
						
			$result_host = $this->conexion->queryFetch($sql_host);

			$totalHijos = 	count($result_host);
			
			//$base = $this->parametro->get("URL_BASE");
			
			$urlInicial = URL_BASE_FULL.'poller2/process';
			
			if($totalHijos >= 1){
				$this->logs->debug("Cargando un total de $totalHijos hilos",NULL,'logs_poller');
				foreach ($result_host as $key => $host) {				
					$post = array();
					$post['hilo'] = $key;
					$post['hiloType'] = 'retry';
					$post['pidPadre'] = $this->pid;
					$post['pidType'] = 'indexPollerRetry';
					$post['ip'] = $host['ip_wan'];
					$post['id_host'] = $host['id_host'];
					$post['host'] = $host['host'];
					$post['dns'] = $host['dns'];
					$post['itemid'] = $host['id_item'];
					$post['utime'] = $host['utime'];
					$post['snmp_community'] = $host['snmp_community'];
					$post['snmp_port'] = $host['snmp_port'];
					$post['snmp_oid'] = $host['snmp_oid'];
					$post['type_item'] = $host['type_item'];
					$post['description'] = $host['description'];
					
					$this->curl->cargar($urlInicial,$host['id_item_profile'],$post);	
					$this->logs->debug("Hilo numero $key , cargado",NULL,'logs_poller');
				}
	
				$this->curl->ejecutar();
			} else {
				$this->logs->debug("No hay host disponibles",NULL,'logs_poller');
				 //$queryReset="update items set lastlogsize=0 where lastlogsize=1 and (error='' or error is null)  and nextcheck < unix_timestamp(now())-60*5";
				 //$this->conexion->query($queryReset);
			}
		}
		
		public function process(){
				
			$getParam = (object)$_POST;
			
			if($getParam->hiloType == 'start') {
				$poler_log_text = '[POLLER_INDEX]';
			} else {
				$poler_log_text = '[POLLER_RETRY]';
			}

			$this->logs->debug("$poler_log_text Hilo numero $getParam->hilo , activo",NULL,'logs_poller');
			
			$this->validaPoller($getParam->pidType,'hijo',$getParam->pidPadre);
					
			$ip = $getParam->ip.":".$getParam->snmp_port;
	
			switch ($getParam->type_item) {
				case 'float':
					$tablehistory="bm_history";
				break;
				
				case 'string':
					$tablehistory="bm_history_str";
				break;
				
				case 'text':
					$tablehistory="bm_history_text";
				break;
				
				case 'log':
					$tablehistory="bm_history_uint";
				break;
								
				default:
					$tablehistory="bm_history";
				break;
			}
			
			//Cargando parametros
			
			$timeout = $this->parametro->get("SNMP_TIMEOUT",1000000);
			$retry = $this->parametro->get("SNMP_RETRY",3);
			$version = $this->parametro->get("SNMP_VERSION",'2c');
			
			//$query="update items set nextcheck=" . $getParam->utime . "+delay,lastlogsize=1 where itemid=" . $getParam->itemid;
			//$this->conexion->query($query);
			
		
			
			//Validamos si el modulo o paquete php_snmp se encuentra activo
			if (function_exists('snmp2_get')) {
				//Via modulo Inicio
				$result = @snmp2_get($ip,$getParam->snmp_community,$getParam->snmp_oid,$timeout,$retry);
				if($result){
					$this->logs->debug("La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  , respondio: ",$result,'logs_poller');
					list($tipoValorSnmp, $valorSnmp) = explode(": ", $result);
					
					//validaciones 
					
					if(($tipoValorSnmp == 'STRING') && (!preg_match('/"/i',$valorSnmp)) ) {
						$valorSnmp = '"'.$valorSnmp.'"';
					}
					
					$filtroObligatorio = false;
					if(($tipoValorSnmp == 'STRING') && ($getParam->type_item == 'float') ) {
						$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string $valorSnmp", NULL,'logs_poller');
						$filtroObligatorio = true;	
					}
					
					$this->logs->debug("$poler_log_text Buscando filtro especial", NULL,'logs_poller');
					
					$filterStatus = $this->filter($getParam->description,$getParam->dns,$valorSnmp);
					
					if($filterStatus) {
						$valorSnmp = $this->snmpFilterValue;
					}
									
					if(($filterStatus == false) && ($filtroObligatorio)) {
						$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string y no tiene filtro", NULL,'logs_poller');
						exit;
					}
					
					$query="/* BSW - Poller OK */ INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->itemid,$getParam->id_host,$getParam->utime, $valorSnmp ,1)";
					$this->conexion->query($query);
					
					if(empty($getParam->lastvalue)) {
						$getParam->lastvalue = "0";
					}
					
					if(is_numeric($valorSnmp)) {
						$valorSnmp = 'abs('.$valorSnmp.')';
					}
				
					$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  `status`='ok' ,`nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND) , `lastclock` =" . $getParam->utime  . ",prevvalue=lastvalue,lastvalue=" . $valorSnmp . " where id_item=". $getParam->itemid." AND `id_host`=$getParam->id_host";
					$result = $this->conexion->query($update_query);
				} else {
					$this->logs->error("$poler_log_text Timeout al consultar via snmp al host: $getParam->host ",NULL,'logs_poller');
					//Notificando error
					if($getParam->hiloType == 'start') {
						$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='retry' ,`nextcheck` = NOW() , `lastclock` =" . $getParam->utime  . ",prevvalue=lastvalue,lastvalue=0 where id_item=". $getParam->itemid." AND `id_host`=$getParam->id_host";
					} else {
						$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='error' ,`nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND)  , `lastclock` =" . $getParam->utime  . ",prevvalue=lastvalue,lastvalue=0 where id_item=". $getParam->itemid." AND `id_host`=$getParam->id_host";
						
						$insert_query="/* BSW - Poller NOK */  INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->itemid,$getParam->id_host,$getParam->utime, 0 ,0)";
						$this->conexion->query($insert_query);
					}
					
					$this->conexion->query($update_query);	
								
				}
				//Via modulo Fin
			} else {
				//Via comando Inicio
				$this->logs->error("El modulo snmp no existe por lo cual se intenta via comando",NULL,'logs_poller');
				$retval = shell_exec("whereis snmpget | wc -w");
				if($retval == 1) {
					$this->logs->error("Error el comando snmpget no existe",NULL,'logs_poller');
					exit;
				}
				$snmpget_cmd="snmpget -t10 -Oqv -v$version -c$getParam->snmp_community $ip $getParam->snmp_oid";
				$snmpget=shell_exec($snmpget_cmd);
				$this->logs->info("El comando snmp: ",$snmpget_cmd ." Respondio: ". $snmpget,'logs_poller');
				//Via comando Fin
			}
		}
		
		private function filter($description, $dns, $valor)
		{
			switch ($description) {
				case 'Availability.bsw':
					if(preg_match("/$dns/i",$valor)){
						$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
						$this->snmpFilterValue = '1';
						return true;
					} else {
						$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
						$this->snmpFilterValue = '0';
						return true;
					}
				break;

				case 'Availability-ForTrap.bsw':
					if(preg_match("/$dns/i",$valor)){
						$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
						$this->snmpFilterValue = '1';
						return true;
					} else {
						$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
						$this->snmpFilterValue = '0';
						return true;
					}
				break;
					
				default:
					return false;
				break;
			}	
		}
		
		function __destruct() {
			if($this->tipo == 'padre'){
				$campo = 'pid_padre';
			} else {
				$campo = 'pid_hijo';
			}
			$update = " UPDATE `bsw_poller` SET `fecha_fin` = NOW() WHERE `$campo` = '$this->pid'";
			$result_update = $this->conexion->query($update);
			if(!$result_update) {
				$this->logs->error("Error al actualizar estado del poller con el pid",$this->pid,'logs_poller');
			}
			$limpiaPoller = 'DELETE FROM `bsw_poller` WHERE `fecha_fin` IS NOT NULL;';
			$this->conexion->query($limpiaPoller);
   		}
	}
?>