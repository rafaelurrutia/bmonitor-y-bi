<?php
class config extends Control {
	
	public function getPlanOption($type = "select",$groupid = false)
	{
		$result = array();
		
		if($groupid) {
			$groupid_value = $groupid;
		} else {
			$groupid_value = $_POST['groupid'];
		}
		
		if(is_numeric($groupid_value)) {
			
			$select_plan = "SELECT `bm_plan`.`id_plan`, `bm_plan`.`plan` 
								FROM `bm_plan` JOIN `bm_plan_groups` USING(`id_plan`) 
									WHERE `bm_plan_groups`.`groupid`=".$groupid_value." AND `bm_plan_groups`.`borrado` = 0";
			$planes_array = $this->conexion->queryFetch($select_plan);
			
			if($planes_array) {
				$option = $this->basic->getOption($planes_array, 'id_plan', 'plan',$type);
				$result['datos'] = $option;
				$result['status'] = true;
			} else {
				$result['status'] = false;
			}
		} else {
			$result['status'] = false;
		}
		
		if($groupid) {
			return $option;
		} else {
			echo json_encode($result);
		}
	}

	public function getProvinciaOption($type = "select",$id = false)
	{
		$result = array();
		
		if($id) {
			$value = $id;
		} else {
			$value = $_POST['id'];
		}
		
		if(is_numeric($value)) {
			
			$select_sql = "SELECT `PROVINCIA_ID`,`PROVINCIA_NOMBRE` FROM `bm_provincia` WHERE `PROVINCIA_REGION_ID`=".$value;
			
			$select_array = $this->conexion->queryFetch($select_sql);
			
			if($select_array) {
				$option = $this->basic->getOption($select_array, 'PROVINCIA_ID', 'PROVINCIA_NOMBRE',$type);
				$result['datos'] = $option;
				$result['status'] = true;
			} else {
				$result['status'] = false;
			}
		} else {
			$option = '<option selected value="0">Error</option>';
			$result['status'] = false;
		}
		
		if($id) {
			return $option;
		} else {
			echo json_encode($result);
		}
	}

	public function getComunaOption($type = "select",$id = false)
	{
		$result = array();
		
		if($id != false) {
			$value = $id;
		} else {
			$value = $_POST['id'];
		}
		
		if(is_numeric($value)) {
			
			$select_sql = "SELECT `COMUNA_ID`,`COMUNA_NOMBRE` FROM `bm_comuna` WHERE `COMUNA_PROVINCIA_ID`=".$value;
			
			$select_array = $this->conexion->queryFetch($select_sql);
			
			if($select_array) {
				$option = $this->basic->getOption($select_array, 'COMUNA_ID', 'COMUNA_NOMBRE',$type);
				$result['datos'] = $option;
				$result['status'] = true;
			} else {
				$option = '<option selected value="0">Error</option>';
				$result['status'] = false;
			}
		} else {
			$result['status'] = false;
		}
		
		if($id) {
			return $option;
		} else {
			echo json_encode($result);
		}
	}
		
	public function cfgSondas()
	{

		$grupos = $this->bmonitor->getGroupsHost();
		
		$value = array();

		$value["option_group"] = $this->basic->getOption($grupos,'groupid','name','select');
			
		$value["input_config"] = $this->getConfigSonda();
		$this->plantilla->load('configuracion/form_new_sonda');
		
		$this->plantilla->set($value);
		$value["form_new_sonda"]  = $this->plantilla->get();
		
		$this->plantilla->load('configuracion/config_equipos_trigger');
		$value["form_trigger_sonda"]  = $this->plantilla->get();
		
		$valida = $this->protect->access_page('CONFIG_HOST_LIST');
		$this->plantilla->load_sec("configuracion/config_equipos", "denegado",$valida);
		
		$value["option_group"] = $this->basic->getOption($grupos,'groupid','name','all');

		$value["displayColum"] = $this->getTableColum('config_sondas');
	
		$this->plantilla->set($value);
		echo $this->plantilla->get();
	}

	private function getTableColum($table) {
	
		$getColumn = "select hf.`feature`, hf.`display`, ht.`width`, ht.`sortable`, ht.`align` 
						FROM `bm_host_table` ht JOIN `bm_host_feature` hf USING(`id_feature`)
						WHERE ht.`view_table` = 'true' AND ht.`table`='$table' 
						ORDER BY ht.`orden` asc;";
		$Columns = $this->conexion->queryFetch($getColumn);
		$option = array();
		foreach ($Columns as $key => $Colum) {
			if($Colum['width'] == 'none') {
				$width = '';
			} else {
				$width = ", width : '".$Colum['width']."'";
			}		
			
			$option[] = "{display: '".$Colum['display']."', name : '".$Colum['feature']."' ".$width."  , sortable : ".$Colum['sortable'].", align: '".$Colum['align']."'}";
		}
		$optionF = join(',',$option);
		return $optionF;
	}
			
	public function getSondas()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$page = 1; 
		$sortname = 'id'; 
		$sortorder = 'asc'; 
		$qtype = ''; 
		$query = '';
		$rp = 15;
		
		// Validaciones de los parametros enviados por la libreria flexigrid
		if (isset($_POST['page'])) {
		        $page = $_POST['page'];
		}
		if (isset($_POST['sortname']) && ($_POST['sortname']  != 'undefined')) {
		        $sortname = $_POST['sortname'];
		}
		if (isset($_POST['sortorder']) && ($_POST['sortname']  != 'undefined')) {
		        $sortorder = $_POST['sortorder'];
		}
		if (isset($_POST['qtype'])) {
		        $qtype = $_POST['qtype'];
		}
		if (isset($_POST['query'])) {
		        $query = $_POST['query'];
		}
		if (isset($_POST['rp'])) {
		        $rp = $_POST['rp'];
		}
		
		//Parametros Table
		
		$data = array();
		$data['page'] = $page;
		
		$pageStart = ($page-1)*$rp;
		
		$limitSql = " LIMIT $pageStart, $rp";
				
		//Filtros
		
		if(isset($getParam->fm_sondas_Grupo)) {
			if($getParam->fm_sondas_Grupo == 0) {
				$WHERE = "AND H.`groupid` IN ".$this->bmonitor->getAllGroupsHost(false,'sql');
			} else {
				$WHERE = "AND H.`groupid`=".$getParam->fm_sondas_Grupo;
			}
		}
		
		//Total Sondas 
		$sondas_sql_total =	"SELECT count(*) as Total FROM `bm_host` H WHERE `borrado`=0 ".$WHERE;
					
		$host_totales = $this->conexion->queryFetch($sondas_sql_total);
		
		if($host_totales) {
			$data['total'] = $host_totales[0]['Total'];
		} else {
			$data['total'] = 0;
		}
		
		//Features
		
		$query_feature = "SELECT HF.`feature`
							FROM `bm_host_table`  HT
							LEFT JOIN `bm_host_feature` HF USING(`id_feature`)
							WHERE `table`='config_sondas' ORDER BY HT.`orden` ASC;";
							
		$featuresTable = $this->conexion->queryFetch($query_feature);
		
		//Sondas
		
		$query_get_sondas = "SELECT H.*,G.`name` as grupo ,P.`plan` ,GROUP_CONCAT(HF.`feature`)  AS feature,GROUP_CONCAT(HD.`value`) AS valueFeature
				FROM `bm_host` H
									LEFT JOIN `bm_host_detalle` HD USING(`id_host`)
									LEFT JOIN `bm_host_feature` HF ON HF.`id_feature`=HD.`id_feature`
									LEFT JOIN `bm_plan` P ON P.`id_plan`=H.`id_plan`
									LEFT JOIN `bm_host_groups` G ON G.`groupid`=H.`groupid`
				WHERE  
						H.`borrado`=0 AND HF.`feature_type` = 'other' $WHERE
				GROUP BY H.`id_host`
				ORDER BY HF.`id_feature` ASC;";
				
		$sondas = $this->conexion->queryFetch($query_get_sondas);
		
		$data['rows'] = array();
		$adminActive = false;
		
		$profile = $this->protect->getUserProfile();
		
		foreach ($sondas as $keySonda => $sonda) {
			
			$valores = array();
			$valores['host'] = $sonda['host'];
			$valores['ip_wan'] = $sonda['ip_wan'];
			$valores['plan'] = $sonda['plan'];
			$valores['dns'] = $sonda['dns'];
			$valores['mac'] = $sonda['mac'];
			$valores['grupo'] = $sonda['grupo'];
			
			if($sonda['availability'] == 1) {
				$valores['availability'] = '<span class="status_on">Disponible<span>';
			} else {
				$valores['availability'] = '<span class="status_off">No disponible<span>';
			}
			
			if($sonda['status']) {
				$valores['estado'] = '<a href="#" onclick="statusSonda('.$sonda['id_host'].')" id="statusSonda_'.$sonda['id_host'].'" ><span id="statusSonda_span_'.$sonda['id_host'].'" class="status_on">Activo</span></a>';
			} else {
				$valores['estado'] = '<a href="#" onclick="statusSonda('.$sonda['id_host'].')" id="statusSonda_'.$sonda['id_host'].'" ><span id="statusSonda_span_'.$sonda['id_host'].'" class="status_off">Inactivo</span></a>';
			}
			
			$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
			$option .= '<span id="toolbarSet">
							<button id="editSonda" onclick="editSonda('.$sonda['id_host'].',false)" name="editSonda">Editar</button>
							<button id="clonarSonda" onclick="editSonda('.$sonda['id_host'].',true)" name="clonarSonda">Clonar</button>
							<button id="configSonda" onclick="configSonda('.$sonda['id_host'].',true)" name="configSonda">Configurar</button>';
							
			if($profile->secretadmin) {
				$option .= '<button id="triggerSonda" onclick="triggerSonda('.$sonda['id_host'].')" name="triggerSonda">Trigger</button>';
			}
			
			$option .= '</span></span>';
			
			$valores['opciones'] = $option;

			$features=explode(',',$sonda['feature']);
			$valoresFeature=explode(',',$sonda['valueFeature']);

			foreach ($features as $keyF => $feature) {
				$valores[$feature] = $valoresFeature[$keyF];
			}
			
			$datos = array();
			
			foreach ($featuresTable as $table) {
				$datos[] = $valores[$table['feature']];
			}
			
			$data['rows'][] = array(
				'id' => $sonda['id_host'],
				'cell' => $datos
			);
			
		}
		
		echo json_encode($data);
	}

	public function getConfigSonda($idsonda=false)
	{
		
		if($idsonda) {
			$query_get_config = "SELECT `id_feature`, `feature`, `display` , `value` as default_value
									FROM `bm_host_detalle` HD 
									INNER JOIN `bm_host_feature` HF USING(`id_feature`)
									WHERE HD.`id_host`=$idsonda AND `type_feature` = 'config'";
		} else {
			$query_get_config = "SELECT `id_feature`,`feature`, `display`,`default_value` 
									FROM `bm_host_feature` 
									WHERE `type_feature`='config'";			
		}

		$config = $this->conexion->queryFetch($query_get_config);
		
		
		$configArray = array();
		foreach ($config as $key => $value) {
			$config_name = $value['feature'];
			$configArray[$config_name]['id_feature'] = $value['id_feature'];
			$configArray[$config_name]['display'] = $value['display'];
			$configArray[$config_name]['default_value'] = $value['default_value'];
		}
		$checked = '';
		if($configArray['wifi']['default_value'] === "1") {
			$checked = 'checked';
		}

		$input = '<div class="row"><label for="cfg_'.$configArray['wifi']['id_feature'].'" class="col1">'.$configArray['wifi']['display'].'</label>';
		$input .= '<span class="col2"><input type="checkbox" class="checkbox" name="cfg_'.$configArray['wifi']['id_feature'].'" value="1" '.$checked.'></span></div>';
		
		$input .= '<div class="row"><label for="cfg_'.$configArray['wl_channel']['id_feature'].'" class="col1">'.$configArray['wl_channel']['display'].'</label>';
		$input .= '<span class="col2"><select name="cfg_'.$configArray['wl_channel']['id_feature'].'" id="cfg_'.$configArray['wl_channel']['id_feature'].'" class="select">';
		
		for ($i=1; $i < 12; $i++) {
			if($configArray['wl_channel']['default_value'] == $i) {
				$select = 'selected';
			} else {
				$select = '';
			}
			$input .= "<option $select value='$i'>$i</option>\n";
		}

		$input .= '</select></span></div>';
		
		$input .= '<div class="row"><label for="cfg_'.$configArray['wl_ssid']['id_feature'].'" class="col1">'.$configArray['wl_ssid']['display'].'</label>';
		$input .= '<span class="col2"><input type="text" name="cfg_'.$configArray['wl_ssid']['id_feature'].'" id="cfg_'.$configArray['wl_ssid']['id_feature'].'" value="'.$configArray['wl_ssid']['default_value'].'" class="input ui-widget-content ui-corner-all" /></span></div>';
		
		$input .= '<div class="row"><label for="cfg_'.$configArray['wl_key']['id_feature'].'" class="col1">'.$configArray['wl_key']['display'].'</label>';
		$input .= '<span class="col2"><input type="text" name="cfg_'.$configArray['wl_key']['id_feature'].'" id="cfg_'.$configArray['wl_key']['id_feature'].'" value="'.$configArray['wl_key']['default_value'].'" class="input ui-widget-content ui-corner-all" /></span></div>';

		$input .= '<div class="row"><label for="cfg_'.$configArray['block_ip']['id_feature'].'" class="col1comment">'.$configArray['block_ip']['display'].'</label>';
		$input .= '<span class="col2comment"><textarea class="textarea" name="cfg_'.$configArray['block_ip']['id_feature'].'" cols="60" rows="5">'.$configArray['block_ip']['default_value'].'</textarea></span></div>';
		
		$checked = '';
		if($configArray['cparental']['default_value'] === "1") {
			$checked = 'checked';
		}
		
		$input .= '<div class="row"><label for="cfg_'.$configArray['cparental']['id_feature'].'" class="col1">'.$configArray['cparental']['display'].'</label>';
		$input .= '<span class="col2"><input type="checkbox" class="checkbox" name="cfg_'.$configArray['cparental']['id_feature'].'" value="1" '.$checked.'></span></div>'; 

		$input .= '<div class="row"><label for="cfg_'.$configArray['timezone']['id_feature'].'" class="col1">'.$configArray['timezone']['display'].'</label>';
		$input .= '<span class="col2"><select name="cfg_'.$configArray['timezone']['id_feature'].'" id="cfg_'.$configArray['timezone']['id_feature'].'" class="select">';
		
		for ($i=-11; $i < 12; $i++) {
			$val = "GMT".$i;
			if($val == 'GMT0') {
				$val = 'GMT';
			}
			if($configArray['timezone']['default_value'] == $val) {
				$select = 'selected';
			} else {
				$select = '';
			}
			
			$input .= "<option $select value='$val'>$val</option>\n";
		}

		$input .= '</select></span></div>';		

		$input .= '<input type="hidden" name="cfg_'.$configArray['encryption']['id_feature'].'" id="cfg_'.$configArray['encryption']['id_feature'].'" value="'.$configArray['encryption']['default_value'].'"  class="input ui-widget-content ui-corner-all" />';
		$input .= '<input type="hidden" name="cfg_'.$configArray['wl_net_mode']['id_feature'].'" id="cfg_'.$configArray['wl_net_mode']['id_feature'].'" value="'.$configArray['wl_net_mode']['default_value'].'"  class="input ui-widget-content ui-corner-all" />';				
		
		return $input;
	}

	public function getFormFeature($idsonda = false) {
		
		if($idsonda) {
			$query_feature = "SELECT `id_feature`, `feature`, `display` , `value` as default_value
								FROM `bm_host_detalle` HD 
								INNER JOIN `bm_host_feature` HF USING(`id_feature`)
								WHERE HD.`id_host`=$idsonda AND HF.`feature_type`='other' AND `type_feature` != 'config' ORDER BY HF.`orden`;";
		} else {
			$groupid = $_POST['groupid'];
			$query_feature = "SELECT `id_feature`, `feature`, `display`,`default_value`, HF.*, HG.`groupid`
								FROM `bm_host_feature` HF
									LEFT OUTER JOIN `bm_host_groups` HG ON HF.`type_feature`=HG.`type`
								WHERE  `feature_type`='other' AND  `type_feature`  != 'config' AND (HG.`groupid`=$groupid OR HG.`groupid` IS NULL) ORDER BY HF.`orden`";		
		}

		$features = $this->conexion->queryFetch($query_feature);
		
		$value = array();
		
		$value['status'] = false;
		
		if($features) {
			
			$form = '<fieldset class="ui-widget-content ui-corner-all">';
			
			foreach ($features as $key => $feature) {
				if($feature['feature'] == 'region') {
					$form .= '<div class="row"><label for="sonda_opc_'.$feature['id_feature'].'" class="col1">'.$feature['display'].'</label>';
					$form .= '<span class="col2"><select name="sonda_opc_'.$feature['id_feature'].'" id="sonda_opc_'.$feature['feature'].'" class="select">';
		
					$select_region = "SELECT * FROM `bm_region`";
									
					$region_array = $this->conexion->queryFetch($select_region);
			
					if($region_array) {
						$region = $feature['default_value'];
						$form .= $this->basic->getOption($region_array, 'REGION_ID', 'REGION_NOMBRE',(string)$feature['default_value']);
					}
					
					$form .= '</select></span></div>'."\n";
		
				} elseif (($feature['feature'] == 'comuna') || ($feature['feature'] == 'provincia')) {
					$form .= '<div class="row"><label for="sonda_opc_'.$feature['id_feature'].'" class="col1">'.$feature['display'].'</label>';
					$form .= '<span class="col2"><select name="sonda_opc_'.$feature['id_feature'].'" id="sonda_opc_'.$feature['feature'].'" class="select">';
						if($feature['default_value'] == 0) {
							$form .= '<option selected value="0">'.$this->language->SELECT.'</option>';
						} else {
							if($feature['feature'] == 'provincia') {
								$provincia = $feature['default_value'];
								$form .= $this->getProvinciaOption($feature['default_value'],$region);
							} elseif ($feature['feature'] == 'comuna') {
								$form .= $this->getComunaOption($feature['default_value'],$provincia);
							}
						}	
					$form .= '</select></span></div>'."\n";
				} else { 
					$form .= '<div class="row"><label for="sonda_opc_'.$feature['id_feature'].'" class="col1">'.$feature['display'].'</label>';
					$form .= '<span class="col2"><input type="text" name="sonda_opc_'.$feature['id_feature'].'" id="sonda_opc_'.$feature['feature'].'" value="'.$feature['default_value'].'"  class="input ui-widget-content ui-corner-all" /></span></div>'."\n";
				}
			}

			$form .= '</fieldset>';
			$form .= '<script type="text/javascript" src="'.URL_BASE.'sitio/js/form_config.js"></script>';

			$value['status'] = true;
			$value['datos'] = utf8_encode($form);
		}
		
		if($idsonda) {
			return $form;
		} else {
			echo json_encode($value);
		}
		
	}

	public function statusSonda()
	{
		$valida = $this->protect->access_page('CONFIG_STATUS_SONDA');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			$update_item_group = "UPDATE `bm_host` SET `status` = '$getParam->status' WHERE `id_host` = '$getParam->id';";
			
			$valida = $this->conexion->query($update_item_group);
			
			if(!$valida) {
				$data['status'] = false;
			}
			
		} else {
			$data['status'] = false;
		}
		
		echo json_encode($data);
	}
	
	public function deleteSonda()
	{
		$valida = $this->protect->access_page('CONFIG_DELETE_SONDA');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			foreach ($getParam->idSonda as $key => $value) {
				if(is_numeric($value)) {
					
					$update_sonda = "UPDATE `bm_host` SET `borrado` = '1' 
										WHERE `id_host` = '$value';";
										
					$valida = $this->conexion->query($update_sonda);
					
					if(!$valida) {
						$data['status'] = false;
					}
					
				}
			}
					
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}

	public function createSonda() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_SONDA');
		if($valida) {
			$getParam = (object)$_POST;
			
			//Validar valores
			
			$getParam->cfg_5 = (isset($_POST['cfg_5'])) ? 1 : 0;
			$getParam->cfg_3 = (isset($_POST['cfg_3'])) ? 1 : 0;
			
			//Categoriza resultado	
			
			$config = array();
			$op = array();
			$host = array();
			
			foreach ($getParam as $key => $value) {
				if(preg_match("/^cfg_/i",$key)) {
					$keyName = explode("_", $key,2);
					$config[$keyName[1]] = $value;
				} elseif (preg_match("/^sonda_opc_/i",$key)) {
					$keyName = explode("sonda_opc_", $key,2);
					$op[$keyName[1]] = $value;
				} else{
					$keyName = explode("sonda_", $key,2);
					$host[$keyName[1]] = $value;
				} 
			}
			
			//Valdando Mac Existente
			
			$select_result = $this->conexion->queryFetch("SELECT count(*) as Total , `host`  FROM `bm_host` WHERE ( `mac` = '?' OR `mac_lan` = '?' ) AND `borrado` = 0 ",$host['mac_wan'],$host['mac_wan']);
			
			if($select_result) {
				
				if($select_result[0]['Total'] > 0) {
					
					$result['status'] = false;	
					$result['msg'] =  str_replace('{NAME_SONDA}',$select_result[0]['host'],$this->language->CONFIG_SONDA_CREATE_CONFLICT_MAC);
					
					header("Content-type: application/json");
					echo json_encode($result);
					exit;
				}
				
			}

			//Generando Host
			
			$result = array();
			
			$this->conexion->InicioTransaccion();
			
			$insert_host = sprintf("INSERT INTO `bm_host` (`groupid`, `host`, `dns`, `mac`, `ip_wan`, `ip_lan`, `mac_lan`, `netmask_lan`, `id_plan`, `status`, `borrado`)
										VALUE (%s, '%s', '%s', '%s', '%s', '%s', '%s','%s', %s , 1, 0)",
										$host['group'],
										$host['name'],
										$host['dns'],
										$host['mac_wan'],
										$host['ip_wan'],
										$host['ip_lan'],
										$host['mac_lan'],
										$host['netmask_lan'],
										$host['plan']);
										
			$host_result = $this->conexion->query($insert_host);
			$idHost = $this->conexion->lastInsertId();
			
			if($host_result) {
				
				
				//Cargando feature 
				
				$insert_feture = "INSERT INTO `bm_host_detalle` (`id_host`, `id_feature`, `value`) VALUES ";
				
				// Inicio Config
				$value_feature_config = array();
				
				foreach ($config as $key => $value) {
					$value_feature_config[] = '('.$idHost.','.$key.','."'$value')";
				}
			
				$insert_feture_config = $insert_feture.join(',',$value_feature_config);
				
				$feature_insert_config = $this->conexion->query($insert_feture_config);
				
				// Fin Config
				
				if($feature_insert_config) {
					$value_feature_op = array();
					foreach ($op as $key => $value) {
						$value_feature_op[] = '('.$idHost.','.$key.','."'$value')";
					}
				
					$insert_feture_op = $insert_feture.join(',',$value_feature_op);
					
					$feature_insert_op = $this->conexion->query($insert_feture_op);	
					
					if($feature_insert_op) {
						
						//Creando Monitores
						
						$get_monitores = 'SELECt I.`id_item`, I.`type_poller`,  I.`snmp_oid` 
									FROM `bm_items` I 
									LEFT JOIN `bm_items_groups` IG USING(`id_item`) 
									WHERE  IG.`groupid` = '.$host['group'];
									
						$monitores = $this->conexion->queryFetch($get_monitores);
						
						if($monitores) {
							$insert_monitor_host = 'INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`) VALUES ';
							$values_insert = array();
							foreach ($monitores as $key => $monitor) {
								if($monitor['type_poller'] == 'snmp') {
									$snmp_oid = $monitor['snmp_oid'];
									$values_insert [] = '('.$monitor['id_item'].','.$idHost.','."'".$monitor['snmp_oid']."','1161','public','0000-00-00 00:00:00')";
								} else {
									$values_insert [] = '('.$monitor['id_item'].','.$idHost.",NULL,NULL,NULL,'0000-00-00 00:00:00')";
								}
								
							}
							
							$insert_monitor_host = $insert_monitor_host.join(',',$values_insert);
							
							$insert_monitor_host_result = $this->conexion->query($insert_monitor_host);
							
							if($insert_monitor_host_result) {
								$result['status'] = true;
							} else {
								$result['status'] = false;
							}
						} else {
							$result['status'] = false;
						}
					} else {
						$result['status'] = false;
					}
				} else {
					$result['status'] = false;
				}
			} else {
				$result['status'] = false;
			}
			
		} else {
			$result['status'] = false;
		}
		
		if($result['status']) {
			$this->protect->reg_event('create_sonda',"Usuario creo la sonda: [$idHost] ".$host['name']);
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		if(!$result['status']) {
			$result['msg'] = $this->language->CONFIG_SONDA_CREATE;
		}
		
		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}

	public function getSondaForm()
	{
		if(is_numeric($_POST['sondaID'])) {
			$idsonda = $_POST['sondaID'];
		} else {
			return false;
		}
		
		$valida = $this->protect->access_page('CONFIG_EDIT_SONDA');
		
		$this->plantilla->load_sec('configuracion/form_edit_sonda', 'denegado', $valida);

		// Detalle Sonda
		
		$sonda_sql = "SELECT H.`id_host` as id_sonda, H.`host`, H.`dns`, H.`mac` as mac_wan, H.`ip_wan`,H.`ip_lan`, H.`mac_lan`, H.`netmask_lan` , H.`id_plan`,HG.`name` , HG.`groupid`, P.`plan`
						FROM `bm_host` H
						LEFT JOIN `bm_host_groups` HG USING(`groupid`)
						LEFT JOIN `bm_plan` P ON H.`id_plan`=P.`id_plan`
						WHERE H.`id_host`=$idsonda";
						
		$sonda_detalles = $this->conexion->queryFetch($sonda_sql);

		if($sonda_detalles) {
			
			$sonda_detalles = $sonda_detalles[0];
			
			foreach ($sonda_detalles as $key => $sonda) {
				$value["sonda_".$key] = $sonda;
			}
		}
		
		// Grupo Sonda
		$value["option_group"] = '<option selected value="'.$value["sonda_groupid"].'">'.$value["sonda_name"].'</option>';
		
		// Grupo Plan
		$value["option_plan"] = $this->getPlanOption($value["sonda_id_plan"],$value["sonda_groupid"]);
		
		// Formulario configuracion
		$value["input_config"] = $this->getConfigSonda($idsonda);
		
		// Formulario opcionales
		$value["input_opcional"] = $this->getFormFeature($idsonda);

		$this->plantilla->set($value);
		echo $this->plantilla->get();
	}

	public function editSonda() 
	{
		$valida = $this->protect->access_page('CONFIG_EDIT_SONDA');
		
		if($valida) {
			$getParam = (object)$_POST;
			
			//Validar valores
			
			$getParam->cfg_5 = (isset($_POST['cfg_5'])) ? 1 : 0;
			$getParam->cfg_3 = (isset($_POST['cfg_3'])) ? 1 : 0;
			
			//Categoriza resultado	
			
			$config = array();
			$op = array();
			$host = array();
			
			foreach ($getParam as $key => $value) {
				if(preg_match("/^cfg_/i",$key)) {
					$keyName = explode("_", $key,2);
					$config[$keyName[1]] = $value;
				} elseif (preg_match("/^sonda_opc_/i",$key)) {
					$keyName = explode("sonda_opc_", $key,2);
					$op[$keyName[1]] = $value;
				} elseif (preg_match("/^sonda_/i",$key)) {
					$keyName = explode("sonda_", $key,2);
					$host[$keyName[1]] = $value;
				} 
			}
			
			//Generando Host
			
			$result = array();
			
			$this->conexion->InicioTransaccion();
			
			$idHost = $getParam->id;
			
			$update_host = sprintf("UPDATE `bm_host` SET `host` = '%s', `dns` = '%s', `mac` = '%s', `ip_wan` = '%s', `ip_lan` = '%s', `mac_lan` = '%s', `netmask_lan` = '%s' , `id_plan` = '%s' WHERE (`id_host`='$idHost') LIMIT 1;",
									$host['name'],
									$host['dns'],
									$host['mac_wan'],
									$host['ip_wan'],
									$host['ip_lan'],
									$host['mac_lan'],
									$host['netmask_lan'],
									$host['plan']);

			$host_result = $this->conexion->query($update_host);
		
			if($host_result) {
				
				// Inicio Config
				
				//Areglar codigo sin validar**************************************
				$feature_insert_config = 'INSERT INTO `bm_host_detalle` (`id_host`,`id_feature`,`value`) VALUES ';
				$feature_insert_config_value = array();
				foreach ($config as $key => $value) {
					if($key != '') {
						$feature_insert_config_value[] =  "($idHost,$key,'$value')";
					}
				}
				$feature_insert_config = $feature_insert_config.join(',', $feature_insert_config_value)." ON DUPLICATE KEY UPDATE `value`=VALUES(`value`)";
				$feature_insert_config = $this->conexion->query($feature_insert_config);
			
				// Fin Config
				
				if($feature_insert_config) {
						
					$feature_insert_op = 'INSERT INTO `bm_host_detalle` (`id_host`,`id_feature`,`value`) VALUES ';
					$feature_insert_op_value = array();
					foreach ($op as $key => $value) {
						if($key != '') {
							$feature_insert_op_value[] =  "($idHost,$key,'$value')";
						}
					}
					
					$feature_insert_op = $feature_insert_op.join(',', $feature_insert_op_value)." ON DUPLICATE KEY UPDATE `value`=VALUES(`value`)";
					$feature_insert_op = $this->conexion->query($feature_insert_op);
				
					
					if($feature_insert_op) {
						$result['status'] = true;
					} else {
						$result['status'] = false;
					}
				} else {
					$result['status'] = false;
				}
			} else {
				$result['status'] = false;
			}
			
		} else {
			$result['status'] = false;
		}
		
		if($result['status']) {
			$this->protect->reg_event('edit_sonda',"Usuario modifico la sonda: [$idHost] ".$host['name']);
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		if(!$result['status']) {
			$result['msg'] = $this->language->CONFIG_SONDA_CREATE;
		}
		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}

	public function setConfigSonda()
	{
		$valida = $this->protect->access_page('CONFIG_TRIGGER_SONDA');
		
		if($valida) { 
			$getParam = (object)$_POST;
			$data['status'] = true;
			foreach ($getParam->idSonda as $key => $value) {
				if(is_numeric($value)) {
					
					$update_sonda = "INSERT INTO `bm_trigger` (`id_host`, `fecha_modificacion`, `type`) VALUES ('$value', NOW(), 'config')";
										
					$valida = $this->conexion->query($update_sonda);
					
					if(!$valida) {
						$data['status'] = false;
					}
					
				}
			}
					
		} else {
			$data['status'] = false;
		}
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}

	public function setTriggerSonda()
	{
		$valida = $this->protect->access_page('CONFIG_TRIGGER_SONDA');
		
		if($valida) { 
			$getParam = (object)$_POST;
			$data['status'] = true;
			if(is_numeric($getParam->idSonda)) {
				
				$update_sonda = "INSERT INTO `bm_trigger` (`id_host`, `fecha_modificacion`, `command`,`responsable`) VALUES ('$getParam->idSonda', NOW(), '$getParam->trigger_id','$getParam->trigger_responsable')";
									
				$valida = $this->conexion->query($update_sonda);
				
				if(!$valida) {
					$data['status'] = false;
				}
				
			}
		} else {
			$data['status'] = false;
		}
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}
	
	public function cfgMonitores()
	{
		$value = array();
		
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_monitores_form");
		
		//Items
		
		$grupos = $this->bmonitor->getGroupsHost();;
		
		if($grupos) {
			$value["menu_groupid_list"] = '';
			foreach ($grupos as $key => $grupo) {				
				$value["menu_groupid_list"] .= '<option value="'.$grupo['groupid'].'">'.$grupo['name'].'</option>';
			}
		}
		
		$value["lang"] = $this->protect->getLang();
		
		$value["form_id"] = 'config_monitores_form_new';
		
		$value['option_type_monitor'] = '<option selected value="1">Snmp</option><option value="2">Agente BSW</option>';
		$value["option_type_item"] = $this->basic->getOptionValue('type_data');
		
		$this->plantilla->set($value);
		
		$value["form_new_monitor"] = $this->plantilla->get();
		
		$valida = $this->protect->access_page('CONFIG_MONITOR_LIST');
		
		$this->plantilla->load_sec("configuracion/config_monitores", "denegado",$valida);

		$value["option_group"] = $this->basic->getOption($grupos,'groupid','name','all');
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getMonitores()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$page = 1; 
		$sortname = 'id_item'; 
		$sortorder = 'asc'; 
		$qtype = ''; 
		$query = '';
		$rp = 15;
		
		// Validaciones de los parametros enviados por la libreria flexigrid
		if (isset($_POST['page'])) {
		        $page = $_POST['page'];
		}
		if (isset($_POST['sortname']) && ($_POST['sortname']  != 'undefined')) {
		        $sortname = $_POST['sortname'];
		}
		if (isset($_POST['sortorder']) && ($_POST['sortname']  != 'undefined')) {
		        $sortorder = $_POST['sortorder'];
		}
		if (isset($_POST['qtype'])) {
		        $qtype = $_POST['qtype'];
		}
		if (isset($_POST['query'])) {
		        $query = $_POST['query'];
		}
		if (isset($_POST['rp'])) {
		        $rp = $_POST['rp'];
		}
		
		//Parametros Table
		
		$data = array();
		$data['page'] = $page;
		
		$pageStart = ($page-1)*$rp;
		
		$limitSql = " LIMIT $pageStart, $rp";
				
		//Filtros
		
		if(isset($getParam->fmGrupoMonitores)) {
			if($getParam->fmGrupoMonitores == 0) {
				$WHERE = "";
			} else {
				$WHERE = "WHERE ITG.`groupid`=".$getParam->fmGrupoMonitores;
			}
		}
		
		//Total Monitorese 
		$monitores_sql_total =	"SELECT count(DISTINCT `id_item`)  as Total
									FROM  `bm_items_groups` ITG ".$WHERE;
					
		$monitores_totales = $this->conexion->queryFetch($monitores_sql_total);
		
		if($monitores_totales) {
			$data['total'] = $monitores_totales[0]['Total'];
		} else {
			$data['total'] = 0;
		}
		
		//Monitores
		
		$monitores_sql =	"SELECT IT.*,ITG.`status`
							FROM `bm_items` IT
							LEFT JOIN `bm_items_groups` ITG USING(`id_item`)
							".$WHERE.
							" GROUP BY ITG.`id_item` $limitSql ";

		$monitores = $this->conexion->queryFetch($monitores_sql);
		
		$data['rows'] = array();
		
		foreach ($monitores as $keyMonitor => $monitor) {
			
			
			$valores['opciones'] = 
			
			$datos[0] = $monitor['description'];
			$datos[1] = $monitor['name'];
			$datos[2] = $monitor['delay'];
			$datos[3] = $monitor['history'];
			$datos[4] = $monitor['trend'];
			$datos[5] = $monitor['type_poller'];
			if($monitor['status']) {
				$status = '<a href="#" onclick="statusMonitor('.$monitor['id_item'].','.$getParam->fmGrupoMonitores.')" id="statusMonitor_'.$monitor['id_item'].'" >Activo</a>';
			} else {
				$status = '<a href="#" onclick="statusMonitor('.$monitor['id_item'].','.$getParam->fmGrupoMonitores.')" id="statusMonitor_'.$monitor['id_item'].'" >Inactivo</a>';
			}
			$datos[6] = $status;
			
			$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
			$option .= '<span id="toolbarSet">
							<button id="editMonitor" onclick="editar('.$monitor['id_item'].',false)" name="editMonitor">Editar</button>
							<button id="clonarMonitor" onclick="editar('.$monitor['id_item'].',true)" name="clonarMonitor">Clonar</button>
						</span>';
			$option .= '</span>';
			$datos[7] = $option;
			//$datos[7] = '<a href="#" id="editMonitor" onclick="editMonitor('.$monitor['id_item'].',false)" name="editMonitor" class="buttonDefault">Editar</a><a href="#" id="clonarMonitor" onclick="editMonitor('.$monitor['id_item'].',true)" name="clonarMonitor" class="buttonDefault">Clonar</a>';
			
			$data['rows'][] = array(
				'id' => $monitor['id_item'],
				'cell' => $datos
			);
			
		}
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}
	
	private function fixMonitores()
	{
		$getMonitores_error_sql = "SELECT H.`id_host`,IG.`id_item`,I.`snmp_community`, I.`snmp_oid`, I.`snmp_port`
					FROM `bm_host` H
					LEFT JOIN `bm_items_groups` IG USING(`groupid`) 
					LEFT JOIN `bm_items` I ON  I.`id_item`=IG.`id_item`
					LEFT OUTER JOIN `bm_item_profile` IP ON IP.`id_host`=H.`id_host` AND IP.`id_item`=IG.`id_item`
					WHERE IP.`id_item` IS NULL";
					
		$getMonitores_error_result = $this->conexion->queryFetch($getMonitores_error_sql);
		
		if($getMonitores_error_result) {
			
			$insert_monitor_profile = "INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`) VALUES ";
			
			foreach ($getMonitores_error_result as $key => $host) {
				$insert_monitor_value[] = '('.$host['id_item'].','.$host['id_host'].',\''.$host['snmp_oid'].'\',\''.$host['snmp_port'].'\',\''.$host['snmp_community'].'\', NOW())';
			}
			
			$insert_monitor_profile_f = join(",", $insert_monitor_value);
			$insert_monitor_profile_f = $insert_monitor_profile.$insert_monitor_profile_f;
			$result_monitor_profile = $this->conexion->query($insert_monitor_profile_f);
			
		}
		
	}
	
	private function generaMonitores($iditem,$groupID,$snmp_oid,$snmp_port,$snmp_community) 
	{
		if(is_array($groupID)) {
				
			$groupID = $this->conexion->arrayToIN($groupID);
			
			$getHosts_sql = "SELECT `id_host` FROM `bm_host` WHERE `groupid` IN $groupID;";
			
			$getHosts = $this->conexion->queryFetch($getHosts_sql);
			if($getHosts) {
				$insert_monitor_profile = "INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`) VALUES ";
				$insert_monitor_value = array();
				foreach ($getHosts as $key => $host) {
					$insert_monitor_value[] = '('.$iditem.','.$host['id_host'].',\''.$snmp_oid.'\',\''.$snmp_port.'\',\''.$snmp_community.'\', NOW())';
				}
				$insert_monitor_profile_f = join(",", $insert_monitor_value);
				$insert_monitor_profile_f = $insert_monitor_profile.$insert_monitor_profile_f." ON DUPLICATE KEY UPDATE `snmp_oid`=VALUES(`snmp_oid`), `snmp_port`=VALUES(`snmp_port`), `snmp_community`=VALUES(`snmp_community`)";
				$result_monitor_profile = $this->conexion->query($insert_monitor_profile_f,true);
				
				if($result_monitor_profile) {
					$this->fixMonitores();
					return true;
				} else {
					return false;
				}
			}

		}
	}

	public function createMonitor() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_MONITOR');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			$result['status'] = true;
			
			foreach ($getParam as $key => $value) {
				if((preg_match("/^config_monitores_form_new_/i",$key)) || (preg_match("/^config_monitores_form_edit_/i",$key))) {
						$keyName = explode("_", $key,5);
						$values[$keyName[4]] = $value;
				}
			}
			
			if(!is_numeric($getParam->id)) {
				/* Creando nuevo Grafico */ 
				
				$this->conexion->InicioTransaccion();
				
				$insert_sql = sprintf("INSERT INTO `bm_items` (`name`, `description`, `type_item`, `delay`, `history`, `trend`, `type_poller`, `unit`, `snmp_oid`, `snmp_community`, `snmp_port`)
										VALUE ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s')",
										$values['name'],
										$values['description'],
										$values['type_item'],
										$values['delay'],
										$values['history'],
										$values['trend'],
										$values['type'],
										utf8_decode((string)$values['unit']),
										$values['snmp_oid'],
										$values['snmp_community'],
										$values['snmp_port']);
										
				$sql_result = $this->conexion->query($insert_sql);
				$idInsert = $this->conexion->lastInsertId();
				
				if($sql_result) {
							
					if((int)$values['status'] == (int)1) {
						$status = 1;
					} else {
						$status = 0;
					}
							
					$insert_sql = "INSERT INTO `bm_items_groups` (`id_item`, `groupid`, `status`) VALUES";
					
					foreach ($values['groupid'] as  $groupid) {
						$insert_values[] = "($idInsert,$groupid,$status)";
					}
					
					$insert_values = join(',',$insert_values);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_values);
					
					if(!$sql_result) {
						$result['status'] = false;
					} else {
						
					$result_monitor = $this->generaMonitores($idInsert,$values['groupid'],$values['snmp_oid'],$values['snmp_community'],$values['snmp_port']);
						
					}
				}
			} else {
				/* Editando Grafico */ 
				$this->conexion->InicioTransaccion();

				$update_sql = sprintf("UPDATE `bm_items` SET `name` = '%s', `description` = '%s', `type_item` = '%s', `delay` = '%s', `history` = '%s', `trend` = '%s', `type_poller` = '%s', `unit` = '%s', `snmp_oid` = '%s', `snmp_community` = '%s', `snmp_port` = '%s' WHERE `id_item` = '%s'",
										$values['name'],
										$values['description'],
										$values['type_item'],
										$values['delay'],
										$values['history'],
										$values['trend'],
										$values['type'],
										$values['unit'],
										$values['snmp_oid'],
										$values['snmp_community'],
										$values['snmp_port'],
										$getParam->id);
											
				$update_result = $this->conexion->query($update_sql);
				
				if($update_result) {
					
					$delete_sql = "/* BSW  */ DELETE FROM `bm_items_groups` WHERE `id_item` = '$getParam->id';";
					$delete_result = $this->conexion->query($delete_sql);
					
					if((int)$values['status'] == (int)1) {
						$status = 1;
					} else {
						$status = 0;
					}
							
					$insert_sql = "INSERT INTO `bm_items_groups` (`id_item`, `groupid`, `status`) VALUES";
					
					foreach ($values['groupid'] as  $groupid) {
						$insert_values[] = "($getParam->id,$groupid,$status)";
					}
					
					$insert_values = join(',',$insert_values);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_values);
					
					if(!$sql_result) {
						$result['status'] = false;
					}
				}
			}
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(!is_numeric($getParam->id))  {
				$this->protect->reg_event('new_monitor',"Usuario creo el monitor: [$idInsert] ".$values['name']);
			} else {
				$this->protect->reg_event('edit_monitor',"Usuario edito el monitor: [$getParam->id] ".$values['name']);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		echo json_encode($result);
	}
	
	public function deleteMonitor()
	{
		$valida = $this->protect->access_page('CONFIG_DELETE_MONITOR');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			foreach ($getParam->idSelect as $key => $value) {
				if(is_numeric($value)) {
					
					//Validando 
					
					$select_sql = "SELECT count(*) As Total FROM `bm_items_groups` WHERE `id_item`='$value';";
					
					$select_result = $this->conexion->queryFetch($select_sql);
					
					if($select_result) {
						
						if($select_result[0]['Total'] > 1) {
							
							$delete_monitor = "DELETE FROM `bm_items_groups` WHERE `id_item` = '$value' AND `groupid` = $getParam->groupid;";
							
						} else {
							$delete_monitor = "DELETE FROM `bm_items` WHERE `id_item` = '$value';";
						}
					} 
														
					$valida = $this->conexion->query($delete_monitor);
					
					if(!$valida) {
						$data['status'] = false;
					} else {
						$this->protect->reg_event('delete_monitor',"Usuario borro el monitor: [$value] del grupo  $getParam->groupid");
					}
				}
			}
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}

	public function statusMonitor()
	{
		$valida = $this->protect->access_page('CONFIG_STATUS_MONITOR');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			$update_item_group = "UPDATE `bm_items_groups` SET `status` = '$getParam->status' WHERE `id_item` = '$getParam->idmonitor' AND `groupid` = '$getParam->groupid';";
			
			$valida = $this->conexion->query($update_item_group);
			
			if(!$valida) {
				$data['status'] = false;
			}
			
		} else {
			$data['status'] = false;
		}

		echo json_encode($data);
	}

	public function getMonitorForm()
	{
		if(is_numeric($_POST['IDSelect'])) {
			$IDSelect = $_POST['IDSelect'];
		} else {
			return false;
		}
		
		$valida = $this->protect->access_page('CONFIG_EDIT_MONITOR');
		
		$this->plantilla->load_sec('configuracion/config_monitores_form', 'denegado', $valida);

		// Detalle Sonda
		
		$monitor_sql = "SELECT * FROM `bm_items`  WHERE `id_item` = $IDSelect";
						
		$monitor_result = $this->conexion->queryFetch($monitor_sql);

		if($monitor_result) {
			$monitor_rows = $monitor_result[0];
			foreach ($monitor_rows as $key => $monitor) {
				$value["monitor_".$key] = $monitor;
			}
		}
		
		if($value["monitor_type_poller"] == 'bsw_agent') {
			$value['option_type_monitor'] = '<option value="1">Snmp</option><option selected value="2">Agente BSW</option>';
		} else {
			$value['option_type_monitor'] = '<option selected value="1">Snmp</option><option value="2">Agente BSW</option>';
		}
			
		//Tipos de datos
		
		$value['option_type_item'] = $this->basic->getOptionValue('type_data',$value["monitor_type_item"]);
		
		//Grupos
		
		$get_grupos_activos = $this->conexion->queryFetch("SELECT DISTINCT HG.`groupid`,HG.`name` , IF(IG.`id_item` IS NULL, '', 'selected') as selected
				FROM `bm_host_groups` HG
				LEFT OUTER JOIN `bm_items_groups` IG ON HG.`groupid`=IG.`groupid`
				WHERE IG.`id_item`=$IDSelect OR IG.`id_item` IS NULL");
		//$get_grupos_activos = $this->basic->arrayKeyToValue($get_grupos_activos, 'groupid', 'name');
		
		if($get_grupos_activos) {
			$value["menu_groupid_list"] = '';
			foreach ($get_grupos_activos as $key => $grupo) {				
				$value["menu_groupid_list"] .= '<option '.$grupo['selected'].' value="'.$grupo['groupid'].'">'.$grupo['name'].'</option>';
			}
		}
		
		//Status 
		
		if($value["monitor_type_poller"] == 'bsw_agent') {
			$value['option_edit_status'] = '<option value="1">Activo</option><option selected value="2">Desactivado</option>';
		} else {
			$value['option_edit_status'] = '<option selected value="1">Activo</option><option value="2">Desactivado</option>';
		}
		
		$value["lang"] = $this->protect->getLang();
		
		$value["form_id"] = 'config_monitores_form_edit';

		$this->plantilla->set($value);
		echo $this->plantilla->get();
	}

	public function cfgPlanes()
	{
		$value = array();
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_planes_form");
		
		//Items
		
		$grupos = $this->bmonitor->getGroupsHost();;
		
		if($grupos) {
			$value["menu_groups_inactive"] = '';
			foreach ($grupos as $key => $grupo) {				
				$value["menu_groups_inactive"] .= '<option value="'.$grupo['groupid'].'">'.$grupo['name'].'</option>';
			}
		}
		
		$value["planes_id_menu"] = 'planes_groups';
		$value["config_planes_id_form"] = 'config_planes_form_new';
		
		
		$value["plan_sysctl"] = $this->parametro->get('SYSCTL_DEFAULT');
		$value["plan_ppp"] = $this->parametro->get('PPP_CONFIG_DEFAULT');
		
		$this->plantilla->set($value);
		
		$value["form_new_plan"] = $this->plantilla->get();
		
		$grupos_option = $this->basic->getOption($grupos,'groupid','name','all');

		$value["combobox_groups"]  = $grupos_option;
		
		//Tabla
		
		$this->basic->setTableId('table_planes');
		$this->basic->setTableUrl("/config/getTablePlanes");
		$this->basic->setTableColModel('config_plan');
		$this->basic->setTableToolbox('toolboxPlanes');
		
		$value["table_planes"] = $this->basic->getTableFL('Planes');
		
		//Generar Plantilla
		
		$valida = $this->protect->access_page('CONFIG_PLAN_LIST');
		
		$this->plantilla->load_sec("configuracion/config_planes", "denegado",$valida);
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getTablePlanes()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'plan');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
				
		//Filtros
		
		$getGroups_in = $this->bmonitor->getGroupsHost(true,true);
		
		if(isset($getParam->fmGrupoPlanes)) {
			if($getParam->fmGrupoPlanes == 0) {
				$WHERE = "";
			} else {
				$WHERE = " AND HG.`groupid`=".$getParam->fmGrupoPlanes;
			}
		}
		
		//Total rows
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total
				FROM `bm_plan_groups` HG
				WHERE `groupid` IN $getGroups_in AND `borrado`=0 $WHERE";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}
		
		//Rows
		
		$getRows_sql =	"SELECT P.`id_plan`, P.`plan`, HG.`name` as groupnamem
							FROM `bm_plan` P
							LEFT JOIN `bm_plan_groups` PG USING(`id_plan`)
							LEFT JOIN `bm_host_groups` HG ON PG.`groupid`=HG.`groupid`
							WHERE PG.`groupid` IN $getGroups_in AND PG.`borrado`=0  $WHERE $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);
		
		if($getRows_result) {
			foreach ($getRows_result as $key => $row) {
				
				$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
				$option .= '<span id="toolbarSet">
								<button id="editPlan" onclick="editar('.$row['id_plan'].',false)" name="editPlan">Editar</button>
								<button id="clonarPlan" onclick="editar('.$row['id_plan'].',true)" name="clonarPlan">Clonar</button>
							</span>';
				$option .= '</span>';
				
				//$option = '<a href="#" id="editPlan" onclick="editPlan('.$row['id_plan'].',false)" name="editPlan" class="buttonDefault">Editar</a><a href="#" id="clonarPlan" onclick="editPlan('.$row['id_plan'].',true)" name="clonarPlan" class="buttonDefault">Clonar</a>';
			
				$data['rows'][] = array(
					'id' => $row['id_plan'],
					'cell' => array($row['groupnamem'],$row['plan'],$option)
				);
			}			
		}
		echo json_encode($data);
	}

	public function createPlan() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_PLAN');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			$result['status'] = true;
			
			foreach ($getParam as $key => $value) {
				if(preg_match("/^plan_/i",$key)) {
						$keyName = explode("_", $key,2);
						if(is_numeric($value)){
							$values[$keyName[1]] = (int)$value;
						} elseif (is_array($value)) {
							$values[$keyName[1]] = $value;
						}  else {
							$values[$keyName[1]] = utf8_encode(trim($value));
						}
						
				}
			}

			if(!is_numeric($getParam->id)) {
				/* Creando nuevo Grafico */ 
				
				$this->conexion->InicioTransaccion();
				
				$insert_sql = sprintf("INSERT INTO `bm_plan` (`plan`, `plandesc`, `planname`, `nacD`, `nacU`, `locD`, `locU`, `intD`, `intU`, `nacDS`, `nacDT`, `nacUS`, `nacUT`, `locDS`, `locDT`, `locUS`, `locUT`, `intDS`, `intDT`, `intUS`, `intUT`, `sysctl`, `ppp`)
											VALUE ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
											$values['plan'],
											$values['plandesc'],
											$values['planname'],
											$values['nacD'],
											$values['nacU'],
											$values['locD'],
											$values['locU'],
											$values['intD'],
											$values['intU'],
											$values['nacDS'],
											$values['nacDT'],
											$values['nacUS'],
											$values['nacUT'],
											$values['locDS'],
											$values['locDT'],
											$values['locUS'],
											$values['locUT'],
											$values['intDS'],
											$values['intDT'],
											$values['intUS'],
											$values['intUT'],
											$this->basic->limpiarMetas($values['sysctl']),
											$this->basic->limpiarMetas($values['ppp']));
											
				$sql_result = $this->conexion->query($insert_sql);
				$idInsert = $this->conexion->lastInsertId();
				
				if($sql_result) {
					
					$insert_sql = "INSERT INTO `bm_plan_groups` (`id_plan`, `groupid`, `borrado`) VALUES";
					
					foreach ($values['groupid'] as  $groupid) {
						$insert_values[] = "($idInsert,$groupid,0)";
					}
					
					$insert_values = join(',',$insert_values);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_values);
					
					if(!$sql_result) {
						$result['status'] = false;
					}
				}
			} else {
				/* Editando Grafico */ 
				$this->conexion->InicioTransaccion();
				
				$update_sql = sprintf("UPDATE `bm_plan` SET `plan` = '%s', `plandesc` = '%s', `planname` = '%s', `nacD` = '%s', `nacU` = '%s', `locD` = '%s', `locU` = '%s', `intD` = '%s', `intU` = '%s', `nacDS` = '%s', `nacDT` = '%s', `nacUS` = '%s', `nacUT` = '%s', `locDS` = '%s', `locDT` = '%s', `locUS` = '%s', `locUT` = '%s', `intDS` = '%s', `intDT` = '%s', `intUS` = '%s', `intUT` = '%s', `sysctl` = '%s', `ppp` = '%s' WHERE `id_plan` = '$getParam->id'",
						$values['plan'],
						$values['plandesc'],
						$values['planname'],
						$values['nacD'],
						$values['nacU'],
						$values['locD'],
						$values['locU'],
						$values['intD'],
						$values['intU'],
						$values['nacDS'],
						$values['nacDT'],
						$values['nacUS'],
						$values['nacUT'],
						$values['locDS'],
						$values['locDT'],
						$values['locUS'],
						$values['locUT'],
						$values['intDS'],
						$values['intDT'],
						$values['intUS'],
						$values['intUT'],
						$this->basic->limpiarMetas($values['sysctl']),
						$this->basic->limpiarMetas($values['ppp']));
				
				
											
				$update_result = $this->conexion->query($update_sql);
				
				if($update_result) {
					
					$delete_sql = "/* BSW  */ DELETE FROM `bm_plan_groups` WHERE `id_plan` = '$getParam->id';";
					$delete_result = $this->conexion->query($delete_sql);
					
					$insert_sql = "INSERT INTO `bm_plan_groups` (`id_plan`, `groupid`, `borrado`) VALUES";
					
					foreach ($values['groupid'] as  $groupid) {
						$insert_values[] = "($getParam->id,$groupid,0)";
					}
					
					$insert_values = join(',',$insert_values);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_values);
					
					if(!$sql_result) {
						$result['status'] = false;
					}
				}
			}
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(!is_numeric($getParam->id))  {
				$this->protect->reg_event('new_pla',"Usuario creo el plan: [$idInsert] ".$values['plan']);
			} else {
				$this->protect->reg_event('edit_plan',"Usuario edito el plan: [$getParam->id] ".$values['plan']);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		echo json_encode($result);
	}

	public function createPlan2() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_PLAN');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			//Categoriza resultado	
			
			$getParam->form = explode("&", $getParam->cadena_1);
			$group = explode("|", $getParam->cadena_2);

			$monitor = array();
			
			if(($getParam->idValue == 'clone') || (is_numeric($getParam->idValue))) {
				foreach ($getParam->form as $key => $value) {
					$form = explode('=',$value);
					if(preg_match("/^plan_edit_/i",$form[0])) {
						$keyName = explode("_edit_", $form[0],2);
						$monitor[$keyName[1]] = $form[1];
					}
				}				
			} else {
				foreach ($getParam->form as $key => $value) {
					$form = explode('=',$value);
					if(preg_match("/^plan_/i",$form[0])) {
						$keyName = explode("_", $form[0],2);
						$monitor[$keyName[1]] = $form[1];
					}
				}				
			}
			
			$this->conexion->InicioTransaccion();
			
			if(($getParam->idValue == 'clone') || (!is_numeric($getParam->idValue)))  {
				//Generando nuevo registro
				$insert_plan = sprintf("INSERT INTO `bm_plan` (`plan`, `plandesc`, `planname`, `nacD`, `nacU`, `locD`, `locU`, `intD`, `intU`, `nacDS`, `nacDT`, `nacUS`, `nacUT`, `locDS`, `locDT`, `locUS`, `locUT`, `intDS`, `intDT`, `intUS`, `intUT`, `sysctl`, `ppp`)
											VALUE ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
											$this->basic->limpiarMetas($monitor['plan']),
											$this->basic->limpiarMetas($monitor['plandesc']),
											$this->basic->limpiarMetas($monitor['planname']),
											$monitor['nacD'],
											$monitor['nacU'],
											$monitor['locD'],
											$monitor['locU'],
											$monitor['intD'],
											$monitor['intD'],
											$monitor['nacDS'],
											$monitor['nacDT'],
											$monitor['nacUS'],
											$monitor['nacUT'],
											$monitor['locDS'],
											$monitor['locDT'],
											$monitor['locUS'],
											$monitor['locUT'],
											$monitor['intDS'],
											$monitor['intDT'],
											$monitor['intUS'],
											$monitor['intUT'],
											$this->basic->limpiarMetas($monitor['sysctl']),
											$this->basic->limpiarMetas($monitor['ppp']));
				$plan_result = $this->conexion->query($insert_plan);
				$idPlan = $this->conexion->lastInsertId();
			} else {
				//Actualizando registro
				$update_sql = sprintf("UPDATE `bm_plan` SET `plan` = '%s', `plandesc` = '%s', `planname` = '%s', `nacD` = '%s', `nacU` = '%s', `locD` = '%s', `locU` = '%s', `intD` = '%s', `intU` = '%s', `nacDS` = '%s', `nacDT` = '%s', `nacUS` = '%s', `nacUT` = '%s', `locDS` = '%s', `locDT` = '%s', `locUS` = '%s', `locUT` = '%s', `intDS` = '%s', `intDT` = '%s', `intUS` = '%s', `intUT` = '%s', `sysctl` = '%s', `ppp` = '%s' WHERE `id_plan` = '$getParam->idValue'",
				$monitor['plan'],
				$monitor['plandesc'],
				$monitor['planname'],
				$monitor['nacD'],
				$monitor['nacU'],
				$monitor['locD'],
				$monitor['locU'],
				$monitor['intD'],
				$monitor['intD'],
				$monitor['nacDS'],
				$monitor['nacDT'],
				$monitor['nacUS'],
				$monitor['nacUT'],
				$monitor['locDS'],
				$monitor['locDT'],
				$monitor['locUS'],
				$monitor['locUT'],
				$monitor['intDS'],
				$monitor['intDT'],
				$monitor['intUS'],
				$monitor['intUT'],
				$this->basic->limpiarMetas($monitor['sysctl']),
				$this->basic->limpiarMetas($monitor['ppp']));
				
				$plan_result = $this->conexion->query($update_sql);
				$idPlan = $getParam->idValue;
			}

			if($plan_result) {
				$result['status'] = true;
				
				foreach ($group as $value) {
					list($order,$groupId) = explode(":", $value);
					
					$insert_plan_group = sprintf("INSERT INTO `bm_plan_groups` (`id_plan`, `groupid`, `borrado`)
													VALUES (%s, %s, 0)",
													$idPlan,
													$groupId);
					
					$plan_group_result = $this->conexion->query($insert_plan_group,true);
					
					if(!$plan_group_result) {
						$result['status'] = false;
					}
				}
			} else {
				$result['status'] = false;
			}	
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(($getParam->idValue == 'clone') || (!is_numeric($getParam->idValue)))  {
				$this->protect->reg_event('create_plan',"Usuario creo el plan: [$idPlan] ".$monitor['plan']);
			} else {
				$this->protect->reg_event('edit_plan',"Usuario edito el plan: [$idPlan] ".$monitor['plan']);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		echo json_encode($result);
	}

	public function deletePlan()
	{
		$valida = $this->protect->access_page('CONFIG_DELETE_PLAN');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			foreach ($getParam->idSelect as $key => $value) {
				if(is_numeric($value)) {
					
					//Validando Plan:
					
					$get_plan_active_host_sql = "SELECT count(*) as Total FROM `bm_host` WHERE `id_plan` = '$value' AND `borrado`=0";
					
					$get_plan_active_host = $this->conexion->queryFetch($get_plan_active_host_sql);
					
					$get_plan_active_host = (int)$get_plan_active_host[0]['Total'];
					
					if($get_plan_active_host === 0) {
						$delete_sql = "UPDATE `bm_plan_groups` SET `borrado` = '1' WHERE `id_plan` = '$value' AND `groupid` = '$getParam->groupID';";
											
						$valida = $this->conexion->query($delete_sql);		
						
						if(!$valida) {
							$data['status'] = false;
						}else {
							$this->protect->reg_event('delete_plan',"Usuario borro el plan: [$value] del grupo $getParam->groupID");
						}
									
					} else {
						$data['status'] = false;
						$data['error'] = $this->language->SONDA_DELETE_BLOCK_HOST;
					}
				}
			}
		} else {
			$data['status'] = false;
			$data['error'] = $this->language->access_deny;
		}
		echo json_encode($data);
	}

	public function getPlanForm()
	{
		if(is_numeric($_POST['IDSelect'])) {
			$IDSelect = $_POST['IDSelect'];
		} else {
			return false;
		}

		$valida = $this->protect->access_page('CONFIG_EDIT_PLAN');
		
		$this->plantilla->load_sec('configuracion/config_planes_form', 'denegado', $valida);

		// Detalle Sonda
		
		$select_sql = "SELECT * FROM `bm_plan`  WHERE `id_plan` = $IDSelect";
						
		$select_result = $this->conexion->queryFetch($select_sql);

		if($select_result) {
			$select_rows = $select_result[0];
			foreach ($select_rows as $key => $row) {
				$value["plan_".$key] = utf8_encode($row);
			}
		}
		
		//Grupos
		
		$group_permit_user = $this->bmonitor->getGroupsHost(true,true);
		
		$get_grupos_active = $this->conexion->queryFetch("SELECT  DISTINCT HG.`groupid` , HG.`name` , 'selected' as selected
				FROM `bm_host_groups` HG
				LEFT OUTER JOIN `bm_plan_groups` PG ON HG.`groupid`=PG.`groupid` WHERE HG.`groupid` IN $group_permit_user AND PG.`id_plan` = $IDSelect");
				
		$group_select = $get_grupos_active[0]['groupid'];
				
		$get_grupos_inactive = $this->conexion->queryFetch("SELECT  HG.`groupid` , HG.`name`, '' as selected FROM `bm_host_groups` HG WHERE HG.`groupid` IN $group_permit_user AND HG.`groupid` != $group_select");
		
		//$get_grupos_activos = $this->basic->arrayKeyToValue($get_grupos_activos, 'groupid', 'name');
		
		$get_grupos = array_merge($get_grupos_active,$get_grupos_inactive);
		
		if($get_grupos) {
			$value["menu_groups_inactive"] = '';
			foreach ($get_grupos as $key => $grupo) {				
				$value["menu_groups_inactive"] .= '<option '.$grupo['selected'].' value="'.$grupo['groupid'].'">'.$grupo['name'].'</option>';
			}
		}
		
		$value["planes_id_menu"] = 'planes_groups_edit';
		$value["config_planes_id_form"] = 'config_planes_form_edit';
		
		//Ocultando cosas hho jojo
		if($_SESSION['name'] != 'Administrator') {
			$value["plan_sysctl_disable"] = 'disabled="disabled"';
			$value["plan_ppp_disable"]  = 'disabled="disabled"';	
		}
		
		$this->plantilla->set($value);
		echo $this->plantilla->get();
	}

	/*******
	 * 
	 * Grupos
	 * 
	 */
	
	public function cfgGrupos()
	{
		$value = array();
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_groups_form");

		/*
		$input = "<option value='0'>Default</option>\n";
		for ($i=-11; $i < 12; $i++) {
			$val = "UTC".$i;
			if($val == 'UTC0') {
				$val = 'UTC';
			}
			if($i > 0) {
				$val = "UTC+".$i; 
			}
			$input .= "<option value='$val'>$val</option>\n";
		}
		
		$value["option_timezone"] = $input; */
		
		$select_sql = 'SELECT  `id_template`, `name` FROM `bm_template_config` ';
		$select_result = $this->conexion->queryFetch($select_sql);
		$value["option_template"] = $this->basic->getOption($select_result, 'id_template', 'name');
		
		$value["option_type"] = $this->basic->getOptionValue('type_group','select');
		
		$value["form_id_group"] = 'form_new_group';
		
		$this->plantilla->set($value);
		
		$value["form_new_group"] = $this->plantilla->get();
		
		//Tabla
		
		$this->basic->setTableId('table_groups');
		$this->basic->setTableUrl("/config/getTableGroups");
		$this->basic->setTableColModel('config_groups');
		$this->basic->setTableToolbox('toolboxGroups');
		
		$value["table_groups"] = $this->basic->getTableFL('Grupos');
		
		//Generar Plantilla
		
		$valida = $this->protect->access_page('CONFIG_GROUPS_LIST');
		
		$this->plantilla->load_sec("configuracion/config_groups", "denegado",$valida);
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getTableGroups()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'name');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
				
		//Filtros
		
		$getGroups_in = $this->bmonitor->getGroupsHost(true,true);
		
		
		//Total rows
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total
				FROM `bm_host_groups` HG
				WHERE HG.`groupid` IN $getGroups_in AND HG.`borrado`=0";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}
		
		//Rows
		
		$getRows_sql =	"SELECT * FROM `bm_host_groups` HG 
							WHERE HG.`groupid` IN $getGroups_in AND HG.`borrado`=0 $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);
		
		if($getRows_result) {
			foreach ($getRows_result as $key => $row) {
				
				$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
				$option .= '<span id="toolbarSet">
								<button id="editGroup" onclick="editar('.$row['groupid'].',false)" name="editGroup">Editar</button>
								<button id="clonarGroup" onclick="editar('.$row['groupid'].',true)" name="clonarGroup">Clonar</button>
							</span>';
				$option .= '</span>';

				$data['rows'][] = array(
					'id' => $row['groupid'],
					'cell' => array($row['name'], $row['type'],$option)
				);
			}			
		}
		echo json_encode($data);
	}
	/*
	public function createGroup() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_GROUP');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			//Categoriza resultado	
			
			$monitor = array();
						
			
			$this->conexion->InicioTransaccion();
			
			if((!is_numeric($getParam->id)))  {
				//Generando nuevo registro
				$insert_sql = sprintf("INSERT INTO `bm_host_groups` (`name`, `type`, `snmp_monitor`, `timezone`)
											VALUE ('%s', '%s', '%s', '%s')",
											$getParam->group_name,
											$getParam->group_type,
											$getParam->group_snmp_monitor,
											$getParam->group_timezone
											);
				$sql_result = $this->conexion->query($insert_sql);
				$idInsert = $this->conexion->lastInsertId();
			} else {
				//Actualizando registro
				$update_sql = sprintf("UPDATE `bm_host_groups` SET `name` = '%s', `type` = '%s', `snmp_monitor` = '%s', `timezone` = '%s' WHERE `groupid` = '$getParam->id'",
				$getParam->group_name,
				$getParam->group_type,
				$getParam->group_snmp_monitor,
				$getParam->group_timezone);
				
				$sql_result = $this->conexion->query($update_sql);
				$idInsert = $getParam->idValue;
			}

			if($sql_result) {
				$result['status'] = true;
			} else {
				$result['status'] = false;
			}	
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(($getParam->idValue == 'clone') || (!is_numeric($getParam->idValue)))  {
				$this->protect->reg_event('create_group',"Usuario creo el grupo: [$idInsert] ".$getParam->group_name);
			} else {
				$this->protect->reg_event('edit_group',"Usuario edito el grupo: [$getParam->id] ".$getParam->group_name);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		echo json_encode($result);
	}*/

	public function createGroup()
	{
		$getParam = (object)$_POST;
		
		$valida = $this->protect->access_page('CONFIG_CREATE_GROUP');

		$result['status'] = true;
		
		if($valida) {
			$getParam = (object)$_POST;
			
			if(!is_numeric($getParam->id)) {
						
				foreach ($getParam as $key => $value) {
					if(preg_match("/^template_/i",$key) || preg_match("/^group_/i",$key)) {
						$keyName = explode("_", $key,2);
						$column[] = '`'.$keyName[1].'`';
					
						$value = "'".$value."'";
						
						$valueC[] = $value;
						$config[$keyName[1]] = $value;
						
					} 
				}
				
				$column = join(',', $column);
				$valueC = join(',', $valueC);
				
				$insert_sql = 'INSERT INTO `bm_host_groups` ('.$column.') VALUES ('.$valueC.');';
				
				$valida = $this->conexion->query($insert_sql);
				
				if(!$valida) {
					$result['status'] = false;
				}
			} else {
				
				foreach ($getParam as $key => $value) {
					if(preg_match("/^template_/i",$key) || preg_match("/^group_/i",$key)) {
						$keyName = explode("_", $key,2);

						$update[] = '`'.$keyName[1].'` = '."'".$value."'";
					}
				}
				
				
				$update_sql = "UPDATE `bm_host_groups` SET ".join(',', $update)." WHERE `groupid` = '$getParam->id';";
				
				$update_result = $this->conexion->query($update_sql);
				
				if(!$update_result) {
					$result['status'] = false;
				}			

			}	
		}
		
		echo json_encode($result);
		exit;
	}

	public function deleteGroups()
	{
		$valida = $this->protect->access_page('CONFIG_DELETE_GROUP');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			foreach ($getParam->idSelect as $key => $value) {
				if(is_numeric($value)) {
					
					//Validando Plan:
					
					$get_active_host_sql = "SELECT count(*) as Total FROM `bm_host` H WHERE H.`groupid` = '$value' AND H.`borrado`=0";
					
					$get_active_host = $this->conexion->queryFetch($get_active_host_sql);
					
					$get_active_host = (int)$get_active_host[0]['Total'];
					
					if($get_active_host === 0) {
						$delete_sql = "UPDATE `bm_host_groups` SET `borrado` = '1' WHERE `groupid` = '$value';";
											
						$valida = $this->conexion->query($delete_sql);		
						
						if(!$valida) {
							$data['status'] = false;
						}else {
							$this->protect->reg_event('delete_groups',"Usuario borro el grupo: [$value]");
						}
									
					} else {
						$data['status'] = false;
						$data['error'] = $this->language->GRUPO_DELETE_BLOCK_HOST;
					}
				}
			}
		} else {
			$data['status'] = false;
			$data['error'] = $this->language->access_deny;
		}
		echo json_encode($data);
	}

	public function getFormTemplate()
	{
		$getParam = (object)$_POST;
		
		$this->plantilla->load('configuracion/config_groups_template');
		
		$select_sql = 'select * FROM `bm_template_config` WHERE `id_template` = '.$getParam->id_template;
		$select_result = $this->conexion->queryFetch($select_sql);

		foreach ($select_result[0] as $key => $graph) {
			$value['template_'.$key] = $graph;
		}

		$value["form_id_template"] = 'form_edit_template';
		
		$this->plantilla->set($value);
			
		$data['datos'] = $this->plantilla->get();
		$data['status'] = true;
		
		echo json_encode($data);
	}
	
	public function getGroupForm()
	{
		if(is_numeric($_POST['IDSelect'])) {
			$IDSelect = $_POST['IDSelect'];
		} else {
			return false;
		}
				
		$value["form_id_group"] = 'form_edit_group';
		
		// Detalle Grupo
		
		$monitor_sql = "SELECT HG.*,TC.`name` AS Template FROM `bm_host_groups` HG  LEFT JOIN `bm_template_config` TC USING(`id_template`) WHERE HG.`groupid` = $IDSelect";
						
		$monitor_result = $this->conexion->queryFetch($monitor_sql);

		if($monitor_result) {
			$monitor_rows = $monitor_result[0];
			foreach ($monitor_rows as $key => $monitor) {
				$value["group_".$key] = $monitor;
			}
		}
		
		/*
		$input = "<option value='0'>Default</option>\n";
		for ($i=-11; $i < 12; $i++) {
			$val = "UTC".$i;
			if($val == 'UTC0') {
				$val = 'UTC';
			}
			if($i > 0) {
				$val = "UTC+".$i;
			}
			
			$value["group_timezone"];
			if((string)$value["group_timezone"] == (string)$val) {
				$input .= "<option selected value='$val'>$val</option>\n";
			} else {
				$input .= "<option value='$val'>$val</option>\n";
			}
			
		} */
		
		//Plantillas
		
		$this->plantilla->load("configuracion/config_groups_template");

		// Value Plantilla

		foreach ($monitor_rows as $key => $graph) {
			$valueTemplate['template_'.$key] = $graph;
		}
		
		$this->plantilla->set($valueTemplate);
		
		$value["tabs_new_group_tabs_2_menu"] = '<li><a href="#new_group_tabs_2">'.$monitor_rows['name'].'</a></li>';
		
		$value["tabs_new_group_tabs_2"] = '<div id="new_group_tabs_2">'.$this->plantilla->get().'</div>';
		
		
		//Op Grupos
		
		$value["option_template"] = '<option value="'.$valueTemplate['template_id_template'].'">'.$valueTemplate['template_Template'].'</option>';
		
		$value["option_type"] = $this->basic->getOptionValue('type_group',$value["group_type"]);		

		
		if($value["group_snmp_monitor"] === 'true') {
			$value["group_snmp_monitor"] = 'checked';
		} else {
			$value["group_snmp_monitor"] = '';
		}
		
		$valida = $this->protect->access_page('CONFIG_EDIT_GROUP');
		$this->plantilla->load_sec('configuracion/config_groups_form', 'denegado', $valida);
		$this->plantilla->set($value);
		echo $this->plantilla->get();
	}

	/*******
	 * 
	 * Graficos 
	 * 
	 */
	
	public function cfgGraph()
	{
		$value = array();
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_graph_form");
		
		//Items
		
		$get_items_sql = 'SELECT `id_item`, `name`  FROM `bm_items`';
		$items = $this->conexion->queryFetch($get_items_sql);
		
		if($items) {
			$value["menu_items_inactive"] = '';
			foreach ($items as $key => $item) {				
				$value["menu_items_inactive"] .= '<option value="'.$item['id_item'].'">'.$item['name'].'</option>';
			}
		}
		
		$value["graph_id_menu"] = 'graph_items';
		$value["config_graph_id_form"] = 'config_graph_form';
		
		$this->plantilla->set($value);
		
		$value["form_new_graph"] = $this->plantilla->get();
		
		//Tabla
		
		$this->basic->setTableId('table_graph');
		$this->basic->setTableUrl("/config/getTableGraph");
		$this->basic->setTableColModel('config_graph');
		$this->basic->setTableToolbox('toolboxGraph');
		
		$value["table"] = $this->basic->getTableFL('Gráficos');
		
		//Generar Plantilla
		
		$valida = $this->protect->access_page('CONFIG_GRAPH_LIST');
		
		$this->plantilla->load_sec("configuracion/config_graph", "denegado",$valida);
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getTableGraph()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'name');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
		//Total rows
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total FROM `bm_graphs`";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}
		
		//Rows
		
		$getRows_sql =	"SELECT * FROM `bm_graphs` $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);
		
		if($getRows_result) {
			foreach ($getRows_result as $key => $row) {
				$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
				$option .= '<span id="toolbarSet"><button id="editGraph" onclick="editGraph('.$row['id_graph'].',false)" name="editGraph">Editar</button></span>';
				$option .= '</span>';
				
				$data['rows'][] = array(
					'id' => $row['id_graph'],
					'cell' => array($row['name'], $row['width'],$row['height'],$row['graphtype'],$option)
				);
			}
		}
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}

	public function createGraph() 
	{
		$valida = $this->protect->access_page('CONFIG_CREATE_GRAPH');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			$result['status'] = false;
			
			if(!is_numeric($getParam->id)) {
				/* Creando nuevo Grafico */ 
				
				$this->conexion->InicioTransaccion();
				
				$insert_sql = sprintf("INSERT INTO `bm_graphs` (`name`, `width`, `height`, `graphtype`)
											VALUE ('%s', '%s', '%s', '%s')",
											$getParam->graph_name,
											$getParam->graph_width,
											$getParam->graph_height,
											$getParam->graph_graphtype);
											
				$sql_result = $this->conexion->query($insert_sql);
				$idInsert = $this->conexion->lastInsertId();
				
				if($sql_result) {
						
					$insert_sql = 'INSERT INTO `bm_graphs_items` (`id_graph`, `id_item`) VALUES  ';
					
					$insert_value = array();
					
					
					foreach ($getParam->graph_items as $key => $items) {
						$insert_value[] = "($idInsert,$items)";
					}
				
					$insert_value = join(',',$insert_value);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_value);
					
					if($sql_result) {
						$result['status'] = true;
					}
				}
			} else {
				/* Editando Grafico */ 
				$this->conexion->InicioTransaccion();
				
				$update_sql = sprintf(/* BSW */ "UPDATE `bm_graphs` SET `name` = '%s', `width` = '%s', `height` = '%s', `graphtype` = '%s' WHERE `id_graph` = '%s'",
							$getParam->graph_name,
							$getParam->graph_width,
							$getParam->graph_height,
							$getParam->graph_graphtype,
							$getParam->id);
											
				$update_result = $this->conexion->query($update_sql);
				
				if($update_result) {
					
					$delete_sql = "/* BSW  */ DELETE FROM `bm_graphs_items` WHERE `id_graph` = '$getParam->id';";
					$delete_result = $this->conexion->query($delete_sql);
					
					$insert_sql = 'INSERT INTO `bm_graphs_items` (`id_graph`, `id_item`) VALUES  ';
					
					$insert_value = array();
					
					foreach ($getParam->graph_items as $key => $items) {
						$insert_value[] = "($getParam->id,$items)";
					}
				
					$insert_value = join(',',$insert_value);
					
					$sql_result = $this->conexion->query($insert_sql.$insert_value);
					
					if($sql_result) {
						$result['status'] = true;
					}
				}
			}
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(!is_numeric($getParam->id))  {
				$this->protect->reg_event('new_graph',"Usuario creo el grafico: [$idInsert] ".$getParam->graph_name);
			} else {
				$this->protect->reg_event('edit_graph',"Usuario edito el grafico: [$getParam->id] ".$getParam->graph_name);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}

	public function deleteGraph()
	{
		$valida = $this->protect->access_page('CONFIG_DELETE_GRAPH');
		if($valida) {
			$getParam = (object)$_POST;
											
			$graph_delete = $this->conexion->arrayToIN($getParam->idSelect,false,true);					
													
			$delete_sql =  "DELETE FROM `bm_graphs` WHERE `id_graph` IN $graph_delete";
			
			$delete_result = $this->conexion->query($delete_sql);
			
			if($delete_result){
				$result['status'] = true;
				echo json_encode($result);
				exit;
			}
			else {
				$result['status'] = false;
				$result['error'] = $this->language->ERROR_DELETE_GRAPH_CONFIG;
				echo json_encode($result);
				exit;
			}

		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}	
	}
	
	public function getGraphForm()
	{
		$getParam = (object)$_POST;
		
		if(is_numeric($getParam->IDSelect)) {
	
			//Formulario nuevo 
			$this->plantilla->load("configuracion/config_graph_form");
			
			// Value Graph
			
			$select_sql = 'SELECT `name`,`width`,`height`,`graphtype` FROM  `bm_graphs` WHERE `id_graph` = '.$getParam->IDSelect;
			$select_result = $this->conexion->queryFetch($select_sql);
	
			foreach ($select_result[0] as $key => $graph) {
				$value['graph_'.$key] = $graph;
			}
			
			//Items
			
			$get_items_sql = 'SELECT 
					I.`id_item`, 
					I.`name`, 
					IF(GI.`id_graph` IS NULL, 0, 1) AS selected  
				FROM `bm_items` I LEFT OUTER JOIN `bm_graphs_items` GI USING(`id_item`)
				WHERE ( GI.`id_graph` = '.$getParam->IDSelect.' OR GI.`id_graph` IS NULL ) 
				ORDER BY I.`name`';
				
			$items = $this->conexion->queryFetch($get_items_sql);
			
			if($items) {
				$value["menu_items_inactive"] = '';
				foreach ($items as $key => $item) {
					if((int)$item['selected'] === 1 ) {
						$SELECTED = 'selected';
					} else {
						$SELECTED = '';
					}
							
					$value["menu_items_inactive"] .= '<option '.$SELECTED.' value="'.$item['id_item'].'">'.$item['name'].'</option>';
				}
			}
			
			$value["graph_id_menu"] = 'graph_items_edit';
			$value["config_graph_id_form"] = 'config_graph_form_edit';
			
			$this->plantilla->set($value);
			
			echo  $this->plantilla->get();
		
		}
	}

	public function cfgUbicacion()
	{
		$value = array();
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_ubicacion_form");
		
		//Items
		
		$value["option_region"] = $this->basic->getLocalidadOption('region');
		$value["option_provincia"] = '<option selected value="0">'.$this->language->SELECT.' Region</option>';
		$value["option_comuna"] = '<option selected value="0">'.$this->language->SELECT.' Provincia</option>';
		$value["option_plan"] = $this->bmonitor->getAllPlan('option',false,false,'ENLACES');
		
		$value["config_id_form"] = 'config_ubicacion_form_new';
		
		$this->plantilla->set($value);
		
		$value["form_new_ubicacion"] = $this->plantilla->get();
		
		//Tabla
		
		$this->basic->setTableId('table_ubicacion');
		$this->basic->setTableUrl("/config/getTableUbicaciones");
		$this->basic->setTableColModel('config_ubicacion');
		$this->basic->setTableToolbox('toolboxUbicacion');
		$this->basic->setTableSearch('Region','REGION_NOMBRE');
		$this->basic->setTableSearch('Provincia','PROVINCIA_NOMBRE');
		$this->basic->setTableSearch('Comuna','COMUNA_NOMBRE');
		$this->basic->setTableSearch('Código localidad','CO.rdb');
		$this->basic->setTableSearch('Nombre','CO.establecimiento');
		
		$value["table"] = $this->basic->getTableFL('Ubicación');
		
		//Generar Plantilla
		
		$valida = $this->protect->access_page('CONFIG_LOCATION_LIST');
		
		$this->plantilla->load_sec("configuracion/config_ubicacion", "denegado",$valida);
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getTableUbicaciones()
	{
		$getParam = (object)$_POST;
		
		//Order
		
		if($_POST['sortname'] == 'region'){
			$_POST['sortname'] = 'CO.REGION_ID';
		}
		
		if($_POST['sortname'] == 'provincia'){
			$_POST['sortname'] = 'CO.PROVINCIA_ID';
		}
		
		if($_POST['sortname'] == 'comuna'){
			$_POST['sortname'] = 'CO.COMUNA_ID';
		}
		
		if($_POST['sortname'] == 'name'){
			$_POST['sortname'] = 'establecimiento';
		}
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'CO.REGION_ID');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
		//Total rows
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total FROM `bm_colegios` CO LEFT JOIN `bm_comuna` C ON CO.`COMUNA_ID`=C.`COMUNA_ID`
							LEFT JOIN `bm_provincia` P ON CO.`PROVINCIA_ID` = P.`PROVINCIA_ID`
							LEFT JOIN `bm_region` R ON CO.`REGION_ID`=R.`REGION_ID` $var->searchSql";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}
		
		//Rows
		
		$getRows_sql =	"SELECT * FROM `bm_colegios` CO
							LEFT JOIN `bm_comuna` C ON CO.`COMUNA_ID`=C.`COMUNA_ID`
							LEFT JOIN `bm_provincia` P ON CO.`PROVINCIA_ID` = P.`PROVINCIA_ID`
							LEFT JOIN `bm_region` R ON CO.`REGION_ID`=R.`REGION_ID` $var->searchSql $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);
		
		if($getRows_result) {
			foreach ($getRows_result as $key => $row) {
				$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
				$option .= '<span id="toolbarSet"><button id="editUbicacion" onclick="edit('.$row['id_colegio'].',false)" name="editUbicacion">Editar</button></span>';
				$option .= '</span>';
				
				$data['rows'][] = array(
					'id' => $row['id_colegio'],
					'cell' => array(utf8_encode($row['establecimiento']), utf8_encode($row['REGION_NOMBRE']),utf8_encode($row['PROVINCIA_NOMBRE']),utf8_encode($row['COMUNA_NOMBRE']),$option)
				);
			}
		}
		header("Content-type: application/json");
		echo json_encode($data);
		exit;
	}

	public function ubicacion()
	{
		$getParam = (object)$_POST;

		echo $this->basic->getLocalidadOption($getParam->type,$getParam->id);
	}

	public function getUbicacionForm()
	{
		$getParam = (object)$_POST;
		
		//Formulario nuevo 
		$this->plantilla->load("configuracion/config_ubicacion_form");
		
		$value["config_id_form"] = 'config_ubicacion_form_edit';
		
		//Get value Colegio
		$value_result = $this->conexion->queryFetch("SELECT * FROM `bm_colegios` WHERE `id_colegio`=?",$getParam->IDSelect);
		
		if($value_result) {
			$colegio = (object)$value_result[0];
		} else {
			header("Content-type: application/json");
			$result["status"] = false;
			echo json_encode($result);
			exit;			
		}
		
		foreach ($value_result[0] as $ckey => $cvalue) {
			$value[$ckey] = $cvalue;
		}
		
		//Items
		
		$value["option_region"] = $this->basic->getLocalidadOption('region',false,$colegio->REGION_ID);
		$value["option_provincia"] = $this->basic->getLocalidadOption('provincia',$colegio->REGION_ID,$colegio->PROVINCIA_ID);
		$value["option_comuna"] = $this->basic->getLocalidadOption('comuna',$colegio->PROVINCIA_ID,$colegio->COMUNA_ID);
		
		//Buscando Plan
		
		if((int)$colegio->id_plan === 0) {
			
			$select_sql = "SELECT * FROM `bm_plan` WHERE `plan` =  '?'";
		
			$select_result = $this->conexion->queryFetch($select_sql,$colegio->planfdt);
			
			if($select_result) {
				$value["option_plan"] = $this->bmonitor->getAllPlan('option',$select_result[0]['id_plan'],false,'ENLACES');
				
			} else {
				$value["option_plan"] = $this->bmonitor->getAllPlan('option',false,false,'ENLACES').'<option selected value="0">Plan [ '.$colegio->planfdt.' ] No registrado</option>';
			}
		} else {
			
			$value["option_plan"] = $this->bmonitor->getAllPlan('option',$colegio->id_plan,false,'ENLACES');
			
		}
		
		
		
		$this->plantilla->set($value);
		
		$result["html"] = $this->plantilla->get();
		$result['status'] = true;
		
		echo json_encode($result);
	}

	public function createUbicacion() 
	{
		$valida = $this->protect->access_page('CONFIG_LOCATION_CREATE');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			$result['status'] = false;
			
			foreach ($getParam as $key => $value) {
				if(preg_match("/^config_ubicacion_form/i",$key)) {
							$keyName = explode("_", $key,5);
							$param[$keyName[4]] = $value;
				}
			}
			
			$param = (object)$param;
		
			if(!is_numeric($getParam->id)) {
				/* Creando nuevo Grafico */ 
				
				$this->conexion->InicioTransaccion();
				
				$insert_sql = sprintf(/* BSW */ "INSERT INTO `bm_colegios` (`sostenedor`, `establecimiento`, `rdb`, `direccion`, `rut`, `tarifa`, `nodo`, `cuadrante`, `viviendaid`, `planfdt`, `REGION_ID`, `PROVINCIA_ID`, `COMUNA_ID`, `id_plan`) 
													VALUES ('%s', '%s', %s, '%s', '%s', '%s', %s, '%s', '%s', %s, '%s', %s, %s, %s)",
													$param->sostenedor,
													$param->establecimiento,
													$param->rdb,
													$param->direccion,
													$param->rut,
													$param->tarifa,
													$param->nodo,
													$param->cuadrante,
													$param->viviendaid,
													$param->planfdt,
													$param->REGION_ID,
													$param->PROVINCIA_ID,
													$param->COMUNA_ID,
													$param->planfdt);
											
				$sql_result = $this->conexion->query($insert_sql,false,'logs_config');
				
				$idInsert = $this->conexion->lastInsertId();
				
				if($sql_result) {
					$result['status'] = true;
				}
			} else {
				/* Editando Grafico */ 
				$this->conexion->InicioTransaccion();
				
				$update_sql = sprintf(/* BSW */ "UPDATE `bm_colegios` SET `sostenedor` = '%s', `establecimiento` = '%s', `rdb` = '%s', `direccion` = '%s', `rut` = '%s', `tarifa` = '%s', `nodo` = '%s', `cuadrante` = '%s', `viviendaid` = '%s', `planfdt` = '%s', `REGION_ID` = '%s', `PROVINCIA_ID` = '%s', `COMUNA_ID` = '%s', `id_plan` = '%s' WHERE `id_colegio` = '%s'",
							$param->sostenedor,
							$param->establecimiento,
							$param->rdb,
							$param->direccion,
							$param->rut,
							$param->tarifa,
							$param->nodo,
							$param->cuadrante,
							$param->viviendaid,
							$param->planfdt,
							$param->REGION_ID,
							$param->PROVINCIA_ID,
							$param->COMUNA_ID,
							$param->planfdt,
							$getParam->id);

				$update_result = $this->conexion->query($update_sql);
				
				if($update_result) {
					$result['status'] = true;
				}
				
			}
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}
		
		if($result['status']) {
			if(!is_numeric($getParam->id))  {
				$this->protect->reg_event('new_ubicacion',"Usuario creo la Ubicacion: [$idInsert] ".$param->establecimiento);
			} else {
				$this->protect->reg_event('edit_ubicacion',"Usuario edito la Ubicacion: [$getParam->id] ".$param->establecimiento);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		
		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}

	public function deleteubicacion()
	{
		$valida = $this->protect->access_page('CONFIG_LOCATION_DELETE');
		if($valida) {
			$getParam = (object)$_POST;
											
			$reg_delete = $this->conexion->arrayToIN($getParam->idSelect,false,true);					
													
			$delete_sql =  "DELETE FROM `bm_colegios` WHERE `id_colegio` IN $reg_delete";
			
			$delete_result = $this->conexion->query($delete_sql);
			
			if($delete_result){
				$result['status'] = true;
				echo json_encode($result);
				exit;
			}
			else {
				$result['status'] = false;
				$result['error'] = $this->language->ERROR_DELETE_LOCATION_CONFIG;
				echo json_encode($result);
				exit;
			}

		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			echo json_encode($result);
			exit;
		}	
	}
	
}
?>