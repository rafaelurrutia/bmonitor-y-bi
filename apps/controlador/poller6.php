<?php 

class poller6 extends Control {


	public function pollerMarca()
	{
		//Poller pendientes
		$get_sql = "SELECT DISTINCT HG.`delay`  FROM `bm_host_groups` HG LEFT OUTER JOIN `bm_poller`  P ON HG.`delay`=P.`delay` WHERE P.`id_poller` IS NULL AND HG.`delay` != 0";
		
		$get_result = $this->conexion->queryFetch($get_sql);
		
		if($get_result && (count($get_result) > 0)) {
			
			foreach ($get_result as $key => $value) {
				
				$insert_poller_sql = "INSERT INTO `bm_poller` (`uptime`, `delay`, `item_poller`, `fecha_update`, `startcheck`, `nextcheck`)
										VALUES ( UNIX_TIMESTAMP(NOW()) , ?, 0, NOW(), NOW(), NOW() + `delay`)";
										
				$this->conexion->queryFetch($insert_poller_sql,$value['delay']);
			}
		}
		
		//Validando ejecucion poller 
		
		$AVAILABILITY_GROUP = $this->parametro->get('AVAILABILITY_GROUP',true);
		
		if($AVAILABILITY_GROUP) {
			$FILTER = 'DELAY_GROUP';
		} else {
			$FILTER = 'DELAY_ITEM';
		}

		//Item pendientes:
		
		//$get_zero_poller_sql = "SELECT `delay` FROM `bm_poller` WHERE `nextcheck` <= NOW() AND `item_poller` = 0";
		$get_zero_poller_sql = "SELECT `delay` FROM `bm_poller` WHERE `nextcheck` <= NOW()";
		
		$get_zero_poller_result = $this->conexion->queryFetch($get_zero_poller_sql);
		
		if($get_zero_poller_result) {
			
			foreach ($get_zero_poller_result as $key => $value) {
				
				$get_item_pendientes_sql = "SELECT COUNT(DISTINCT H.`id_host`) AS TOTAL, item.`delay` AS DELAY_ITEM, HG.`delay` AS DELAY_GROUP
											FROM `bm_host` H
												LEFT OUTER JOIN `bm_item_profile` P USING(`id_host`)
												LEFT JOIN `bm_items` item ON P.`id_item`=item.`id_item`
												LEFT JOIN `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND H.`groupid`=IG.`groupid` )
												LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=H.`groupid`
												WHERE ( P.`id_item` = 1 OR P.`id_item` IS NULL ) AND H.`borrado`=0 AND IG.`status`= 1 AND P.`nextcheck`  < NOW() 
												GROUP BY HG.`delay` HAVING $FILTER =".$value['delay'];
				
				$get_item_pendientes_result = $this->conexion->queryFetch($get_item_pendientes_sql);
				
				if($get_item_pendientes_result) {
					$item_count = (int)$get_item_pendientes_result[0]['TOTAL'];
					$update_count_item_sql = "UPDATE `bm_poller` SET `item_poller` = $item_count, `duration_poller` = ( UNIX_TIMESTAMP(NOW()) - `uptime`), `uptime` = UNIX_TIMESTAMP(NOW())  ,`startcheck` = NOW(), `fecha_update` = NOW() , `nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND)  WHERE `delay` = ".$value['delay'];
					$update_count_item_result = $this->conexion->query($update_count_item_sql);
				}
				
			}
			
		} 
		
		$nextcheck_sql = "SELECT `uptime`,`delay`,`nextcheck` FROM `bm_poller`";
		$nextcheck_result = $this->conexion->queryFetch($nextcheck_sql);
		if($nextcheck_result) {
			foreach ($nextcheck_result as $key => $value) {
				$utime_delay[$value['delay']]['uptime'] = $value['uptime'];
				$utime_delay[$value['delay']]['nextcheck'] = $value['nextcheck'];
			}
			return $utime_delay;
		} else {
			return false;
		}
	}

	public function validActive($poller)
	{
		$limit_max = $this->parametro->get('GET_MAX_POLLER',10);
		
		if(($poller === "process") || ($poller === "processr" )) {
			if($poller == 'process') $poller_name = 'POLLER_INDEX';
			else $poller_name = 'POLLER_RETRY';
		} else {
			return false;
		}
		
		$getProcessActive_sql = "SELECT count(*) as active FROM `bm_item_profile` 
			LEFT JOIN `bm_items` USING(`id_item`) 
			WHERE `type_poller`='snmp'  AND `status`='$poller'
			ORDER BY `id_host`,`id_item`;"; 
			
		$getProcessActive_result = $this->conexion->queryFetch($getProcessActive_sql);
		
		if($getProcessActive_result) {
			
			$limit_active = $getProcessActive_result[0]['active'];
			
			$limit_permit = $limit_max - $limit_active;
			
			if($limit_permit > 0) {
				$this->logs->info("[$poller_name] Iniciando poller con un limite de [$limit_active/$limit_max] hilos",NULL,'logs_poller');
				return $limit_permit;
			} else {
				$this->logs->error("[$poller_name] Poller inicial supero el maximo permitido:",$limit_max,'logs_poller');
				return false;
			}

		} else {
			return false;
		}
	}
	
	public function index()
	{
		$limit = $this->validActive('process');
		
		if(!$limit) {
			echo "NOK";
			exit;
		}
		
		$uptime_delay = $this->pollerMarca();
		
		if(!$uptime_delay) {
			exit;
		}
		
		$sql_host = "SELECT  profile.`id_item_profile`, host.`host`, host.`id_host`, host.`dns`, item.`id_item`, item.`delay` , item.`delay` AS DELAY_ITEM, HG.`delay` AS DELAY_GROUP , item.`type_item`, item.`description` , host.`ip_wan` , item.`snmp_oid`, item.`snmp_community`, item.`snmp_port`
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			LEFT JOIN  `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND host.`groupid`=IG.`groupid` )
			LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=host.`groupid`
			WHERE host.`borrado`=0 AND IG.`status`= 1 AND host.`status` = 1 AND profile.`nextcheck`  < NOW()  AND item.`type_poller` = 'snmp' AND ( ( profile.`status`='pendiente' OR profile.`status`='ok' )  OR ( profile.`nextcheck`  < DATE_SUB(NOW(), INTERVAL  item.`delay`*3 SECOND) AND profile.`status`='process'  ) ) LIMIT ".$limit;
			
		$result_host = $this->conexion->queryFetch($sql_host);

		$totalHijos = 	count($result_host);
	
		$AVAILABILITY_GROUP = $this->parametro->get('AVAILABILITY_GROUP',true);
		
		if($AVAILABILITY_GROUP) {
			$FILTER = 'DELAY_GROUP';
		} else {
			$FILTER = 'DELAY_ITEM';
		}
		
		if($totalHijos >= 1){
			$this->logs->info("[POLLER_INDEX] Cargando un total de $totalHijos hilos",NULL,'logs_poller');
			
			$result = array();
			
			
			foreach ($result_host as $key => $host) {
					
				$getParam = (object)array();

				$getParam->type_poller = 'index';
				$getParam->pipe = $key;
				if(isset($uptime_delay[$FILTER])){
					$getParam->uptime = $uptime_delay[$FILTER]['uptime'];
					$getParam->nextcheck = $uptime_delay[$FILTER]['nextcheck'];
				} else {
					$getParam->uptime = time();
					$getParam->nextcheck = $getParam->uptime+$host[$FILTER];
				}
				$getParam->host = $host['host'];
				$getParam->id_host = $host['id_host'];
				$getParam->dns = $host['dns'];
				$getParam->id_item = $host['id_item'];
				$getParam->type_item = $host['type_item'];
				$getParam->description = $host['description'];
				$getParam->ip_wan = $host['ip_wan'];
				$getParam->snmp_oid = $host['snmp_oid'];
				$getParam->snmp_community = $host['snmp_community'];
				$getParam->snmp_port = $host['snmp_port'];
	
				$result[] = array(
					"id" => $host['id_item_profile'],
					"param" => $getParam
				);
			}

			return $result;
		} else {
			$this->logs->debug("[POLLER_INDEX] No hay host disponibles",NULL,'logs_poller');
			return false;
		}
	}
	
	public function retry()
	{
		$limit = $this->validActive('processr');
		
		if(!$limit) {
			exit;
		}
	
		$uptime_delay = $this->pollerMarca();
		
		if(!$uptime_delay) {
			exit;
		}
		
		$sql_host = "SELECT  profile.`id_item_profile`, host.`host`, host.`id_host`, host.`dns`, item.`id_item`, item.`delay` , item.`delay` AS DELAY_ITEM, HG.`delay` AS DELAY_GROUP , item.`type_item`, item.`description` , host.`ip_wan` , item.`snmp_oid`, item.`snmp_community`, item.`snmp_port`
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			LEFT JOIN  `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND host.`groupid`=IG.`groupid` )
			LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=host.`groupid`
			WHERE host.`borrado`=0 AND IG.`status`= 1 AND host.`status` = 1 AND profile.`nextcheck`  < NOW()  AND item.`type_poller` = 'snmp' AND   (( profile.`status`='retry' OR profile.`status`='error' ) OR ( profile.`nextcheck`  < DATE_SUB(NOW(), INTERVAL  item.`delay`*3 SECOND) AND profile.`status`='processr'  ) ) LIMIT ".$limit;
			
		$result_host = $this->conexion->queryFetch($sql_host);

		$totalHijos = count($result_host);
		
		$AVAILABILITY_GROUP = $this->parametro->get('AVAILABILITY_GROUP',true);
		
		if($AVAILABILITY_GROUP) {
			$FILTER = 'DELAY_GROUP';
		} else {
			$FILTER = 'DELAY_ITEM';
		}
		
		if($totalHijos >= 1){
			$this->logs->debug("[POLLER_RETRY] Cargando un total de $totalHijos hilos",NULL,'logs_poller');
			foreach ($result_host as $key => $host) {

				$getParam = (object)array();
					
				$getParam->type_poller = 'retry';
				$getParam->pipe = $key;
				if(isset($uptime_delay[$FILTER])){
					$getParam->uptime = $uptime_delay[$FILTER]['uptime'];
					$getParam->nextcheck = $uptime_delay[$FILTER]['nextcheck'];
				} else {
					$getParam->uptime = time();
					$getParam->nextcheck = $getParam->uptime+$host[$FILTER];
				}
				$getParam->host = $host['host'];
				$getParam->id_host = $host['id_host'];
				$getParam->dns = $host['dns'];
				$getParam->id_item = $host['id_item'];
				$getParam->type_item = $host['type_item'];
				$getParam->description = $host['description'];
				$getParam->ip_wan = $host['ip_wan'];
				$getParam->snmp_oid = $host['snmp_oid'];
				$getParam->snmp_community = $host['snmp_community'];
				$getParam->snmp_port = $host['snmp_port'];
	
				$result[] = array(
					"id" => $host['id_item_profile'],
					"param" => $getParam
				);
				
			}
			return $result;
		} else {
			$this->logs->debug("[POLLER_INDEX] No hay host disponibles",NULL,'logs_poller');
			return false;
		}
	}
	
	public function process($id_item,$getParam){
		
		if($getParam->type_poller == 'index') {
			$poler_log_text = '[POLLER_INDEX]';
			$status = 'process';
			$timeout = (int)$this->parametro->get("SNMP_TIMEOUT_POLLER",1000000);
			$retry = (int)$this->parametro->get("SNMP_RETRY_POLLER",0);
			$version = $this->parametro->get("SNMP_VERSION_POLLER",'2c');
		} else {
			$status = 'processr';
			$poler_log_text = '[POLLER_RETRY]';
			$timeout = (int)$this->parametro->get("SNMP_TIMEOUT_RETRY",2000000);
			$retry = (int)$this->parametro->get("SNMP_RETRY_RETRY",3);
			$version = $this->parametro->get("SNMP_VERSION_RETRY",'2c');
		}

		$this->logs->debug("$poler_log_text Hilo numero $getParam->pipe , activo",NULL,'logs_poller');
		
		if(!is_numeric($id_item)) {
			return false;
		}

		$update_query="INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`, `lastclock`, `lastvalue`, `prevvalue`, `error`, `retry`, `status`, `active`)
						VALUES ($getParam->id_item, $getParam->id_host, '$getParam->snmp_oid', $getParam->snmp_port, '$getParam->snmp_community', NOW(), NULL, NULL, NULL, NULL, 0, '$status', 'true')
						ON DUPLICATE KEY UPDATE  error=NULL, `status`='$status', `nextcheck` = NOW() ";
		
		//$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  `status`='$status' ,`nextcheck` = NOW() , `lastclock` =" . $getParam->uptime . ",prevvalue=lastvalue,lastvalue=0 where id_item=". $getParam->id_item." AND `id_host`=$getParam->id_host";
		$result = $this->conexion->query($update_query);

		$ip = $getParam->ip_wan.":".$getParam->snmp_port;

		switch ($getParam->type_item) {
			case 'float':
				$tablehistory="bm_history";
			break;
			
			case 'string':
				$tablehistory="bm_history_str";
			break;
			
			case 'text':
				$tablehistory="bm_history_text";
			break;
			
			case 'log':
				$tablehistory="bm_history_uint";
			break;
							
			default:
				$tablehistory="bm_history";
			break;
		}
							
		//Validamos si el modulo o paquete php_snmp se encuentra activo
		if (function_exists('snmp2_get')) {
			//Via modulo Inicio
			$time_start = microtime(true);
			$this->logs->debug("$poler_log_text Iniciando consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  ",NULL,'logs_poller');
			$result = @snmp2_get($ip,$getParam->snmp_community,$getParam->snmp_oid,$timeout,$retry);
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			$this->logs->debug("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  , tiempo: $time respondio: ",$result,'logs_poller');
			if($result){
				
				list($tipoValorSnmp, $valorSnmp) = explode(": ", $result);
				
				//validaciones 
				
				$validResultSnmp = strpos($valorSnmp, 'uci');
					
				if($validResultSnmp !== false) {
					$this->logs->error("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid] respondio con Error",NULL,'logs_poller');
					$valorSnmp = 'UCI : Entry not found';
				} 
				
				if(($tipoValorSnmp == 'STRING') && (!preg_match('/"/i',$valorSnmp)) ) {
					$valorSnmp = '"'.$valorSnmp.'"';
				}
				
				$filtroObligatorio = false;
				if(($tipoValorSnmp == 'STRING') && ($getParam->type_item == 'float') ) {
					$this->logs->warning("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string $valorSnmp", NULL,'logs_poller');
					$filtroObligatorio = true;	
				}
				
				$this->logs->debug("$poler_log_text Buscando filtro especial", NULL,'logs_poller');
				
				$filterStatus = $this->filter($getParam->description,$getParam->dns,$valorSnmp,$poler_log_text);
				
				if($filterStatus) {
					$valorSnmp = $this->snmpFilterValue;
				}
								
				if(($filterStatus == false) && ($filtroObligatorio)) {
					$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string y no tiene filtro", NULL,'logs_poller');
					exit;
				}
				
				$query="/* BSW - Poller OK */ INSERT INTO `$tablehistory` (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->id_item,$getParam->id_host,$getParam->uptime, $valorSnmp ,1)";
				$this->conexion->query($query);
				
				if(empty($getParam->lastvalue)) {
					$getParam->lastvalue = "0";
				}
				
				if(is_numeric($valorSnmp)) {
					$valorSnmp = 'abs('.$valorSnmp.')';
				}
			
				$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  `status`='ok' ,`nextcheck` = '$getParam->nextcheck' , `lastclock` =" . $getParam->uptime  . ",prevvalue=lastvalue,lastvalue=" . $valorSnmp . " where id_item=". $getParam->id_item." AND `id_host`=$getParam->id_host";
				$result = $this->conexion->query($update_query);
				
				//Status Host 
				if((int)$getParam->id_item === 1) {
					$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '1' WHERE `id_host` = '$getParam->id_host';";
					$this->conexion->query($set_status_host_sql);
				}
				
			} else {
				$this->logs->error("$poler_log_text Timeout al consultar via snmp al host: $getParam->host ",NULL,'logs_poller');
				//Notificando error
				if($getParam->type_poller == 'index') {
					$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='retry' ,`nextcheck` = NOW() ,lastvalue=0 where id_item=".$getParam->id_item." AND `id_host`=$getParam->id_host";
				} else {
					$insert_query="/* BSW - Poller NOK */  INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->id_item,$getParam->id_host,$getParam->uptime, 0 ,0)";
					$this->conexion->query($insert_query);
					
					$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='error' ,`nextcheck` = '$getParam->nextcheck',lastvalue=0 where id_item=". $getParam->id_item ." AND `id_host`=$getParam->id_host";

					//Status Host 
					if((int)$getParam->id_item === 1) {
						$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '0' WHERE `id_host` = '$getParam->id_host';";
						$this->conexion->query($set_status_host_sql);
					}
				
				}
				
				$this->conexion->query($update_query);	
							
			}
			//Via modulo Fin
		} else {
			//Via comando Inicio
			$this->logs->error("El modulo snmp no existe por lo cual se intenta via comando",NULL,'logs_poller');
			$retval = shell_exec("whereis snmpget | wc -w");
			if($retval == 1) {
				$this->logs->error("Error el comando snmpget no existe",NULL,'logs_poller');
				exit;
			}
			$snmpget_cmd="snmpget -t10 -Oqv -v$version -c$getParam->snmp_community $ip $getParam->snmp_oid";
			$snmpget=shell_exec($snmpget_cmd);
			$this->logs->info("El comando snmp: ",$snmpget_cmd ." Respondio: ". $snmpget,'logs_poller');
			//Via comando Fin
		}
	}
		
	private function filter($description, $dns, $valor,$poler_log_text)
	{
		switch ($description) {
			case 'Availability.bsw':
				if(preg_match("/$dns/i",$valor)){
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
			break;

			case 'Availability-ForTrap.bsw':
				if(preg_match("/$dns/i",$valor)){
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
			break;
				
			default:
				return false;
			break;
		}
	}
}
?>