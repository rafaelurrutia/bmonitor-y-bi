<?php
ini_set('max_execution_time', 600);
class pollerv2 extends Control
{
    private $timeLimit = 124;

    private $memcache_host = 'localhost';
    private $memcache_port = 11211;

    public function run()
    {
        $this->parametro->set('LOGS_FILE', 'logs_poller8');

        $this->logs->info("[POLLER] ===============================================");
        $this->logs->info("[POLLER] Starting poller Server");
        $this->logs->info("[POLLER] Copyright Baking Software 2013");
        $this->logs->info("[POLLER] Version 2");
        $this->logs->info("[POLLER] ===============================================");

        $active = $this->_crontab("pollerv2_run", "start");
        //$active = true;

        if ($active) {
            $this->logs->info("[POLLER] Starting OK");
        } else {
            $this->logs->info("[POLLER] Starting NOK");
            exit ;
        }

        $LIMIT = $this->parametro->get('GET_MAX_POLLER', 10);
        $fecha = date('Y-m-d H:i:00');

        $post = array(
            'LIMIT_THREAD' => $LIMIT / 2,
            'DATE' => "$fecha"
        );

        //Fix lock item
        $clean_poller_sql = "DELETE FROM  `bm_poller_active` WHERE `fechahora` <  DATE_SUB(NOW(),INTERVAL `delay`*2 SECOND)";
        $this->conexion->query($clean_poller_sql);

        /*
        $this->curl->cargar(URL_BASE_FULL . 'pollerv2/start', 1, $post);
        $this->curl->cargar(URL_BASE_FULL . 'pollerv2/retry', 2, $post);

        $this->curl->ejecutar();*/

        $this->curl->multipleThreadsRequest(array(
            URL_BASE_FULL . 'pollerv2/start',
            URL_BASE_FULL . 'pollerv2/retry',
        ),$post);

        $this->_crontab("pollerv2_run", "finish");
    }

    public function start()
    {
        ini_set('max_execution_time', 300);

        $this->parametro->set('LOGS_FILE', 'logs_poller8');

        $active = $this->_crontab("pollerv2_start", "start");

        if ($active) {
            $this->logs->info("[POLLER][START] Starting OK");
        } else {
            $this->logs->info("[POLLER][START] Starting NOK");
            exit ;
        }

        $this->logs->info("[POLLER][START] ===============================================");
        $this->logs->info("[POLLER][START] Starting poller start Server");
        $this->logs->info("[POLLER][START] ===============================================");

        $memcache = new Memcached('poller_memcached');

        $ss = $memcache->getServerList();
        if (empty($ss)) {

            try {
                $memcache->addServer($this->memcache_host, $this->memcache_port);
            } catch (Exception $e) {
                $this->logs->error("[POLLER][START] Server Error: memcached down", $e);
                exit ;
            }

            $this->logs->info("[POLLER][START] Starting Memcache");
        } else {
            $this->logs->info("[POLLER][START] Server list: ", $ss);
        }

        $serial = round(microtime(true) . mt_rand(0, time()) . mt_rand(0, time()), 0);

        if (isset($_POST['DATE'])) {
            $DATE = $_POST['DATE'];
        } else {
            $DATE = date('Y-m-d H:i:s');
        }

        $get_item_sql = "SELECT 
                            I.`id_item` As idItem , 
                            H.`id_host` AS idHost,  
                            H.`dns`, 
                            H.`host`,
                            H.`ip_wan` As ipWan,
                            I.`id_item` As idItem, 
                            I.`description` ,
                            I.`type_item` As typeItem,
                            G.`feriados`,
                            G.`horario`,
                            G.`snmp_monitor`,
                            I.`snmp_community` As snmpCommunity,
                            I.`snmp_port` As snmpPort,
                            I.`snmp_oid` As snmpOid, 
                            unix_timestamp(now()) as `utime`,
                            IF(G.`delay_bsw` IS NULL or G.`delay_bsw` = 0,I.`delay`,G.`delay_bsw`) AS delay,
                            DATE_ADD(NOW(),INTERVAL  IF(G.`delay_bsw` IS NULL or G.`delay_bsw` = 0,I.`delay`,G.`delay_bsw`) SECOND) as 'nextcheck'
                        FROM `bm_host` H
                        LEFT JOIN `bm_host_groups` G ON G.`groupid`=H.`groupid`
                        LEFT JOIN `bm_items_groups` IG ON IG.`groupid`=H.`groupid`
                        LEFT JOIN `bm_items` I ON I.`id_item`=IG.`id_item`
                        LEFT OUTER JOIN `bm_poller_active` PA ON (PA.`id_host`=H.`id_host` AND  PA.`id_item`=I.`id_item`)
                        LEFT OUTER JOIN `bm_item_profile` IP ON (IP.`id_host`=H.`id_host` AND IP.`id_item`=IG.`id_item`) 
                        WHERE 
                            H.`borrado`=0 AND 
                            I.`type_poller` = 'snmp' AND H.`status`=1 AND
                            (IP.`nextcheck`  < NOW() OR IP.`nextcheck` IS NULL) AND
                            (IP.`status` = 'ok' OR IP.`status` IS NULL) AND
                            PA.`id_poller` IS NULL";

        $get_item_result = $this->conexion->queryFetch($get_item_sql);

        if ($get_item_result === false) {
            $this->logs->error("[POLLER][START] Error al obtener items");
            $this->_crontab("pollerv2_start", "finish");
            exit ;
        } elseif (count($get_item_result) === 0) {
            $this->logs->info("[POLLER][START] 0 items");
            $this->_crontab("pollerv2_start", "finish");
            exit ;
        }

        $time =  strtotime($DATE);

        foreach ($get_item_result as $key => $value) {

            $nextcheck = strtotime("+" . $value['delay'] . " second", strtotime($DATE));
            $nextcheck = date('Y-m-d H:i:00', $nextcheck);

            $value['nextcheck'] = $nextcheck;

            $threads_all[$value['idItem'] . "_" . $value['idHost']] = array(
                'command' => SITE_PATH . 'server/poller.php',
                'result' => 'none',
                'finished' => false,
                'detail' => $value + array('type' => 'start')
            );

            $lock_item_value[] = '(' . $value['idHost'] . ',' . $value['idItem'] . ',' . $value['delay'] . ',' . $time . ',"' . $DATE . '","active")';
        }

        //Lock item

        $lock_item_sql = "INSERT INTO `bm_poller_active` (`id_host`, `id_item`, `delay`, `uptime`, `fechahora`, `status`) VALUES ";

        $lock_item_sql = $lock_item_sql . join(',', $lock_item_value);

        $lock_item_result = $this->conexion->query($lock_item_sql);

        if ($get_item_result === false) {
            $this->logs->error("[POLLER][START] error lock items");
            $this->_crontab("pollerv2_start", "finish");
            exit ;
        }

        if (isset($_POST['LIMIT_THREAD']) && is_numeric($_POST['LIMIT_THREAD'])) {
            $LIMIT_THREAD = $_POST['LIMIT_THREAD'];
        } else {
            $LIMIT_THREAD = 10;
        }

        $ARRAY_THREAD = array_chunk($threads_all, $LIMIT_THREAD, true);

        foreach ($ARRAY_THREAD as $key => $threads) {

            foreach ($threads as $id => $thread) {
                $threads[$id]['started'] = time();
                $memcache->set("poller_" . $id, $thread['detail'], 10);
                if ($memcache->getResultCode() != Memcached::RES_SUCCESS) {
                    $this->logs->error("Asignacion de memoria fallida: ", $memcache->getResultCode());
                }
                $unix = $thread['detail'];
                exec("/usr/bin/php -f {$thread['command']} {$serial} {$id} start 2> /dev/null > /dev/null &");
            }

            $finished = 0;

            while ($finished <= count($threads) - 1) {
                foreach ($threads as $id => $thread) {
                    if ($thread['finished']) {
                        continue;
                    }
                    $result = $memcache->get("{$serial}_{$id}");
                    if (($memcache->getResultCode() == Memcached::RES_SUCCESS)) {
                        $threads[$id]['result'] = $result;
                        $threads[$id]['finished'] = true;
                        $finished++;
                    } elseif ($thread['started'] < time() - $this->timeLimit) {
                        $thread['finished'] = true;
                        $finished++;
                    }
                }
            }
        }

        $post['LIMIT_THREAD'] = $LIMIT_THREAD;
        $post['DATE'] = $DATE;

        $this->curl->cargar(URL_BASE_FULL . 'pollerv2/retry', 2, $post);
        $this->curl->ejecutar();
        $this->_crontab("pollerv2_start", "finish");
    }

    public function retry()
    {
        ini_set('max_execution_time', 300);

        $this->parametro->set('LOGS_FILE', 'logs_poller8');

        $this->logs->info("[POLLER][RETRY] ===============================================");
        $this->logs->info("[POLLER][RETRY] Starting poller retry Server");
        $this->logs->info("[POLLER][RETRY] ===============================================");

        $memcache = new Memcached('poller_memcached');

        $ss = $memcache->getServerList();
        if (empty($ss)) {
            $memcache->addServer($this->memcache_host, $this->memcache_port) or die('Server Error: memcached down.');
        }

        $serial = round(microtime(true) . mt_rand(0, time()) . mt_rand(0, time()), 0);

        if (isset($_POST['DATE'])) {
            $DATE = $_POST['DATE'];
        } else {
            $DATE = date('Y-m-d H:i:s');
        }

        $get_item_sql = "SELECT 
                            I.`id_item` As idItem , 
                            H.`id_host` AS idHost,  
                            H.`dns`, 
                            H.`host`,
                            H.`ip_wan` As ipWan,
                            I.`id_item` As idItem, 
                            I.`description` ,
                            I.`type_item` As typeItem,
                            G.`feriados`,
                            G.`horario`,
                            G.`snmp_monitor`,
                            I.`snmp_community` As snmpCommunity,
                            I.`snmp_port` As snmpPort,
                            I.`snmp_oid` As snmpOid, 
                            unix_timestamp(now()) as `utime`,
                            IF(G.`delay_bsw` IS NULL or G.`delay_bsw` = 0,I.`delay`,G.`delay_bsw`) AS delay,
                            DATE_ADD(NOW(),INTERVAL  IF(G.`delay_bsw` IS NULL or G.`delay_bsw` = 0,I.`delay`,G.`delay_bsw`) SECOND) as 'nextcheck'
                        FROM `bm_host` H
                        LEFT JOIN `bm_host_groups` G ON G.`groupid`=H.`groupid`
                        LEFT JOIN `bm_items_groups` IG ON IG.`groupid`=H.`groupid`
                        LEFT JOIN `bm_items` I ON I.`id_item`=IG.`id_item`
                        LEFT OUTER JOIN `bm_poller_active` PA ON (PA.`id_host`=H.`id_host` AND  PA.`id_item`=I.`id_item`)
                        LEFT OUTER JOIN `bm_item_profile` IP ON (IP.`id_host`=H.`id_host` AND IP.`id_item`=IG.`id_item`) 
                        WHERE 
                            H.`borrado`=0 AND 
                            I.`type_poller` = 'snmp' AND H.`status`=1 AND
                            (IP.`nextcheck`  < NOW() OR IP.`nextcheck` IS NULL) AND
                            ( IP.`status` = 'error' OR  IP.`status` = 'retry') AND
                            PA.`id_poller` IS NULL";

        $get_item_result = $this->conexion->queryFetch($get_item_sql);

        if ($get_item_result === false) {
            $this->logs->error("[POLLER][RETRY] Error al obtener items");
            exit ;
        } elseif (count($get_item_result) === 0) {
            $this->logs->info("[POLLER][RETRY] 0 items");
            exit ;
        }

        $time = strtotime($DATE);

        foreach ($get_item_result as $key => $value) {

            $nextcheck = strtotime("+" . $value['delay'] . " second", strtotime($DATE));
            $nextcheck = date('Y-m-d H:i:00', $nextcheck);

            $value['nextcheck'] = $nextcheck;

            $threads_all[$value['idItem'] . "_" . $value['idHost']] = array(
                'command' => SITE_PATH . 'server/poller.php',
                'result' => 'none',
                'finished' => false,
                'detail' => $value + array('type' => 'retry')
            );

            $lock_item_value[] = '(' . $value['idHost'] . ',' . $value['idItem'] . ',' . $value['delay'] . ',' . $time . ',NOW(),"active")';
        }

        //Lock item

        $lock_item_sql = "INSERT INTO `bm_poller_active` (`id_host`, `id_item`, `delay`, `uptime`, `fechahora`, `status`) VALUES ";

        $lock_item_sql = $lock_item_sql . join(',', $lock_item_value);

        $lock_item_result = $this->conexion->query($lock_item_sql);

        if ($get_item_result === false) {
            $this->logs->error("[POLLER][RETRY] error lock items");
            exit ;
        }

        if (isset($_POST['LIMIT_THREAD']) && is_numeric($_POST['LIMIT_THREAD'])) {
            $LIMIT_THREAD = $_POST['LIMIT_THREAD'];
        } else {
            $LIMIT_THREAD = 10;
        }

        $ARRAY_THREAD = array_chunk($threads_all, $LIMIT_THREAD, true);

        foreach ($ARRAY_THREAD as $key => $threads) {

            foreach ($threads as $id => $thread) {
                $threads[$id]['started'] = time();
                $result = $memcache->set($id, $thread['detail'], 10);
                if (($memcache->getResultCode() !== Memcached::RES_SUCCESS)) {
                    $this->logs->error("[POLLER][RETRY] Failed to store data in memory", $memcache->getResultCode());
                }
                exec("/usr/bin/php -f {$thread['command']} {$serial} {$id} retry 2> /dev/null > /dev/null &");
            }

            $finished = 0;

            while ($finished <= count($threads) - 1) {
                foreach ($threads as $id => $thread) {
                    if ($thread['finished']) {
                        continue;
                    }
                    $result = $memcache->get("{$serial}_{$id}");
                    if (($memcache->getResultCode() == Memcached::RES_SUCCESS)) {
                        $threads[$id]['result'] = $result;
                        $threads[$id]['finished'] = true;
                        $finished++;
                    } elseif ($thread['started'] < time() - $this->timeLimit) {
                        $thread['finished'] = true;
                        $finished++;
                    }
                }
            }
        }
    }

}
?>