<?php
class configsec extends Control {
	
	public function cfgSonda()
	{		
		$valida = $this->protect->access_page('CONFIG_WIZARD_FORM');
		$this->plantilla->load_sec("configuracion/config_configsec", "denegado",$valida);
		echo $this->plantilla->get();
	}

	private function validMAC($mac,$type_logs='logs_config')
	{
		if(isset($mac)) {
				
			$dnsReportado="BSW" . str_replace(":","",$mac);
			
			if (strpos($mac, ":") == false) {
				$mac = rtrim(chunk_split($mac,2,":"),":");
			}
			if (preg_match("/^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/i",$mac,$result))
    		{
    			$query_validaHost="SELECT `id_host`,`dns`,`host`,`status` FROM `bm_host` WHERE `borrado`=0 AND (`bm_host`.`mac`='$mac'  OR `bm_host`.`mac_lan`='$mac')";

				$validaHost = $this->conexion->queryFetch($query_validaHost);

				if($validaHost){
					$validaHost = (object)$validaHost[0];
					$validaHost->mac = $mac;
					$validaHost->active = true;
					$validaHost->dns = $dnsReportado;
					
					return $validaHost;
				} else {
					$this->logs->warning("Mac no registrada: ",$mac,$type_logs);
					$validaHost = array();
					$validaHost['active'] = false;
					$validaHost['mac'] = $mac;
					$validaHost['dns'] = $dnsReportado;
					return (object)$validaHost;
				}
			} else {
				$this->logs->error("Mac no valida: ",$mac,$type_logs);
				return false;
			}
		} else {
			return false;
		}			
	}

	public function manualValidMAC($mac)
	{
		$mac = $this->validMAC($mac);
		
		if($mac) {
			
			if($mac->active) {
				$result['msg'] = "Mac se encuentra actualmente asignada<br/> MAC: $mac->mac";
				$result['status'] = false;
			} else {
				$result['msg'] = "Mac aceptada<br/> MAC: ".$mac->mac;
				$result['status'] = true;
				$_SESSION['config_mac'] = $mac->mac;
				$_SESSION['config_detail'] = $mac;	
			}
			
		} else {
			$result['msg'] = "Mac invalida<br/> MAC: ".$mac;
			$result['status'] = false;
		}

		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}
			
	public function getMac()
	{
		$ip = getenv('REMOTE_ADDR');	
		$comm='public';
		//$ip="200.30.223.81";		
		$cmd = @snmp2_get( $ip . ":1161", "public", "1.3.6.1.4.1.2021.3.101.101.1", 1000000 , 5);
		
		if(!$cmd) {
			$result['status'] = false;
			$result['msg'] = '<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<span id="msg"> IP: '.$ip.' <br/> '.$this->language->CONFIG_SEC_ERROR_SEARCH_MAC.' </span><button type="button" id="refresh">Reintenar</button></p>';
			
			$WIZARD_MANUAL_ASIGG_MAC = $this->parametro->get('WIZARD_MANUAL_ASIGG_MAC',true);
			
			if($WIZARD_MANUAL_ASIGG_MAC) {
				$result['msg2'] = '';
				$_SESSION['config_ip'] = '0.0.0.0';
				$result['manual'] = true;
			} else {
				$result['msg2'] = '';
				$result['manual'] = false;
			}
			
		} else {
			$mac = str_replace(array("STRING: ", "\""),"",$cmd);

			//Validando MAC 
		
			$result_mac = $this->validMAC($mac);
			
			if($result_mac) {
				$_SESSION['config_mac'] = $mac;
				$_SESSION['config_ip'] = $ip;
				$_SESSION['config_detail'] = $result_mac;
				
				if($result_mac->active) {
					$result['msg'] = $this->language->CONFIG_SEC_OK_SEARCH_MAC."<br/> HOST: $result_mac->host <br/> IP: $ip <br/> MAC: ".$mac;
				} else {
					$result['msg'] = $this->language->CONFIG_SEC_OK_SEARCH_MAC."<br/> IP: $ip <br/> MAC: ".$mac;
				}
			} else {
				unset($_SESSION['config_mac']);
				unset($_SESSION['config_detail']);
			}
			
			$result['status'] = true;
			
		}
		header("Content-type: application/json");
		echo json_encode($result);
		exit;
	}

	public function getFormConfig()
	{		
		$valida = $this->protect->access_page('CONFIG_WIZARD_FORM');
		
		$this->plantilla->load_sec("configuracion/config_configsec_form", "denegado",$valida);
		
		$vars['config_groups'] = $this->bmonitor->getAllGroupsHost('ENLACES','option');
		
		$vars['config_groups'] = $this->bmonitor->getAllGroupsHost('ENLACES','option');
		
		$vars['config_region'] = $this->basic->getLocalidadOption('region');
		
		//$vars['config_school'] = $this->bmonitor->getSchool('option');
		
		$FORCE_INCACTIVE_CP = $this->parametro->get('FORCE_INCACTIVE_CP',false);
	
		if($FORCE_INCACTIVE_CP) {
			$vars['config_cparental'] = "<option value='0'>Desactivado</option>";
		} else {
			$vars['config_cparental'] = "<option value='1'>Habilitado</option>";
		}

		$vars['config_ip'] = $_SESSION['config_ip'];
		$vars['config_mac'] = $_SESSION['config_mac'];
		
		$vars['config_ssid'] = $this->basic->getOptionValue('ssid','Enlaces');
		
		$vars['config_wifi_key'] = $this->basic->getOptionValue('wifi_key','enlaces1');
		$vars['config_lan_ip'] = $this->basic->getOptionValue('lan_ip','192.168.40.1');
		$vars['config_mac_lan'] = $this->basic->getOptionValue('lan_mac','255.255.255.0');
		$this->plantilla->set($vars);
		echo $this->plantilla->get();
	}
	
	public function getSchool() {
		echo json_encode($this->bmonitor->getSchool('option',$_POST['region']));
	}

	public function getPlanes() {
		
		$getParam = (object)$_POST;
		
		if((int)$getParam->groupid == 0) {
			$result['status'] = true;
			$result['select'] = '<option selected="" value="0">'.$this->language->SELECT_A_GROUP.'</option>';
		}
		elseif(isset($getParam->idschool)) {
				
				$select_sql = 'SELECT P.`id_plan` FROM `bm_colegios` C LEFT JOIN `bm_plan` P ON  C.`planfdt`=P.`plan` WHERE C.`id_colegio`='.$getParam->idschool;
				
				$select_result = $this->conexion->queryFetch($select_sql);
				$msg = false;
				if($select_result) {
					$idPlan = $select_result[0]['id_plan'];
					if(!is_numeric($idPlan)) {
						$idPlan = false;
					} else {
						$msg = ' (Informado)';
					}
				} else {
					$idPlan = false;
				}
				
				$result['status'] = true;
				$result['select'] = $this->bmonitor->getPlanes($getParam->groupid,true,false,$idPlan,$msg);
			
		} elseif (isset($getParam->groupid)) {
			$result['status'] = true;
			$result['select'] = $this->bmonitor->getPlanes(false,true,false,$idPlan,$msg);
		} else {
			$result['select'] = '<option selected="" value="0">Error interno</option>';
		}
		header('Content-Type: application/json');
		echo json_encode($result);
		exit;
	}

	public function createSonda()
	{
		$getParam = (object)$_POST;
		
		$select_sql = "SELECT CO.*, CONCAT(UCASE(C.`COMUNA_NOMBRE`) , ' - ', `establecimiento` , ' [' ,  `direccion` , ']') As establecimiento FROM `bm_colegios` CO LEFT JOIN `bm_comuna` C ON C.`COMUNA_ID`=CO.`COMUNA_ID` WHERE `id_colegio`=".$getParam->idschool;
		
		$select_result = $this->conexion->queryFetch($select_sql);
		
		if($select_result) {
			
			$school = (object)$select_result[0];
			$result_mac = $this->validMAC($getParam->mac);

			if($result_mac && ($result_mac->active == false)) {
				
				$this->conexion->InicioTransaccion();
				
				$insert_host = sprintf("INSERT INTO `bm_host` (`groupid`, `host`, `dns`, `mac`, `ip_wan`, `ip_lan`, `netmask_lan`, `id_plan`, `status`, `borrado`)
											VALUE (%s, '%s', '%s', '%s', '%s', '%s','%s', %s , 1, 0)",
											$getParam->groupid,
											utf8_encode($school->establecimiento),
											$result_mac->dns,
											$result_mac->mac,
											$getParam->ip,
											$getParam->lanip,
											$getParam->mascara,
											$getParam->planid);
											
				$insert_host = $this->conexion->query($insert_host);
				
				$id_host = $this->conexion->lastInsertId();
				
				$insert_feture = "INSERT INTO `bm_host_detalle` (`id_host`, `id_feature`, `value`) VALUES ";
				
				$insert_value[] = "($id_host,1,$school->rdb)"; //RDB
				$insert_value[] = "($id_host,2,$school->viviendaid)"; //ID Vivienda
				$insert_value[] = "($id_host,3,$getParam->wifi)";
				$insert_value[] = "($id_host,4,'$getParam->ssid')";
				
				$FORCE_INCACTIVE_CP = $this->parametro->get('FORCE_INCACTIVE_CP',false);
				
				if(!$FORCE_INCACTIVE_CP){
					$insert_value[] = "($id_host,5,$getParam->cparental)";
				} else {
					$insert_value[] = "($id_host,5,0)";
				}
				
				$insert_value[] = "($id_host,8,'ap')";
				$insert_value[] = "($id_host,9,6)";
				$insert_value[] = "($id_host,11,'$getParam->wifikey')";
				$insert_value[] = "($id_host,12,'".$this->parametro->get('TIMEZONE')."')";
				$insert_value[] = "($id_host,13,'mixed-psk+tkip+aes')";
				$insert_value[] = "($id_host,21,'$school->rut')";
				$insert_value[] = "($id_host,22,'$school->direccion')";
				$insert_value[] = "($id_host,43,$school->PROVINCIA_ID)";
				$insert_value[] = "($id_host,24,$school->REGION_ID)";
				$insert_value[] = "($id_host,23,$school->COMUNA_ID)";
				
				$insert_feature_config = $insert_feture.join(',',$insert_value);
				
				$feature_insert_config = $this->conexion->query($insert_feature_config);
				
				if($feature_insert_config && $insert_host) {
					$this->conexion->commit();
					$result['status'] = true;
					$result['msg'] = 'Sonda creada exitosamente';
				} else {
					$this->conexion->rollBack();
					$result['msg'] = 'Error al generar la sonda';
					$result['status'] = true;
				}
			} else {
				$result['status'] = false;
				$result['msg'] = "Error ,  mac se encuentra asociada a una sonda existente";
			}
		} else {
			$result['status'] = false;
			$result['msg'] = "Error al consultar por el colegio seleccionado";
		}
		
		echo json_encode($result);
		exit;
	}
}
?>