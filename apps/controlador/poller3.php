<?php 

class poller3 extends Control {

	private function pollerActivo($class,$type = 'hijo') {
		$select_poller = "SELECT count(*) As Active FROM `bsw_poller` WHERE `tipo`='$type' AND `fecha_fin` IS NULL AND `clase`='$class';";
		$result = $this->conexion->queryFetch($select_poller);
		if($result) {
			return $result[0]['Active'];
		} else {
			return false;
		}
	}
		
	private function validaPoller($class,$tipo = 'padre',$pidAnterior = null){
			
		$this->pid=getmypid();
		
		$this->tipo=$tipo;
		
		//$limpiaPoller = 'DELETE FROM `bsw_poller` WHERE `fecha_fin` IS NOT NULL AND ( ( tipo="hijo" AND `fecha_fin` IS NOT NULL)  OR  (tipo="padre" AND `fecha_fin` <   DATE_ADD(NOW(), INTERVAL  - 1 MINUTE)));';

		$result = $this->pollerActivo($class,$tipo);

		if($result == false) {
			
			$sql_inicial = 'INSERT INTO `bsw_poller` (`clase`, `pid_padre`, `pid_hijo`, `tipo`, `fecha_inicio`) VALUES';
			
			if($tipo == 'padre') {
				if($result == 0) {
					$insert = $sql_inicial." ('$class', $this->pid, 0, 'padre', NOW());";
					$this->conexion->query($insert);
				} else {
					print "Poller activo";
					exit;
				}
				
			} elseif ($tipo == 'hijo') {
				$insert = $sql_inicial." ('$class', $pidAnterior, $this->pid, 'hijo', NOW());";
				$this->conexion->query($insert);
			} else {
				$this->logs->error("Tipo de poller invalido: ",$tipo,'logs_poller');
			}	
		} else {
			$this->logs->debug("Error al consultar  poller activo",NULL,'logs_poller');
			exit;
		}
	}

	public function index()
	{
		$this->validaPoller("index");
		
		$LIMIT = $this->parametro->get('GET_MAX_POLLER',10);
		
		$activos = $this->pollerActivo("indexPoller");
		
		if($activos) {
			$LIMIT = $LIMIT - $activos;
		}
		
		if(($LIMIT == 0) || ( $LIMIT < 0)) {
			$this->logs->error("[POLLER_INDEX] Poller inicial supero el maximo permitido",NULL,'logs_poller');
			exit;
		} else {
			$this->logs->debug("[POLLER_INDEX] Iniciando poller con un limite de [$activos/$LIMIT] hilos",NULL,'logs_poller');
		}
		
		$sql_host = "SELECT DISTINCT profile.`id_item_profile`
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			LEFT JOIN  `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND host.`groupid`=IG.`groupid` )
			WHERE host.`borrado`=0 AND IG.`status`= 1 AND host.`status` = 1 AND profile.`nextcheck`  < NOW()  AND item.`type_poller` = 'snmp' AND  ( profile.`status`='pendiente' OR profile.`status`='ok' ) LIMIT ".$LIMIT;
					
		$result_host = $this->conexion->queryFetch($sql_host);

		$totalHijos = 	count($result_host);
		
		if($totalHijos >= 1){
			$this->logs->debug("[POLLER_INDEX] Cargando un total de $totalHijos hilos",NULL,'logs_poller');
			foreach ($result_host as $key => $host) {
				$urlInicial = URL_BASE_FULL.'poller3/process/'.$host['id_item_profile'];
				$post['type_poller'] = 'index';
				$post['pipe'] = $key;
				$post['pidPadre'] = $this->pid;
				$post['pidType'] = 'indexPoller';
				$this->curl->cargar($urlInicial, $host['id_item_profile'],$post);	
				$this->logs->debug("[POLLER_INDEX] Hilo numero $key , cargado",NULL,'logs_poller');
			}
			$this->curl->ejecutar();
		} else {
			$this->logs->debug("[POLLER_INDEX] No hay host disponibles",NULL,'logs_poller');
		}
	}
	
	public function retry()
	{
		$this->validaPoller("retry");
		
		$LIMIT = $this->parametro->get('GET_MAX_POLLER',20);
		
		$activos = $this->pollerActivo("retry");
		
		if($activos) {
			$LIMIT = $LIMIT - $activos;
		}
		
		if(($LIMIT == 0) || ( $LIMIT < 0)) {
			$this->logs->error("[POLLER_RETRY] Poller inicial supero el maximo permitido",NULL,'logs_poller');
			exit;
		} else {
			$this->logs->debug("[POLLER_RETRY] Iniciando poller con un limite de [$activos/$LIMIT] hilos",NULL,'logs_poller');
		}
		
		$sql_host = "SELECT DISTINCT profile.`id_item_profile`
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			LEFT JOIN  `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND host.`groupid`=IG.`groupid` )
			WHERE host.`borrado`=0 AND host.`status` = 1 AND  IG.`status`= 1 AND profile.`nextcheck`  < NOW()  AND item.`type_poller` = 'snmp' AND  ( profile.`status`='retry' OR profile.`status`='error' ) LIMIT ".$LIMIT;
					
		$result_host = $this->conexion->queryFetch($sql_host);

		$totalHijos = 	count($result_host);
		
		if($totalHijos >= 1){
			$this->logs->debug("[POLLER_RETRY] Cargando un total de $totalHijos hilos",NULL,'logs_poller');
			foreach ($result_host as $key => $host) {
				$urlInicial = URL_BASE_FULL.'poller3/process/'.$host['id_item_profile'];
				$post['type_poller'] = 'retry';
				$post['pipe'] = $key;
				$post['pidPadre'] = $this->pid;
				$this->curl->cargar($urlInicial, $host['id_item_profile'],$post);	
				$this->logs->debug("[POLLER_RETRY] Hilo numero $key , cargado",NULL,'logs_poller');
			}
			$this->curl->ejecutar();
		} else {
			$this->logs->debug("[POLLER_RETRY] No hay host disponibles",NULL,'logs_poller');
		}
	}
	
	public function process($id_item){
				
		$getParam = (object)$_POST;
		
		if($getParam->type_poller == 'index') {
			$poler_log_text = '[POLLER_INDEX]';
			$status = 'process';
			$timeout = $this->parametro->get("SNMP_TIMEOUT_POLLER",1000000);
			$retry = $this->parametro->get("SNMP_RETRY_POLLER",1);
			$version = $this->parametro->get("SNMP_VERSION_POLLER",'2c');
		} else {
			$status = 'processr';
			$poler_log_text = '[POLLER_RETRY]';
			$timeout = $this->parametro->get("SNMP_TIMEOUT_RETRY",2000000);
			$retry = $this->parametro->get("SNMP_RETRY_RETRY",4);
			$version = $this->parametro->get("SNMP_VERSION_RETRY",'2c');
		}

		$this->logs->debug("$poler_log_text Hilo numero $getParam->pipe , activo",NULL,'logs_poller');
		
		$this->validaPoller($getParam->type_poller,'hijo',$getParam->pidPadre);
		
		if(!is_numeric($id_item)) {
			return false;
		}
		
		$utime = time();
	
		$get_date_host_sql = "SELECT *
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			WHERE profile.`id_item_profile` = $id_item;";
			
		$itemParam = $this->conexion->queryFetch($get_date_host_sql);
	
		if(!$itemParam) {
			$this->logs->error("Error al obtener datos del item: ".$id_item);
			return false;
		} else {
			$itemParam = (object)$itemParam[0];
		}
		
		$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  `status`='$status' ,`nextcheck` = NOW() , `lastclock` =" . $utime . ",prevvalue=lastvalue,lastvalue=0 where id_item=". $itemParam->id_item." AND `id_host`=$itemParam->id_host";
		$result = $this->conexion->query($update_query);
		
				
		$ip = $itemParam->ip_wan.":".$itemParam->snmp_port;

		switch ($itemParam->type_item) {
			case 'float':
				$tablehistory="bm_history";
			break;
			
			case 'string':
				$tablehistory="bm_history_str";
			break;
			
			case 'text':
				$tablehistory="bm_history_text";
			break;
			
			case 'log':
				$tablehistory="bm_history_uint";
			break;
							
			default:
				$tablehistory="bm_history";
			break;
		}
							
		//Validamos si el modulo o paquete php_snmp se encuentra activo
		if (function_exists('snmp2_get')) {
			//Via modulo Inicio
			
			$result = @snmp2_get($ip,$itemParam->snmp_community,$itemParam->snmp_oid,$timeout,$retry);
			if($result){
				$this->logs->debug("$poler_log_text La consulta snmp -> host: [$itemParam->host] ip: [$ip] objetid: [$itemParam->snmp_oid]  , respondio: ",$result,'logs_poller');
				list($tipoValorSnmp, $valorSnmp) = explode(": ", $result);
				
				//validaciones 
				
				if(($tipoValorSnmp == 'STRING') && (!preg_match('/"/i',$valorSnmp)) ) {
					$valorSnmp = '"'.$valorSnmp.'"';
				}
				
				$filtroObligatorio = false;
				if(($tipoValorSnmp == 'STRING') && ($itemParam->type_item == 'float') ) {
					$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string $valorSnmp", NULL,'logs_poller');
					$filtroObligatorio = true;	
				}
				
				$this->logs->debug("$poler_log_text Buscando filtro especial", NULL,'logs_poller');
				
				$filterStatus = $this->filter($itemParam->description,$itemParam->dns,$valorSnmp,$poler_log_text);
				
				if($filterStatus) {
					$valorSnmp = $this->snmpFilterValue;
				}
								
				if(($filterStatus == false) && ($filtroObligatorio)) {
					$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string y no tiene filtro", NULL,'logs_poller');
					exit;
				}
				
				$query="/* BSW - Poller OK */ INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $itemParam->id_item,$itemParam->id_host,$utime, $valorSnmp ,1)";
				$this->conexion->query($query);
				
				if(empty($getParam->lastvalue)) {
					$getParam->lastvalue = "0";
				}
				
				if(is_numeric($valorSnmp)) {
					$valorSnmp = 'abs('.$valorSnmp.')';
				}
			
				$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  `status`='ok' ,`nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND) , `lastclock` =" . $utime  . ",prevvalue=lastvalue,lastvalue=" . $valorSnmp . " where id_item=". $itemParam->id_item." AND `id_host`=$itemParam->id_host";
				$result = $this->conexion->query($update_query);
				
				//Status Host 
				if((int)$itemParam->id_item === 1) {
					$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '1' WHERE `id_host` = '$itemParam->id_host';";
					$this->conexion->query($set_status_host_sql);
				}
				
			} else {
				$this->logs->error("$poler_log_text Timeout al consultar via snmp al host: $itemParam->host ",NULL,'logs_poller');
				//Notificando error
				if($getParam->type_poller == 'index') {
					$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='retry' ,`nextcheck` = NOW() , `lastclock` =" . $utime  . ",prevvalue=lastvalue,lastvalue=0 where id_item=".$itemParam->id_item." AND `id_host`=$itemParam->id_host";
				} else {
					$update_query="UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error='Router no responde' ,  `status`='error' ,`nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND)  , `lastclock` =" . $utime  . ",prevvalue=lastvalue,lastvalue=0 where id_item=". $itemParam->id_item ." AND `id_host`=$itemParam->id_host";
					$insert_query="/* BSW - Poller NOK */  INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $itemParam->id_item,$itemParam->id_host,$utime, 0 ,0)";
					$this->conexion->query($insert_query);
					//Status Host 
					if((int)$itemParam->id_item === 1) {
						$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '0' WHERE `id_host` = '$itemParam->id_host';";
						$this->conexion->query($set_status_host_sql);
					}
				
				}
				
				$this->conexion->query($update_query);	
							
			}
			//Via modulo Fin
		} else {
			//Via comando Inicio
			$this->logs->error("El modulo snmp no existe por lo cual se intenta via comando",NULL,'logs_poller');
			$retval = shell_exec("whereis snmpget | wc -w");
			if($retval == 1) {
				$this->logs->error("Error el comando snmpget no existe",NULL,'logs_poller');
				exit;
			}
			$snmpget_cmd="snmpget -t10 -Oqv -v$version -c$getParam->snmp_community $ip $getParam->snmp_oid";
			$snmpget=shell_exec($snmpget_cmd);
			$this->logs->info("El comando snmp: ",$snmpget_cmd ." Respondio: ". $snmpget,'logs_poller');
			//Via comando Fin
		}
	}
		
	private function filter($description, $dns, $valor,$poler_log_text)
	{
		switch ($description) {
			case 'Availability.bsw':
				if(preg_match("/$dns/i",$valor)){
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
			break;

			case 'Availability-ForTrap.bsw':
				if(preg_match("/$dns/i",$valor)){
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor",NULL,'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
			break;
				
			default:
				return false;
			break;
		}
	}
		
	function __destruct() {
		if($this->tipo == 'padre'){
			$campo = 'pid_padre';
		} else {
			$campo = 'pid_hijo';
		}
		$update = " UPDATE `bsw_poller` SET `fecha_fin` = NOW() WHERE `$campo` = '$this->pid'";
		$result_update = $this->conexion->query($update);
		if(!$result_update) {
			$this->logs->error("Error al actualizar estado del poller con el pid",$this->pid,'logs_poller');
		}
		$limpiaPoller = 'DELETE FROM `bsw_poller` WHERE `fecha_fin` IS NOT NULL;';
		$this->conexion->query($limpiaPoller);
	}
}
?>