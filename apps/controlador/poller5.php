<?php 

class poller5 extends Control {
	
	public function poller() {
		
		//Delay por registar
		$get_sql = "SELECT DISTINCT HG.`delay`  FROM `bm_host_groups` HG LEFT OUTER JOIN `bm_poller`  P ON HG.`delay`=P.`delay` WHERE P.`id_poller` IS NULL AND HG.`delay` != 0";
		
		$get_result = $this->conexion->queryFetch($get_sql);
		
		if($get_result && (count($get_result) > 0)) {
			
			foreach ($get_result as $key => $value) {
				
				$insert_poller_sql = "INSERT INTO `bm_poller` (`uptime`, `delay`, `item_poller`, `fecha_update`, `startcheck`, `nextcheck`)
										VALUES ( UNIX_TIMESTAMP(NOW()) , ?, 0, NOW(), NOW(), NOW() + `delay`)";
										
				$this->conexion->queryFetch($insert_poller_sql,$value['delay']);
			}
		}
		
		//Validando ejecucion poller 
		
		$AVAILABILITY_GROUP = $this->parametro->get('AVAILABILITY_GROUP',true);
		
		if($AVAILABILITY_GROUP) {
			$FILTER = 'DELAY_GROUP';
		} else {
			$FILTER = 'DELAY_ITEM';
		}

		//Item pendientes:
		
		//$get_zero_poller_sql = "SELECT `delay` FROM `bm_poller` WHERE `nextcheck` <= NOW() AND `item_poller` = 0";
		$get_zero_poller_sql = "SELECT `delay` FROM `bm_poller` WHERE `nextcheck` <= NOW()";
		
		$get_zero_poller_result = $this->conexion->queryFetch($get_zero_poller_sql);
		
		if($get_zero_poller_result) {
			
			foreach ($get_zero_poller_result as $key => $value) {
				
				$get_item_pendientes_sql = "SELECT COUNT(DISTINCT H.`id_host`) AS TOTAL, item.`delay` AS DELAY_ITEM, HG.`delay` AS DELAY_GROUP
											FROM `bm_host` H
												LEFT OUTER JOIN `bm_item_profile` P USING(`id_host`)
												LEFT JOIN `bm_items` item ON P.`id_item`=item.`id_item`
												LEFT JOIN `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND H.`groupid`=IG.`groupid` )
												LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=H.`groupid`
												WHERE ( P.`id_item` = 1 OR P.`id_item` IS NULL ) AND H.`borrado`=0 AND IG.`status`= 1 AND P.`nextcheck`  < NOW() 
												GROUP BY HG.`delay` HAVING $FILTER =".$value['delay'];
				
				$get_item_pendientes_result = $this->conexion->queryFetch($get_item_pendientes_sql);
				
				if($get_item_pendientes_result) {
					$item_count = (int)$get_item_pendientes_result[0]['TOTAL'];
					$update_count_item_sql = "UPDATE `bm_poller` SET `item_poller` = $item_count, `duration_poller` = ( UNIX_TIMESTAMP(NOW()) - `uptime`), `uptime` = UNIX_TIMESTAMP(NOW())  ,`startcheck` = NOW(), `fecha_update` = NOW() , `nextcheck` = DATE_ADD(NOW(), INTERVAL `delay` SECOND)  WHERE `delay` = ".$value['delay'];
					$update_count_item_result = $this->conexion->query($update_count_item_sql);
				}
				
			}
			
		} 
		
		$nextcheck_sql = "SELECT `uptime`,`delay`,`nextcheck` FROM `bm_poller`";
		$nextcheck_result = $this->conexion->queryFetch($nextcheck_sql);
		if($nextcheck_result) {
			foreach ($nextcheck_result as $key => $value) {
				$utime_delay[$value['delay']]['uptime'] = $value['uptime'];
				$utime_delay[$value['delay']]['nextcheck'] = $value['nextcheck'];
			}
			return $utime_delay;
		} else {
			return false;
		}
	}

}
?>