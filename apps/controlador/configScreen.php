<?php

class configScreen extends Control {
	
	/*******
	 * 
	 * Pantallas
	 * 
	 */
	
	public function cfgPantallas()
	{	
        $valida = $this->protect->accessPage('CONFIG_SCREENS',FALSE);
        
        $this->plantilla->loadSec("configuracion/config_screen", $valida);
        
		$valueFormNew["configScreenIDForm"] = 'configScreenFormNew';

        $grupos = $this->bmonitor->getAllGroups();

        $valueFormNew["optionGroupid"] = $grupos->formatOption();

        $value["form_new_screen"] = $this->plantilla->getOne("configuracion/config_screen_form",$valueFormNew);
        
        $button[] = array(
            'name' => 'NEW',
            'bclass' => 'add',
            'protec' => 'CONFIG_SCREEN_NEW',
            'onpress' => 'toolboxScreen'
        );

        $button[] = array(
            'name' => 'DELETE',
            'bclass' => 'delete',
            'protec' => 'CONFIG_SCREEN_DELETE',
            'onpress' => 'toolboxScreen'
        );
        
        $value["option_group"] = $grupos->formatOption();
          
        $value["button"] = $this->generate->getButton($button);
        
		$this->plantilla->set($value);
        $this->plantilla->finalize();	
	}
	
	public function getTableScreens()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'name');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
		//Total rows
		
        if(isset($getParam->groupid) && is_numeric($getParam->groupid)){
            $groupid = $getParam->groupid;
        } else {
            $groupid = 0;
        }
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total FROM `bm_screens` WHERE `groupid` = $groupid";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}

		//Rows
		
		$getRowsSQL =	"SELECT HG.`name` as groupname, S.`screenid`, S.`name`, CONCAT(S.`hsize`,' X ',`vsize`) as dimensions 
                            FROM `bm_screens` S 
                            LEFT OUTER JOIN `bm_host_groups` HG ON HG.`groupid`=S.`groupid`
                            WHERE HG.`groupid` = $groupid OR  HG.`groupid` IS NULL $var->sortSql $var->limitSql";

		$getRowsRESULT = $this->conexion->queryFetch($getRowsSQL);
        
        $CONFIG_SCREEN_NEW = $this->protect->allowed('CONFIG_SCREEN_NEW');
        $CONFIG_SCREEN_EDIT = $this->protect->allowed('CONFIG_SCREEN_EDIT');
		
		if($getRowsRESULT) {
			foreach ($getRowsRESULT as $key => $row) {

				$option = '<span id="toolbarSet">';
                if($CONFIG_SCREEN_EDIT){
                    $option .= '<button id="editScreen" onclick="editScreen('.$row['screenid'].',false)" name="editScreen">'.$this->language->EDIT.'</button>';
                }
                if($CONFIG_SCREEN_NEW){
				    $option .= '<button id="asigScreen" onclick="asigScreen('.$row['screenid'].')" name="asigScreen">'.$this->language->ASSIGN.'</button>';
				}
				$option .= '</span>';

				$data['rows'][] = array(
					'id' => $row['screenid'],
					'cell' => array(
					   "screenid" => $row['screenid'], 
					   "groupname" => $row['groupname'], 
					   "name" => $row['name'], 
					   "dimensions" => $row['dimensions'],
					   "option" => $option
                     )
				);
			}
		}
		$this->basic->jsonEncode($data);
		exit;
	}

	public function newScreen() 
	{
		$valida = $this->protect->access_page('CONFIG_SCREEN_NEW');
		if($valida) {
			$getParam = (object)$_POST;
			$result = array();
			
			$result['status'] = false;
			
			if(!is_numeric($getParam->id)) {
				/* Creando nuevo Grafico */ 
				
				$this->conexion->InicioTransaccion();
				
				$insert_sql = sprintf("INSERT INTO `bm_screens` (`name`, `hsize`, `vsize`,`groupid`)
											VALUES (%s, %s, %s, %s)",
											$this->conexion->quote($getParam->name),
											$this->conexion->quote($getParam->hsize),
											$this->conexion->quote($getParam->vsize),
											$this->conexion->quote($getParam->groupid));
											
				$insert_result = $this->conexion->query($insert_sql);
				
				$idInsert = $this->conexion->lastInsertId();
				
				if($insert_result) {
					$result['status'] = true;
				} else {
				    $result['error'] = $this->language->ITERNAL_ERROR;
				}
			} else {
				/* Editando Grafico */ 
				$this->conexion->InicioTransaccion();
				
				$update_sql = sprintf(/* BSW */ "UPDATE `bm_screens` SET `name` = '%s', `hsize` = '%s', `vsize` = '%s', `groupid` = '%s'  WHERE `screenid` = '%s'",
							$getParam->name,
							$getParam->hsize,
							$getParam->vsize,
							$getParam->groupid,
							$getParam->id);
											
				$update_result = $this->conexion->query($update_sql);
				
				if($update_result) {
					$result['status'] = true;
				} else {
				    $result['error'] = $this->language->ITERNAL_ERROR;
				}
			}
		} else {
			$result['status'] = false;
			$result['error'] = $this->language->access_deny;
			$this->basic->jsonEncode($result);
			exit;
		}
		
		if($result['status']) {
			if(!is_numeric($getParam->id))  {
				$this->protect->setAudit($_SESSION['uid'],'new_screen',"Usuario creo la pantalla: [$idInsert] ".$getParam->name);
			} else {
				$this->protect->setAudit($_SESSION['uid'],'edit_screen',"Usuario edito la pantalla: [$getParam->id] ".$getParam->name);
			}
			$this->conexion->commit();
		} else {
			$this->conexion->rollBack();
		}
		$this->basic->cacheDeleteAll();
		$this->basic->jsonEncode($result);
		exit;
	}

	public function deleteScreen()
	{
		$valida = $this->protect->access_page('CONFIG_SCREEN_DELETE');
		
		$data['status'] = true ;
		
		if($valida) { 
			$getParam = (object)$_POST;
			
			foreach ($getParam->idSelect as $key => $value) {
				if(is_numeric($value)) {
					
					$delete_sql = "/* BMONITOR */ DELETE FROM `bm_screens` WHERE `screenid` IN ('$value')";
										
					$valida = $this->conexion->query($delete_sql);
					
					if(!$valida) {
						$data['status'] = false;
					}
					
				}
			}
					
		} else {
			$data['status'] = false;
		}
        $this->basic->cacheDeleteAll();
		echo json_encode($data);
	}
	
	public function getScreenForm()
	{
		if(is_numeric($_POST['IDSelect'])) {
			$IDSelect = $_POST['IDSelect'];
		} else {
			return false;
		}
		$valida = $this->protect->access_page('CONFIG_SCREEN_EDIT');
		$this->plantilla->loadSec("configuracion/config_screen_form", $valida, 360000);
         
		
		$value["configScreenIDForm"] = 'configScreenFormEdit';
        		
		// Valores
		
		$get_value_sql = "SELECT * FROM `bm_screens`  WHERE `screenid` = $IDSelect";
						
		$get_value_result = $this->conexion->queryFetch($get_value_sql);

		if($get_value_result) {
			$get_value_rows = $get_value_result[0];
			foreach ($get_value_rows as $key => $valores) {
				$value[$key] = $valores;
			}
		}
        
        $grupos = $this->bmonitor->getGroupsHost();;
        
        $value["optionGroupid"] = $this->basic->getOption($grupos, 'groupid', 'name',$value["groupid"]);
        	
		$this->plantilla->set($value);
		$this->plantilla->finalize();
	}

	public function getScreenAsig()
	{
		$getParam = (object)$_POST;
		
		if(is_numeric($getParam->IDSelect)) {
			$IDSelect = $getParam->IDSelect;
		} else {
			return false;
		}
		
		//Cargando graficos
		
		$select_sql = "SELECT `id_graph`, `name` FROM `bm_graphs`";
		
		$select_result_graph = $this->conexion->queryFetch($select_sql);
		
				
		$select_sql = "SELECT `hsize`,`vsize`,`name` FROM `bm_screens`  WHERE `screenid` = $IDSelect";
		
		$select_result_screen = $this->conexion->queryFetch($select_sql);
		
		$colum = $select_result_screen[0]['hsize'];
		$filas = $select_result_screen[0]['vsize'];
		$result['name_screen'] = $select_result_screen[0]['name'];
		
		$result['screen_dialog_title'] = str_replace("{SCREEN_NAME}", $result['name_screen'], $this->language->SCREEN_TITLE_ASIG_DIALOG);
		
		
		$select_sql = "SELECT `id_graph`,`hsize`,`vsize` FROM `bm_screens_items`  WHERE `screenid` = $IDSelect";
		
		$select_result_screen_items = $this->conexion->queryFetch($select_sql);

		$items = array();
		
		foreach ($select_result_screen_items as $key => $item) {
			$items[$item['hsize'].'_'.$item['vsize']] = $item['id_graph'];
		}
		
		$form = '<form id="config_screen_form_asig" class="form_sonda">';
		
		for ($c=1; $c <=  $colum; $c++) { 
			$form .= '<div class="column">';
			
			for ($f=1; $f <=  $filas; $f++) {
				 
				$form .= '<div class="portlet">';
				$form .= '<div class="portlet-header">'.$this->language->COLUMNS.':'.$c.' '.$this->language->ROWS.':'.$f.'</div>';
				$form .= '<div class="portlet-content">'; 
                  
				$form .= '<label for="screen_graph_'.$c.'_'.$f.'" class="col1">'.$this->language->OPTION_CHART.'</label><span class="col2">';
				$form .= '<select name="screen_graph_'.$c.'_'.$f.'" id="screen_graph_'.$c.'_'.$f.'" class="select">';
				$form .= '<option value="0">'.$this->language->SELECT.'</option>';
				foreach ($select_result_graph as $key => $graph) {
					if(isset($items[$c.'_'.$f]) && ($items[$c.'_'.$f] == $graph['id_graph'])) {
						$selected = 'selected';
					} else {
						$selected = '';
					}
					$form .= '<option '.$selected.' value="'.$graph['id_graph'].'">'.$graph['name'].'</option>';
				}
				
				$form .= '</select></span>';
				$form .= '</div>';
				$form .= '</div>';
			
			}

			$form .= '</div>';
		}
		
		$form .= '</form>';
		
		$result['status'] = true;
		$result['html'] = $form;
		
		echo json_encode($result);
	}

	public function asigScreen()
	{
		$getParam = (object)$_POST;
		
		if(is_numeric($getParam->id)) {
			$IDSelect = $getParam->id;
		} else {
			return false;
		}
		
		$delete_sql = "/* BMONITOR */ DELETE FROM `bm_screens_items` WHERE `screenid` = '$IDSelect'";
										
		$valida = $this->conexion->query($delete_sql);
		
		$insert_sql = "INSERT INTO `bm_screens_items` (`screenid`, `hsize`, `vsize`, `id_graph`) VALUES ";
		
		$insert_value = array();

		foreach ($getParam as $key => $value) {
			if(preg_match("/^screen_graph_/i",$key) && ( $value > 0)) {
				$keyName = explode("_", $key,4);
			
				$insert_value[] = "($IDSelect, $keyName[2], $keyName[3], $value)";

			}
		}
		
		if(count($insert_value) > 0) {
			$insert_value = join(',',$insert_value);
		
		
			$valida = $this->conexion->query($insert_sql.$insert_value);
			if($valida) {
				$data['status'] = true;
			} else {
				$data['status'] = false;
			}			
			
		} else {
			$data['status'] = true;
		}
		echo json_encode($data);
	}
}
?>