<?php
ini_set('max_execution_time', 600);
class pollerv1 extends Control {

	var $delay_box = array();
	var $_logs = 'START';
	var $crontab = 'index';

	public function start($crontab = "index") {
		$this->crontab = $crontab;

		if ($crontab == 'index') {
			$logs = 'START';
			$this->_logs = $logs;
		} elseif ($crontab == 'retry') {
			$logs = 'RETRY';
			$this->_logs = $logs;
		} else {
			exit ;
		}

		$this->logs->debug("[POLLER_$logs] ===============================================", NULL, 'logs_poller');
		$this->logs->debug("[POLLER_$logs] Starting poller", NULL, 'logs_poller');
		$this->logs->debug("[POLLER_$logs] Copyright Baking Software 2012", NULL, 'logs_poller');
		$this->logs->debug("[POLLER_$logs] Version 7", NULL, 'logs_poller');
		$this->logs->debug("[POLLER_$logs] ===============================================", NULL, 'logs_poller');

		//Refrescando delay :

		$get_delay_sql = 'SELECT `delay` FROM `bm_poller`';

		$get_delay_result = $this->conexion->queryFetch($get_delay_sql, 'logs_poller');

		if ($get_delay_result) {
			foreach ($get_delay_result as $key => $delay) {
				$this->getDelay($delay['delay']);
			}
		}

		$active = $this->_crontab("poller_crontab_$crontab", "start", TRUE, 600);

		if ($active) {
			$this->logs->debug("[POLLER_$logs] Starting OK", NULL, 'logs_poller');
		} else {
			$this->logs->warning("[POLLER_$logs] Starting NOK", NULL, 'logs_poller');
			exit ;
		}

		//Clean poller

		$clean_poller_sql = "DELETE FROM  `bm_poller_active` WHERE `fechahora` <  DATE_SUB(NOW(),INTERVAL 600 SECOND)";
		$this->conexion->query($clean_poller_sql);

		//Get limit

		$get_limit_sql = "SELECT COUNT(`id_poller`) As TOTAL FROM `bm_poller_active`";

		$get_limit_sql = $this->conexion->queryFetch($get_limit_sql, 'logs_poller');

		if ($get_limit_sql) {

			$limit_max = $this->parametro->get('GET_MAX_POLLER', 5);

			$active = $get_limit_sql[0]['TOTAL'];

			$limit_permit = $limit_max - $active;

			if ($limit_permit > 0) {
				$this->logs->debug("[POLLER_$logs] Iniciando poller con un limite de [$limit_permit/$limit_max] hilos", NULL, 'logs_poller');
				return $limit_permit;
			} else {
				$this->logs->error("[POLLER_$logs] Poller inicial supero el maximo permitido:", $limit_max, 'logs_poller');
				$this->_crontab("poller_crontab_$crontab", "finish");
				exit ;
			}

		} else {
			$this->_crontab("poller_crontab_$crontab", "finish");
			exit ;
		}
	}

	private function fixMonitores() {

		$clean_item_error_asigg_sql = "DELETE IP FROM `bm_item_profile` IP
	LEFT JOIN `bm_host` H ON H.`id_host`=IP.`id_host`
	LEFT OUTER JOIN `bm_items_groups` IG ON ( IG.`id_item` = IP.`id_item` AND IG.`groupid` = H.`groupid`)
	WHERE IG.id IS NULL AND IP.`id_item` NOT IN  (1,3,4)";

		$clean_item_error_asigg_result = $this->conexion->queryFetch($clean_item_error_asigg_sql, 'logs_poller');

		$getMonitores_error_sql = "INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`) 
					 SELECT IG.`id_item`,H.`id_host`,I.`snmp_oid`, I.`snmp_port`,I.`snmp_community`,NOW() as nextcheck
										FROM `bm_host` H
										LEFT JOIN `bm_items_groups` IG USING(`groupid`) 
										LEFT JOIN `bm_items` I ON  I.`id_item`=IG.`id_item`
										LEFT OUTER JOIN `bm_item_profile` IP ON IP.`id_host`=H.`id_host` AND IP.`id_item`=IG.`id_item`
										WHERE IP.`id_item` IS NULL AND H.`borrado` = 0 ";

		$getMonitores_error_result = $this->conexion->queryFetch($getMonitores_error_sql, 'logs_poller');

		$getMonitores_error_sql = "INSERT IGNORE INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`)  SELECT  I.`id_item`,H.`id_host`,I.`snmp_oid`, I.`snmp_port`,I.`snmp_community`,NOW() as nextcheck 
                FROM `bm_host` H  , `bm_items` I
                WHERE I.`id_item` IN (1,3,4)";

		$getMonitores_error_result = $this->conexion->queryFetch($getMonitores_error_sql, 'logs_poller');

	}

	private function getDelay($delay) {

		if (isset($this->delay_box[$delay])) {
			return (object)$this->delay_box[$delay];
		}

		$time = time();
		$time_f = substr($time, 0, -1) . "0";

		$fecha = date('Y-m-d H:i:s');
		$nextcheck = strtotime("+$delay second", strtotime($fecha));
		$nextcheck = date('Y-m-d H:i:00', $nextcheck);

		$logs = $this->_logs;
		$crontab = $this->crontab;

		$get_delay_crontab_sql = "SELECT COUNT(`id_poller` ) AS Total , `uptime`, `nextcheck` FROM `bm_poller` WHERE `delay` = $delay AND NOW() < `nextcheck` LIMIT 1";

		$get_delay_crontab_result = $this->conexion->queryFetch($get_delay_crontab_sql, 'logs_poller');

		if ($get_delay_crontab_result) {
			$row = (object)$get_delay_crontab_result[0];

			if (($row->Total > 0) && (is_numeric($row->uptime))) {
				$this->delay_box[$delay]['uptime'] = $row->uptime;
				$this->delay_box[$delay]['nextcheck'] = $row->nextcheck;
				return (object)$this->delay_box[$delay];
			} else {

				$insert_delay_sql = "INSERT INTO `bm_poller` (`uptime`, `delay`, `item_poller`, `fecha_update`, `startcheck`, `nextcheck`)
										VALUES ( $time_f , $delay , 0, NOW(), NOW(), '$nextcheck') ON DUPLICATE KEY UPDATE `uptime` = $time_f, `fecha_update` = NOW(), `startcheck` = NOW(), `nextcheck` = '$nextcheck' ";
				$insert_delay_result = $this->conexion->query($insert_delay_sql, false, 'logs_poller');

				if ($insert_delay_result) {
					$this->delay_box[$delay]['uptime'] = $time_f;
					$this->delay_box[$delay]['nextcheck'] = $nextcheck;

					return (object)$this->delay_box[$delay];
				} else {
					$this->logs->error("[POLLER_$logs] Error al obtener delay_unix", NULL, 'logs_poller');
					$this->_crontab("poller_crontab_$crontab", "finish");
					exit ;
				}
			}
		} else {
			$this->logs->error("[POLLER_$logs] Error al obtener delay_unix", NULL, 'logs_poller');
			$this->_crontab("poller_crontab_$crontab", "finish");
			exit ;
		}
	}

	public function index() {
		$limit = $this->start("index");

		$this->fixMonitores();

		$logs = $this->_logs;
		$crontab = $this->crontab;

		$get_host_sql = "SELECT  profile.`id_item_profile`, host.`host`, host.`id_host`, host.`dns`, 
        		item.`id_item`, IF( HG.`delay_bsw` > 0 ,  HG.`delay_bsw`,item.`delay` ) as delay,
        			item.`type_item`, item.`description` , host.`ip_wan` , item.`snmp_oid`, 
        			item.`snmp_community`, item.`snmp_port`, PACTIVE.`id_poller`, HG.`horario`, HG.`feriados`, HG.`snmp_monitor`
			FROM `bm_host` host
			LEFT JOIN `bm_item_profile` profile USING(`id_host`)
			LEFT JOIN  `bm_items` item ON profile.`id_item`=item.`id_item`
			LEFT JOIN  `bm_items_groups` IG ON (IG.`id_item`=item.`id_item` AND host.`groupid`=IG.`groupid` )
			LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=host.`groupid`
			LEFT OUTER JOIN `bm_poller_active` PACTIVE ON ( PACTIVE.`id_host`=host.`id_host` AND PACTIVE.`id_item`=item.`id_item`  )
			WHERE host.`borrado`=0 AND IG.`status`= 1 AND host.`status` = 1 AND profile.`nextcheck`  < NOW()  
				AND item.`type_poller` = 'snmp' AND ( ( profile.`status`='pendiente' OR profile.`status`='ok' )  OR ( profile.`nextcheck`  < DATE_SUB(NOW(), INTERVAL 600 SECOND) AND profile.`status`='process'  ) ) 
					AND PACTIVE.`id_poller` IS NULL LIMIT " . $limit;

		$get_host_result = $this->conexion->queryFetch($get_host_sql, 'logs_poller');

		if ($get_host_result) {

			$insert_poller_sql = "INSERT INTO `bm_poller_active` (`id_host`, `id_item`, `fechahora`, `status`,`uptime`,`id_item_profile`) VALUES ";

			$insert_poller_sql_value = array();

			$URLBASEFULL = URL_BASE_FULL;

			if (!isset($URLBASEFULL) || $URLBASEFULL == '') {
				$URLBASEFULL = $this->parametro->get("URL_BASE") . "/";
			}

			foreach ($get_host_result as $key => $host) {

				$delay_unix = $this->getDelay($host['delay']);

				$urlInicial = $URLBASEFULL . 'pollerv1/process/' . $host['id_item_profile'];

				$post['type_poller'] = 'index';
				$post['pipe'] = $key;

				$post['uptime'] = $delay_unix->uptime;
				$post['nextcheck'] = $delay_unix->nextcheck;
				$post['host'] = $host['host'];
				$post['id_host'] = $host['id_host'];
				$post['dns'] = $host['dns'];
				$post['id_item'] = $host['id_item'];
				$post['type_item'] = $host['type_item'];
				$post['description'] = $host['description'];
				$post['ip_wan'] = $host['ip_wan'];

				$post['snmpMonitor'] = $host['snmp_monitor'];

				$post['snmp_oid'] = $host['snmp_oid'];
				$post['snmp_community'] = $host['snmp_community'];
				$post['snmp_port'] = $host['snmp_port'];

				$post['horario'] = $host['horario'];
				$post['feriados'] = $host['feriados'];

				$insert_poller_sql_value[] = sprintf("( %d , %d, NOW(), 'active',%d,%d)", $host['id_host'], $host['id_item'], $delay_unix->uptime, $host['id_item_profile']);

				$this->curl->cargar($urlInicial, $host['id_item_profile'], $post);
			}

			$insert_poller_result = $this->conexion->query($insert_poller_sql . join(',', $insert_poller_sql_value), false, 'logs_poller');

			$this->curl->ejecutar();

			$this->_crontab("poller_crontab_$crontab", "finish");
		} else {
			$this->_crontab("poller_crontab_$crontab", "finish");
		}
	}

	public function retry() {
		$limit = $this->start("retry");

		$this->fixMonitores();

		$pid = getmypid();

		$logs = $this->_logs;
		$crontab = $this->crontab;

		$get_host_sql = "SELECT  
							IP.`id_item_profile`, 
							H.`host`, 
							H.`id_host`, 
							H.`dns`, 
							I.`id_item`, 
							IF( HG.`delay_bsw` > 0 ,  HG.`delay_bsw`,I.`delay` ) as delay,
							I.`type_item`, I.`description` , H.`ip_wan` , I.`snmp_oid`, I.`snmp_community`, I.`snmp_port`, PA.`id_poller`,
							HG.`horario`, HG.`feriados`,
							HG.`snmp_monitor`
						FROM `bm_item_profile` IP
						LEFT JOIN `bm_items` I ON I.`id_item`=IP.`id_item`
						LEFT JOIN `bm_host` H ON H.`id_host`=IP.`id_host`
						LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=H.`groupid`
						LEFT OUTER JOIN `bm_poller_active` PA ON  PA.`id_item_profile`=IP.`id_item_profile`
						WHERE `type_poller` = 'snmp' AND H.`borrado`=0 AND H.`status`=1 AND IP.`nextcheck` < NOW() AND
						(
							( IP.`status`='error' OR IP.`status`='retry' ) OR ( (IP.`nextcheck`  < DATE_SUB(NOW(),INTERVAL 600 SECOND)) AND IP.`status`='processr'  ) 
						) AND  (PA.`id_poller` IS NULL OR PA.`fechahora`  <  DATE_SUB(NOW(),INTERVAL 600 SECOND) ) LIMIT " . $limit;

		$get_host_result = $this->conexion->queryFetch($get_host_sql, 'logs_poller');

		if ($get_host_result) {

			$insert_poller_sql = "INSERT INTO `bm_poller_active` (`id_host`, `id_item`, `fechahora`, `status`,`uptime`,`id_item_profile`,`pid`) VALUES ";

			$insert_poller_sql_value = array();

			$URLBASEFULL = URL_BASE_FULL;

			if (!isset($URLBASEFULL) || $URLBASEFULL == '') {
				$URLBASEFULL = $this->parametro->get("URL_BASE") . "/";
			}

			foreach ($get_host_result as $key => $host) {

				$delay_unix = $this->getDelay($host['delay']);

				$urlInicial = $URLBASEFULL . 'pollerv1/process/' . $host['id_item_profile'];

				$post['type_poller'] = 'retry';

				$post['pipe'] = $key;

				$post['pid'] = $pid;

				$post['uptime'] = $delay_unix->uptime;
				$post['nextcheck'] = $delay_unix->nextcheck;
				$post['host'] = $host['host'];
				$post['id_host'] = $host['id_host'];
				$post['dns'] = $host['dns'];
				$post['id_item'] = $host['id_item'];
				$post['type_item'] = $host['type_item'];
				$post['description'] = $host['description'];
				$post['ip_wan'] = $host['ip_wan'];
				$post['snmp_oid'] = $host['snmp_oid'];
				$post['snmp_community'] = $host['snmp_community'];
				$post['snmp_port'] = $host['snmp_port'];
				$post['snmpMonitor'] = $host['snmp_monitor'];
				$post['horario'] = $host['horario'];
				$post['feriados'] = $host['feriados'];

				$insert_poller_sql_value[] = sprintf("( %d , %d, NOW(), 'active',%d,%d,%d)", $host['id_host'], $host['id_item'], $delay_unix->uptime, $host['id_item_profile'], $pid);

				$this->curl->cargar($urlInicial, $host['id_item_profile'], $post);
			}

			$insert_poller_result = $this->conexion->query($insert_poller_sql . join(',', $insert_poller_sql_value) . " ON DUPLICATE KEY UPDATE `status`=VALUES(`status`)", false, 'logs_poller');

			$this->curl->ejecutar();
			$this->_crontab("poller_crontab_$crontab", "finish");
		} else {
			$this->_crontab("poller_crontab_$crontab", "finish");
		}
	}

	public function process($id_item) {

		$getParam = (object)$_POST;

		if ($getParam->type_poller == 'index') {
			$poler_log_text = '[POLLER_START]';
			$status = 'process';
			$timeout = (int)$this->parametro->get("SNMP_TIMEOUT_POLLER", 1000000);
			$retry = 1;
			$version = $this->parametro->get("SNMP_VERSION_POLLER", '2c');
		} else {
			$status = 'processr';
			$poler_log_text = '[POLLER_RETRY]';
			$timeout = (int)$this->parametro->get("SNMP_TIMEOUT_RETRY", 2000000);
			$retry = 2;
			$version = $this->parametro->get("SNMP_VERSION_RETRY", '2c');
		}

		$this->logs->debug("$poler_log_text Hilo numero $getParam->pipe , activo", NULL, 'logs_poller');

		if (!is_numeric($id_item)) {
			return false;
		}

		$update_query = "INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`, `lastclock`, `lastvalue`, `prevvalue`, `error`, `retry`, `status`, `active`)
						VALUES ($getParam->id_item, $getParam->id_host, '$getParam->snmp_oid', $getParam->snmp_port, '$getParam->snmp_community', NOW(), NULL, NULL, NULL, NULL, 0, '$status', 'true')
						ON DUPLICATE KEY UPDATE  error=NULL, `status`='$status', `nextcheck` = NOW() ";

		$result = $this->conexion->query($update_query, false, 'logs_poller');

		$ip = $getParam->ip_wan . ":" . $getParam->snmp_port;

		switch ($getParam->type_item) {
			case 'float' :
				$tablehistory = "bm_history";
				break;

			case 'string' :
				$tablehistory = "bm_history_str";
				break;

			case 'text' :
				$tablehistory = "bm_history_text";
				break;

			case 'log' :
				$tablehistory = "bm_history_uint";
				break;

			default :
				$tablehistory = "bm_history";
				break;
		}

		//compliance schedule

		$c_schedule = $this->basic->cdateObj($getParam->horario, $getParam->feriados);

		if (($c_schedule == FALSE) || (is_object($c_schedule))) {

			if (isset($c_schedule->nexcheck)) {
				$getParam->nextcheck = $c_schedule->nexcheck;
			}

			$update_query = "UPDATE `bm_item_profile` SET `error`='Fuera de horario', `status`='error' ,`nextcheck` = '$getParam->nextcheck' , `lastclock` =" . $getParam->uptime . ",prevvalue=lastvalue,lastvalue=0 WHERE id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";

			$this->conexion->query($update_query, false, 'logs_poller');

			// Liberando poller
			$delete_poller_sql = sprintf("DELETE FROM `bm_poller_active` WHERE `id_host` = %d AND `id_item` = %d", $getParam->id_host, $getParam->id_item);
			$this->conexion->query($delete_poller_sql, false, 'logs_poller');

			header("Content-Length: 0");
			header("Connection: close");
			flush();

			exit ;
		}

		if ($getParam->snmpMonitor == 'false') {

			if ((int)$getParam->id_item === 1) {

				//Get Status
				$itemStatus = $this->parametro->get("DASHBOARD_ITEM_MONITOR_SONDA", 4);

				$getStatusAgent = "SELECT NOW() as fechahora, H.`id_host`, H.`groupid`, H.`host`,
        IF((IF(IP.`lastclock` IS NULL, 1, IP.`lastclock`)+(I.`delay`*3)) > UNIX_TIMESTAMP(NOW()),1,0) as STATUS_SONDA, 
        IF(IP.`lastclock` IS NULL ,'1970-01-01 00:00:00',FROM_UNIXTIME(IP.`lastclock`)) AS updateSONDA,
        H.`dns`
            FROM `bm_host`  H 
                LEFT OUTER JOIN `bm_item_profile` IP USING (`id_host`)
                LEFT JOIN `bm_host_groups` HG ON HG.`groupid` = H.`groupid`
                LEFT OUTER JOIN `bm_items` I ON I.`id_item`=IP.`id_item`
            WHERE H.`borrado` = 0 AND H.`status`=1 AND IP.`id_item` = $itemStatus  AND H.`id_host` = $getParam->id_host";

				$getStatusAgentREsult = $this->conexion->queryFetch($getStatusAgent);

				if ($getStatusAgentREsult) {
					$status = $getStatusAgentREsult[0]['STATUS_SONDA'];
				} else {
					$status = 0;
				}

				if (((int)$getParam->id_item === 1) && ($status == 1)) {
					$check_ok = ', `check_ok` = NOW()';
				} else {
					$check_ok = '';
				}

				$update_query = "UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  
	                            `status`='ok' $check_ok ,
	                            `nextcheck` = '$getParam->nextcheck' , 
	                            `okcheck` = NOW() , 
	                            `lastclock` =" . $getParam->uptime . ",
	                            prevvalue=lastvalue,lastvalue=1 
	                            where id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";
				$result = $this->conexion->query($update_query, false, 'logs_poller');

				$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '$status' WHERE `id_host` = '$getParam->id_host';";
				$this->conexion->query($set_status_host_sql, false, 'logs_poller');

			} else {
				$update_query = "UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  
	                            `status`='ok',
	                            `nextcheck` = '$getParam->nextcheck' , 
	                            `okcheck` = NOW() , 
	                            `lastclock` =" . $getParam->uptime . ",
	                            prevvalue=lastvalue,lastvalue=0 
	                            where id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";
	            $result = $this->conexion->query($update_query, false, 'logs_poller');
	                            
			}

			header("Content-Length: 0");
			header("Connection: close");
			flush();

			exit ;
			
		}

		if (($getParam->ip_wan == '0.0.0.0') || ($getParam->ip_wan == '1.1.1.1') || ($getParam->ip_wan == '1.0.0.0')) {
			$update_query = "UPDATE `bm_item_profile` SET `error`='Invalid IP', `status`='error' ,`nextcheck` = '$getParam->nextcheck' , `lastclock` =" . $getParam->uptime . ",prevvalue=lastvalue,lastvalue=0 WHERE id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";

			$this->conexion->query($update_query, false, 'logs_poller');

			$insert_query = "/* BSW - Poller NOK */  INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->id_item,$getParam->id_host,$getParam->uptime, 0 ,0)";
			$this->conexion->query($insert_query, false, 'logs_poller');

			// Liberando poller

			$delete_poller_sql = sprintf("DELETE FROM `bm_poller_active` WHERE `id_host` = %d AND `id_item` = %d", $getParam->id_host, $getParam->id_item);
			$this->conexion->query($delete_poller_sql, false, 'logs_poller');

			//Status Host
			if ((int)$getParam->id_item === 1) {
				$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '0' WHERE `id_host` = '$getParam->id_host';";
				$this->conexion->query($set_status_host_sql, false, 'logs_poller');
			}

			header("Content-Length: 0");
			header("Connection: close");
			flush();

			exit ;
		}

		//Validamos si el modulo o paquete php_snmp se encuentra activo
		if (function_exists('snmp2_get')) {
			//Via modulo Inicio
			$time_start = microtime(true);
			$this->logs->debug("$poler_log_text Iniciando consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  ", NULL, 'logs_poller');
			$result = @snmp2_get($ip, $getParam->snmp_community, $getParam->snmp_oid, $timeout, $retry);
			$time_end = microtime(true);
			$time = $time_end - $time_start;

			if ($result) {

				$this->logs->debug("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  , tiempo: $time respondio: ", $result, 'logs_poller');

				//validaciones

				if (strpos($result, 'uci') !== false) {
					$this->logs->error("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid] respondio con Error: ", $result, 'logs_poller');
					$valorSnmp = 'UCI : Entry not found';
					$tipoValorSnmp = 'STRING';
				} elseif (strpos($result, 'OID') !== false) {
					$this->logs->error("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid] respondio con Error: ", $result, 'logs_poller');
					$valorSnmp = 'No Such Object available on this agent at this OID';
					$tipoValorSnmp = 'STRING';
				} else {
					list($tipoValorSnmp, $valorSnmp) = explode(": ", $result);
				}

				if (($tipoValorSnmp == 'STRING') && (!preg_match('/"/i', $valorSnmp))) {
					$valorSnmp = '"' . $valorSnmp . '"';
				}

				$filtroObligatorio = false;
				if (($tipoValorSnmp == 'STRING') && ($getParam->type_item == 'float')) {
					$this->logs->warning("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string $valorSnmp", NULL, 'logs_poller');
					$filtroObligatorio = true;
				}

				$this->logs->debug("$poler_log_text Buscando filtro especial", NULL, 'logs_poller');

				$filterStatus = $this->filter($getParam->description, $getParam->dns, $valorSnmp, $poler_log_text);

				if ($filterStatus) {
					$valorSnmp = $this->snmpFilterValue;
				}

				if (($filterStatus == false) && ($filtroObligatorio)) {
					$this->logs->error("$poler_log_text Revisar: Se esperaba un valor float y se recibio un string y no tiene filtro", NULL, 'logs_poller');
					exit ;
				}

				$query = "/* BSW - Poller OK */ INSERT INTO `$tablehistory` (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->id_item,$getParam->id_host,$getParam->uptime, $valorSnmp ,1)";
				$this->conexion->query($query, true, 'logs_poller');

				if (empty($getParam->lastvalue)) {
					$getParam->lastvalue = "0";
				}

				if (((int)$getParam->id_item === 1) && ($valorSnmp == 1)) {
					$check_ok = ', `check_ok` = NOW()';
				} else {
					$check_ok = '';
				}

				if (is_numeric($valorSnmp)) {
					$valorSnmp = 'abs(' . $valorSnmp . ')';
				}

				$update_query = "UPDATE `bm_item_profile` JOIN `bm_items` USING(`id_item`) SET error=NULL,  
                                `status`='ok' $check_ok ,
                                `nextcheck` = '$getParam->nextcheck' , 
                                `okcheck` = NOW() , 
                                `lastclock` =" . $getParam->uptime . ",prevvalue=lastvalue,lastvalue=" . $valorSnmp . " where id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";
				$result = $this->conexion->query($update_query, false, 'logs_poller');

				//Status Host
				if ((int)$getParam->id_item === 1) {
					$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '1' WHERE `id_host` = '$getParam->id_host';";
					$this->conexion->query($set_status_host_sql, false, 'logs_poller');
				}

				// Liberando poller

				$delete_poller_sql = sprintf("DELETE FROM `bm_poller_active` WHERE `id_host` = %d AND `id_item` = %d", $getParam->id_host, $getParam->id_item);
				$this->conexion->query($delete_poller_sql, false, 'logs_poller');

				header("Content-Length: 0");
				header("Connection: close");
				flush();

			} else {
				$this->logs->warning("$poler_log_text La consulta snmp -> host: [$getParam->host] ip: [$ip] objetid: [$getParam->snmp_oid]  , tiempo: $time termino en Timeout", $result, 'logs_poller');

				//Notificando error
				if ($getParam->type_poller == 'index') {
					$update_query = "UPDATE `bm_item_profile` SET `status`='retry' ,`nextcheck` = NOW() WHERE id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";
					$this->conexion->query($update_query, false, 'logs_poller');
					$update_query = "UPDATE `bm_poller_active` SET `status`='retry' WHERE `id_item`=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";
					$this->conexion->query($update_query, false, 'logs_poller');
				} else {

					//Validando duplicados

					$get_active_profile_SQL = "SELECT IF(`nextcheck` > NOW(),1,0) As Total , IF( PA.`pid` = " . $getParam->pid . ", 1,0) as PID
												FROM `bm_item_profile` IP
												LEFT OUTER JOIN `bm_poller_active` PA ON  PA.`id_item_profile`=IP.`id_item_profile`
												WHERE IP.id_item=" . $getParam->id_item . " AND IP.`id_host`=$getParam->id_host";

					$get_active_profile_result = $this->conexion->queryFetch($get_active_profile_SQL, 'logs_poller');

					if ($get_active_profile_result && ((int)$get_active_profile_result[0]['Total'] == 0) && ((int)$get_active_profile_result[0]['PID'] == 1)) {

						$update_query = "UPDATE `bm_item_profile` SET `error`='Router no responde', `status`='error' ,`nextcheck` = '$getParam->nextcheck' , `lastclock` =" . $getParam->uptime . ",prevvalue=lastvalue,lastvalue=0 WHERE id_item=" . $getParam->id_item . " AND `id_host`=$getParam->id_host";

						$this->conexion->query($update_query, false, 'logs_poller');

						$insert_query = "/* BSW - Poller NOK */  INSERT INTO $tablehistory (`id_item`,`id_host`,`clock`,`value`,`valid`) values ( $getParam->id_item,$getParam->id_host,$getParam->uptime, 0 ,0)";
						$this->conexion->query($insert_query, false, 'logs_poller');

						// Liberando poller

						$delete_poller_sql = sprintf("DELETE FROM `bm_poller_active` WHERE `id_host` = %d AND `id_item` = %d", $getParam->id_host, $getParam->id_item);
						$this->conexion->query($delete_poller_sql, false, 'logs_poller');

						//Status Host
						if ((int)$getParam->id_item === 1) {
							$set_status_host_sql = "/* BSW */ UPDATE `bm_host` SET `availability` = '0' WHERE `id_host` = '$getParam->id_host';";
							$this->conexion->query($set_status_host_sql, false, 'logs_poller');
						}

					}

					header("Content-Length: 0");
					header("Connection: close");
					flush();
				}
			}
			//Via modulo Fin
		} else {
			//Via comando Inicio
			$this->logs->error("El modulo snmp no existe por lo cual se intenta via comando", NULL, 'logs_poller');
			$retval = shell_exec("whereis snmpget | wc -w");
			if ($retval == 1) {
				$this->logs->error("Error el comando snmpget no existe", NULL, 'logs_poller');
				exit ;
			}
			$snmpget_cmd = "snmpget -t10 -Oqv -v$version -c$getParam->snmp_community $ip $getParam->snmp_oid";
			$snmpget = shell_exec($snmpget_cmd);
			$this->logs->info("El comando snmp: ", $snmpget_cmd . " Respondio: " . $snmpget, 'logs_poller');
			//Via comando Fin
		}
	}

	private function filter($description, $dns, $valor, $poler_log_text) {
		switch ($description) {
			case 'Availability.bsw' :
				if (preg_match("/$dns/i", $valor)) {
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor", NULL, 'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("$poler_log_text Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor", NULL, 'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
				break;

			case 'Availability-ForTrap.bsw' :
				if (preg_match("/$dns/i", $valor)) {
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 1, $dns ->  $valor", NULL, 'logs_poller');
					$this->snmpFilterValue = '1';
					return true;
				} else {
					$this->logs->debug("Filtro  encontrado, se tranforma valor inicial en 0, $dns ->  $valor", NULL, 'logs_poller');
					$this->snmpFilterValue = '0';
					return true;
				}
				break;

			default :
				return false;
				break;
		}
	}

}
?>