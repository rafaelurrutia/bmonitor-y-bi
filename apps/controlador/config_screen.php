<?php

class configScreen extends Control {
	
	/*******
	 * 
	 * Pantallas
	 * 
	 */
	
	public function cfgPantallas()
	{
		$value = array();
		
		//Formulario nuevo 
		$this->plantilla->load("form_new_screen");
		
		$value["form_new_group"] = $this->plantilla->get();
		
		//Tabla
		
		$this->basic->setTable_id('table_screen');
		$this->basic->setTable_url("/config/getTableScreens");
		$this->basic->setTable_colModel('config_screen');
		$this->basic->setTable_toolbox('toolboxScreen');
		
		$value["table_groups"] = $this->basic->getTableFL('Grupos');
		
		//Generar Plantilla
		
		$valida = $this->protect->access_page('CONFIG_SCREEN_LIST');
		
		$this->plantilla->load_sec("config_screen", "denegado",$valida);
	
		$this->plantilla->set($value);
		
		echo $this->plantilla->get();		
	}
	
	public function getTableScreens()
	{
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST,'name');
		
		//Parametros Table
		
		$data = array();
		
		$data['page'] = $var->page;
		
		$data['rows'] = array();
		
		//Total rows
		
		$getTotalRows_sql = "SELECT COUNT(*) as Total FROM `bm_screens`";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);
		
		if($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['Total'];
		else {
			$data['total'] = 0;
		}
		
		//Rows
		
		$getRows_sql =	"SELECT * FROM `bm_screens` $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);
		
		if($getRows_result) {
			foreach ($getRows_result as $key => $row) {
				$option = '<a href="#" id="editScreen" onclick="editScreen('.$row['screenid'].',false)" name="editScreen" class="buttonDefault">Editar</a><a href="#" id="asigScreen" onclick="asigScreen('.$row['screenid'].',true)" name="asigScreen" class="buttonDefault">Asign</a>';
			
				$data['rows'][] = array(
					'id' => $row['screenid'],
					'cell' => array($row['name'], $row['hsize'].' X '.$row['vsize'],$option)
				);
			}
		}
		echo json_encode($data);
	}
}
?>