<?php
class profiles2 extends Control {

	public function cfgProfiles() {

		$valida = $this->protect->access_page('CONFIG_PROFILES');

		$this->plantilla->loadSec("configuracion/profiles/config_profiles", $valida, 86000);

		//Formulario nuevo

		$valueFormNew["formID"] = 'formNewProfile';
		$valueFormNew["groupidList"] = $this->bmonitor->getAllGroups()->formatOption();

		$value["formNewProfile"] = $this->plantilla->getOne("configuracion/profiles/configProfilesForm", $valueFormNew);

		$valueFormNew["formID"] = 'formStructureNew';
		$value["formStructureNew"] = $this->plantilla->getOne("configuracion/profiles/configCategoryForm", $valueFormNew);

		$valueFormNew["optionTypeData"] = $this->basic->getOptionValue('type_data', 'string');
		$valueFormNew["formID"] = 'formNewItemParam';
		
		
		$value["formNewItemParam"] = $this->plantilla->getOne("configuracion/profiles/configItemParamForm" , $valueFormNew);

		$typeDate = array(
			0 => array('value' => 'float', 'option' => 'Float'),
			1 => array('value' => 'string', 'option' => 'String'),
			2 => array('value' => 'comment', 'option' => 'Comment'),
			3 => array('value' => 'image', 'option' => 'Image')
		);

		$valueFormNew["optionTypeData"] = $this->basic->getOption($typeDate, 'value', 'option'); //Monitores
		$valueFormNew["formID"] = 'formNewItemMonitor';
		$valueFormNew["cycles"] = '3';
		$value["formNewItemMonitor"] = $this->plantilla->getOne("configuracion/profiles/configItemMonitorForm", $valueFormNew);

		$valueFormNew["formID"] = 'formCloneProfile';
		$value["modalProfileClone"] = $this->plantilla->getOne("configuracion/profiles/configProfileCloneForm", $valueFormNew);

		$valueFormNew["optionTypeData"] = $this->basic->getOptionValue('type_data', 'string');
		$valueFormNew["formID"] = 'formProfileParametersNew';
		$value["modalParametersProfileNew"] = $this->plantilla->getOne("configuracion/profiles/configProfileParameterForm", $valueFormNew);

		$valueFormNew["formID"] = 'formProfileExport';
		$value["modalProfileExport"] = $this->plantilla->getOne("configuracion/profiles/exportForm", $valueFormNew);
		$valueFormNew["formID"] = 'formProfileImport';
		$value["modalProfileImport"] = $this->plantilla->getOne("configuracion/profiles/importForm", $valueFormNew);

		$value["checkedReport0"] = 'checked="checked"';

		//Tabla

		$button[] = array(
			'name' => 'NEW',
			'bclass' => 'add',
			'protec' => 'CONFIG_PROFILES_NEW',
			'onpress' => 'toolboxProfiles'
		);

		$value["button"] = $this->generate->getButton($button);

		$this->plantilla->set($value);
		$this->plantilla->finalize();
	}

	/**
	 * Table Profile Value
	 *
	 * @return json
	 */

	public function getTable() {
		$getParam = (object)$_POST;

		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_profile');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		$groupid = $this->bmonitor->getAllGroupsHost(false, 'sql');

		//Total rows

		$getTotalRows_sql = "SELECT COUNT( DISTINCT P.`id_profile`) As count  
			                         FROM `bm_profiles` P  
			                             LEFT OUTER JOIN `bm_profiles_permit` PP ON P.`id_profile` = PP.`id_profile`
			                         WHERE  PP.`groupid` IN $groupid GROUP BY P.`id_profile`
                               ";

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);

		if ($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['count'];
		else {
			$data['total'] = 0;
		}

		//Rows

		$getRows_sql = "SELECT P.`id_profile`, P.`profile`,GROUP_CONCAT(HG.`name`)  As groupName
    FROM `bm_profiles` P
        LEFT JOIN `bm_profiles_permit` PP  USING(`id_profile`)
        LEFT JOIN `bm_host_groups` HG ON PP.`groupid`=HG.`groupid` WHERE HG.`name` IS NOT NULL AND  PP.`groupid` IN $groupid  GROUP BY P.`id_profile` $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);

		$CONFIG_PROFILES_STRUCTURE = $this->protect->allowed('CONFIG_PROFILES_STRUCTURE');
		$CONFIG_PROFILES_PROBE = $this->protect->allowed('CONFIG_PROFILES_PROBE');
		$CONFIG_PROFILES_CLONE = $this->protect->allowed('CONFIG_PROFILES_CLONE');
		$CONFIG_PROFILES_SEQUENCE = $this->protect->allowed('CONFIG_PROFILES_SEQUENCE');
		$CONFIG_PROFILES_PARAM = $this->protect->allowed('CONFIG_PROFILES_PARAM');
		$CONFIG_PROFILES_SCHEDULE = $this->protect->allowed('CONFIG_PROFILES_SCHEDULE');
		$CONFIG_PROFILES_DELETE = $this->protect->allowed('CONFIG_PROFILES_DELETE');

		$CONFIG_PROFILES_EXPORT = $this->protect->allowed('CONFIG_PROFILES_EXPORT');
		$CONFIG_PROFILES_IMPORT = $this->protect->allowed('CONFIG_PROFILES_IMPORT');

		if ($getRows_result) {
			foreach ($getRows_result as $key => $row) {

				if ($CONFIG_PROFILES_STRUCTURE || $CONFIG_PROFILES_PARAM) {
					$option = '<span id="toolbarSet" class="ui-widget-header ui-corner-all">';
				}

				if ($CONFIG_PROFILES_STRUCTURE) {
					$option .= '<button id="structureProfile" onclick="$.structureProfile(' . $row['id_profile'] . ')" name="structureProfile">' . $this->language->STRUCTURE . '</button>';
				}

				if ($CONFIG_PROFILES_PARAM) {
					$option .= '<button id="paramProfile" onclick="$.paramProfile(' . $row['id_profile'] . ',\'' . $this->language->PARAMETROS . '\')" name="paramProfile">' . $this->language->PARAMETROS . '</button>';
				}

				if ($CONFIG_PROFILES_STRUCTURE || $CONFIG_PROFILES_PARAM) {
					$option .= '</span>';
				}

				if ($CONFIG_PROFILES_EXPORT || $CONFIG_PROFILES_IMPORT) {
					$option .= '<span id="toolbarSet" class="ui-widget-header ui-corner-all">';
				}

				if ($CONFIG_PROFILES_EXPORT) {
					$option .= '<button id="exportProfile" onclick="$.exportProfile(' . $row['id_profile'] . ')" name="exportProfile">' . $this->language->EXPORT . '</button>';
				}

				if ($CONFIG_PROFILES_IMPORT) {
					$option .= '<button id="importProfile" onclick="$.importProfile(' . $row['id_profile'] . ')" name="importProfile">' . $this->language->IMPORT . '</button>';
				}

				if ($CONFIG_PROFILES_EXPORT || $CONFIG_PROFILES_IMPORT) {
					$option .= '</span>';
				}

				$option .= '<span id="toolbarSet" class="ui-widget-header ui-corner-all">';

				if ($CONFIG_PROFILES_DELETE) {
					$option .= '<button id="deleteProfile" onclick="$.deleteProfile(' . $row['id_profile'] . ')" name="deleteProfile">' . $this->language->DELETE . '</button>';
				}

				if ($CONFIG_PROFILES_PROBE) {
					$option .= '<button id="categoriesProfile" onclick="$.categoriesProfile(' . $row['id_profile'] . ')" name="categoriesProfile">' . $this->language->PROBE . '</button>';
				}

				if ($CONFIG_PROFILES_CLONE) {
					$option .= '<button id="cloneProfile" onclick="$.cloneProfile(' . $row['id_profile'] . ',\'' . $row['profile'] . '_clone\')" name="cloneProfile">' . $this->language->CLONE . '</button>';
				}

				if ($CONFIG_PROFILES_SEQUENCE) {
					$option .= '<button id="sequenceProfile" onclick="$.sequenceProfile(' . $row['id_profile'] . ')" name="sequenceProfile">' . $this->language->SEQUENCE . '</button>';
				}

				if ($CONFIG_PROFILES_SCHEDULE) {
					$option .= '<button id="scheduleProfile" onclick="$.scheduleProfile(' . $row['id_profile'] . ')" name="scheduleProfile">' . $this->language->SCHEDULE . '</button>';
				}

				$option .= '</span>';

				$data['rows'][] = array(
					'id' => $row['id_profile'],
					'cell' => array(
						$row['profile'],
						$row['groupName'],
						$option
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function createProfile() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		$valida = $this->protect->access_page('CONFIG_PROFILES_EDIT');

		if ($valida == false) {
			$result['error'] = '(profile) Access is denied';
			echo $this->basic->jsonEncode($result);
			exit ;
		}

		if (isset($getParam->name) && isset($getParam->groupid) && ($getParam->name != '')) {

			if (is_array($getParam->groupid)) {


				$name = $this->conexion->quote($getParam->name);

				$insertProfileSQL = "INSERT INTO `bm_profiles` (`profile`)
                                        VALUE ($name)";

				$insertProfileRESULT = $this->conexion->query($insertProfileSQL);

				if ($insertProfileRESULT) {

					$lastInsertId = $this->conexion->lastInsertId();

					$insertProfilePermSQL = "INSERT INTO `bm_profiles_permit` (`id_profile`, `groupid`) VALUES ";

					foreach ($getParam->groupid as $key => $groupid) {

						if (is_numeric($groupid)) {
							$insertProfilePermValue[] = "($lastInsertId,$groupid)";
						}

					}

					$insertProfilePermRESULT = $this->conexion->query($insertProfilePermSQL . join(',', $insertProfilePermValue));

					if ($insertProfilePermRESULT) {
						$insertProfileCategoriesSQL = "INSERT INTO `bm_profiles_categories` (`id_profile`,`category`,`class`,`global`,`sequenceCode`)
                        VALUE ($lastInsertId,$name,'GLOBAL','true',0)";

						$insertProfileCategoriesRESULT = $this->conexion->query($insertProfileCategoriesSQL);

						if ($insertProfileCategoriesRESULT) {
							$result['status'] = true;
						} else {
							$result['error'] = '(ProfileCategories) Insert failed';
						}

					} else {
						$result['error'] = '(permProfile) Insert failed';
					}

				} else {
					$result['error'] = '(profile) Insert failed';
				}

			} else {
				$result['error'] = '(paramProfile) group option';
			}

		}

		$this->basic->jsonEncode($result);
	}

	public function deleteProfile() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->id) && ($getParam->id != '')) {

			if (is_array($getParam->id)) {

				$deleteProfileSQL = "DELETE FROM `bm_profiles` WHERE `id_profile` IN ";

				$deleteProfileValueSQL = $this->conexion->arrayIN($getParam->id, false, true);

				$deleteProfileRESULT = $this->conexion->query($deleteProfileSQL . $deleteProfileValueSQL);

				if ($deleteProfileRESULT) {
					$result['status'] = true;
				} else {
					$result['error'] = '(permProfile) Insert failed';
				}

			} else {
				$result['error'] = '(paramProfile) id param is not array';
			}

		} else {
			$result['error'] = '(paramProfile) id param';
		}

		$this->basic->jsonEncode($result);
	}

	public function getTableStructure($idprofile) {
		$getParam = (object)$_POST;

		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_categories');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		//Total rows

		$getTotalRows_sql = 'SELECT COUNT(`id_categories`) As count 
                                FROM `bm_profiles_categories` 
                                WHERE `global` = "false" AND `id_profile` = ' . $idprofile;

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);

		if ($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['count'];
		else {
			$data['total'] = 0;
		}

		//Rows

		$getRows_sql = "SELECT `id_categories`, `category`, `class`
                                FROM `bm_profiles_categories` 
                                WHERE `global` = 'false' AND `id_profile` = $idprofile $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);

		if ($getRows_result) {
			foreach ($getRows_result as $key => $row) {

				$option = '<span id="toolbar" class="ui-widget-header ui-corner-all">';
				$option .= '<span id="toolbarSet">
                                <button id="paramCategory" onclick="$.getStructureItem(' . $row['id_categories'] . ',\'param\')" name="paramCategory">' . $this->language->PARAMETERS . '</button>
                                <button id="monitorCategory" onclick="$.getStructureItem(' . $row['id_categories'] . ',\'result\')" name="monitorCategory">' . $this->language->MONITORS . '</button>
                            </span>';
				$option .= '</span>';

				$data['rows'][] = array(
					'id' => $row['id_categories'],
					'cell' => array(
						$row['category'],
						$row['class'],
						$option
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function createStructure($profileid) {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->name) && isset($getParam->class) && ($getParam->name != '')) {

			if (is_numeric($profileid)) {

				$name = $this->conexion->quote($getParam->name);
				$class = $this->conexion->quote($getParam->class);
				$codeSequence = $this->conexion->quote($getParam->codeSequence);

				$insertSQL = "INSERT INTO `bm_profiles_categories` ( `id_profile`, `category`, `class`,`sequenceCode`)
                VALUES
                    ( $profileid, $name, $class,$codeSequence);
                ";

				$insertRESULT = $this->conexion->query($insertSQL);

				if ($insertRESULT) {

					$lastInsertId = $this->conexion->lastInsertId();

					$result['status'] = true;

				} else {
					$result['error'] = '(category) Insert failed';
				}

			} else {
				$result['error'] = '(category) error param';
			}

		}

		$this->basic->jsonEncode($result);
	}

	public function deleteStructure() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->id) && ($getParam->id != '')) {

			if (is_array($getParam->id)) {

				$deleteProfileSQL = "DELETE FROM `bm_profiles_categories` WHERE `id_categories` IN ";

				$deleteProfileValueSQL = $this->conexion->arrayIN($getParam->id, false, true);

				$deleteProfileRESULT = $this->conexion->query($deleteProfileSQL . $deleteProfileValueSQL);

				if ($deleteProfileRESULT) {
					$result['status'] = true;
				} else {
					$result['error'] = '(category) Insert failed';
				}

			} else {
				$result['error'] = '(category) id param is not array';
			}

		} else {
			$result['error'] = '(category) invalid param';
		}

		$this->basic->jsonEncode($result);
	}

	public function getTableItem($idcategory, $type) {
		$getParam = (object)$_POST;

		$type = trim($type);

		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_item');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		//Total rows

		$getTotalRows_sql = 'SELECT COUNT(`id_item`) As count 
                                FROM `bm_profiles_item` 
                                WHERE `id_categories` = ' . $idcategory . ' AND `type` = "' . $type . '"';

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);

		if ($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['count'];
		else {
			$data['total'] = 0;
		}

		//Rows

		$getRows_sql = "SELECT `id_item`, `item`, `item_display`,`type_result`
                                FROM `bm_profiles_item` 
                                WHERE `id_categories` = $idcategory AND `type` = '$type' $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);

		if ($getRows_result) {
			foreach ($getRows_result as $key => $row) {

				$option = '<button id="editItem" onclick="$.editStructureItem(' . $row['id_item'] . ',\'' . $type . '\')" name="editItem">' . $this->language->EDIT . '</button>';

				$data['rows'][] = array(
					'id' => $row['id_item'],
					'cell' => array(
						$row['item'],
						$row['item_display'],
						$row['type_result'],
						$option
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function createStructureItem($idcategory, $iditem = false) {
		$param = (object)$_POST;	
		
		$result['status'] = false;

		if (isset($idcategory) && is_numeric($idcategory)) {

			if (!isset($param->description)) {
				$param->description = 'description';
			}

			if (!isset($param->name) && ($param->name == '') && !isset($param->display) && ($param->display == '')) {
				$result['error'] = 'Error invalid param';
			} else {

				$insertItenm = "INSERT INTO `bm_profiles_item` (`id_categories`, `item`, `item_display`, `description`, `type`, `type_result`, `default`, `report`, `unit`, `saveIP`, `thold`, `cycles`, `maxTh`, `minTh`) VALUES ";
				
				if (isset($param->type) && ($param->type == 'param')) {

					$insertValue = "($idcategory, '$param->name', '$param->display', '$param->description', 'param', '$param->typeData', '$param->default', 'false', NULL,'false','false',3,0,0)";
					
				} elseif (isset($param->type) && ($param->type == 'result')) {

					if (isset($param->report) && $param->report == 1) {
						$report = 'true';
					} else {
						$report = 'false';
					}

					if (isset($param->saveip) && $param->saveip == 1) {
						$saveip = 'true';
					} else {
						$saveip = 'false';
					}

					if (isset($param->thold) && $param->thold == 1) {
						$thold = 'true';
					} else {
						$thold = 'false';
					}
					
					if (isset($param->maxThold) && $param->maxThold >= 0) {
						$maxThold = $param->maxThold;
					} else {
						$maxThold = 0;
					}
					
					if (isset($param->minThold) && $param->minThold >= 0) {
						$minThold = $param->minThold;
					} else {
						$minThold = 0;
					}						

					$insertValue = "($idcategory, '$param->name', '$param->display', '$param->description', 'result', '$param->typeData', '$param->default', '$report', '$param->unit','$saveip','$thold','$param->cycles','$maxThold','$minThold')";
				}

				$insertResult = $this->conexion->query($insertItenm . $insertValue);
				
				if ($insertResult) {
					$result['status'] = true;
				} else {
					$result['error'] = 'Insert failed';
				}

			}

		} else {

			if (($idcategory == 'param' || $idcategory == 'result') && is_numeric($iditem)) {

				if (isset($param->report) && $param->report == 1) {
					$report = 'true';
				} else {
					$report = 'false';
				}

				if (!isset($param->unit)) {
					$param->unit = 'NULL';
				}

				if (isset($param->saveip) && $param->saveip == 1) {
					$saveip = 'true';
				} else {
					$saveip = 'false';
				}

				if (isset($param->thold) && $param->thold == 1) {
					$thold = 'true';
				} else {
					$thold = 'false';
				}

				if (isset($param->cycles) && $param->cycles >= 0) {
					$cycles = $param->cycles;
				} else {
					$cycles = 0;
				}

				if (isset($param->maxThold) && $param->maxThold >= 0) {
					$maxThold = $param->maxThold;
				} else {
					$maxThold = 0;
				}
				
				if (isset($param->minThold) && $param->minThold >= 0) {
					$minThold = $param->minThold;
				} else {
					$minThold = 0;
				}				
				
				$updateProfileItemSQL = "UPDATE `bm_profiles_item` 
                        SET 
                            `item` = '" . $param->name . "', 
                            `item_display` = '" . $param->display . "', 
                            `description` = '" . $param->description . "',
                            `type_result` = '" . $param->typeData . "', 
                            `default` = '" . $param->default . "', 
                            `report` = '" . $report . "', 
                            `unit` = '" . $param->unit . "',
                            `saveIP` = '" . $saveip . "',
                            `thold` = '" . $thold . "',
                            `cycles` = '" . $cycles . "',
                            `maxTh` = '" . $maxThold . "',
                            `minTh` = '" . $minThold . "'
                        WHERE `id_item` = '$iditem'";

				$updateProfileItemRESULT = $this->conexion->query($updateProfileItemSQL);

				if ($updateProfileItemRESULT) {
					$result['status'] = true;
				} else {
					$result['error'] = 'update failed';
				}

			} else {
				$result['error'] = 'Error id category not found';
			}
		}
		$this->fixNameMonitor();
		
//INICIO CAMBIO ID_BUGS=BGS003
		//Aqui voy a editar una prueba de cada categoria de monitor para actializar y hacer que se actualicen y creen los 
		//item de los nuevos monitores (estructura) que en forma actual no se asignaban hasta crear una nueva prueba.
		// los cambios y analisis de este codigo esta en docDevel/BUGS_CONOCIDOS con el ID_BUGS=BGS003
		
		//obtengo cada categoria existente de la tabla bm_profiles_category que concuerda con mi id_categories
		//Rows
		$this->logs->error(" -- > ID_categories(403) es: ".$idcategory);  //es 403 OK PING		

		$getRows_sqlRGR = 'SELECT `id_category` 
		FROM `bm_profiles_category` WHERE `id_categories` = '.$idcategory; //este $idcategory realmente es el $id_categories en BD y representa pj:id_categories=403 PING en id_prodile=66 (neutralidad)

		$getRows_resultRGR = $this->conexion->queryFetch($getRows_sqlRGR);
		//saco los valores de la consulta en dos array	$valIdem  y $arrayItems		
		 //aqui creo el arreglo tipo $_POST para engañar a la funcion y segui el normal conducto del codigo de carlitos.		
		if ($getRows_resultRGR) {
			foreach ($getRows_resultRGR as $key => $row) {				
				$this->logs->error(" -- > para cada category= : ".$row['id_category']);
				//Get Categories, esta query es idem a la q esta en la función getEditFormCategoryValue()
				//y extrae la info necesaria para editar todos los items de la categoria (estructura->Clase PING-> nuevo item de clase (ej: ping-> lost , OK, avg[nuevo]) 
				//que se esta ingresando en todas las tablas de la BD donde esta el item).					
				$getCategorySQLRGR = 'SELECT PC.`category`, PC.`count`,PC.`display`, PI.`id_item`,PI.`type`, PV.`id_monitor`, PV.`value`
		                        FROM `bm_profiles_category`  PC
		                        LEFT JOIN `bm_profiles_categories` PCC ON PC.`id_categories`=PCC.`id_categories`
		                        LEFT JOIN `bm_profiles_item` PI ON PC.`id_categories`=PI.`id_categories`
		                        LEFT JOIN `bm_profiles_values` PV ON (PI.`id_item`=PV.`id_item` AND  PV.`id_category`=PC.`id_category`)
		                        WHERE  PC.`id_category` = '.$row['id_category'];		
				$getCategoryRESULTRGR = $this->conexion->queryFetch($getCategorySQLRGR);		
				//$valIdem=array(); 
				$arrayItems=array();				
				foreach ($getCategoryRESULTRGR as $key => $fila){
				    $arrayItems['category']=$fila['category'];
					$arrayItems['count']=$fila['count'];
					$arrayItems['display']=$fila['display'];

					if($fila['type']=="param"){
						$arrayItems["param_".$fila['id_item']]=$fila['value'];	
					}else{
						$arrayItems["item_".$fila['id_item']]=$fila['id_monitor'];
					}									
				}

				$numero3 = count($arrayItems);
				$this->logs->error("count POST: ".$numero3);
				foreach ($arrayItems as $key => $value2) {
					//$this->logs->error("este es el POST virtual final de id_category".$row['id_category']." :  $key = ".$value2);
					if($value2==""||$value2==null){
						$arrayItems["$key"]=0;
					}
				}				
				$this->ArregloItemsNuevosMonitores($row['id_category'],$arrayItems);  //aqui envio a edit pero con el archivo POST creado	

			}
		}
	//aqui creo el arreglo tipo $_POST para engañar a la funcion y segui el normal conducto del codigo de carlitos.
		//$nuevoArray_POST= array_merge($valIdem,$arrayItems)
				
		//función Editar idem a getEditFormCategoryValue()							
		//$this->ArregloItemsNuevosMonitores($row['id_category'],$valIdem);  //aqui envio a edit pero con el archivo POST creado
		
//FIN CAMBIO	
				
		$this->basic->jsonEncode($result);
	}

	
	// esta función es identica a editCategoryValue($idCategory); la realizamos ya que cada vez que ingresamos un nuevo monitor no se actualizan 
	// y sincronizan los datos en las tablas relacionadas de la Base de Datos.
	// 	 
	public function ArregloItemsNuevosMonitores($idCategory, $valuePOST) 
	{
		$this->logs->error("este es el id_category: ".$idCategory);	
		$this->logs->error(" ESTE es el Arreglo que viene como POST --- nueva FUNCIÓN");
		foreach ($valuePOST as $key => $value) {
				$this->logs->error(" $key : ".$value);	
		}
		$param=(object)$valuePOST;
		$this->logs->error(" ESTE es el Arreglo que  object: ".$param->display);
		$result['status'] = false;
		
		
		if (isset($idCategory) && is_numeric($idCategory)) {

			if (!isset($param->display) && ($param->display == '')) {
				$result['error'] = 'Error invalid param';
			} else {
				//if (!isset($param->default) || ($param->default == '')) {
					$param->default = 'NONE';
				//}
				$this->conexion->InicioTransaccion();

				$display = $this->conexion->quote($param->display);

				$updateCategorySQL = "UPDATE `bm_profiles_category` SET `display` = $display WHERE `id_category` = '$idCategory'";

				$updateCategoryRESULT = $this->conexion->query($updateCategorySQL);

				if ($updateCategoryRESULT) {
					$this->logs->error(" HAY COSAS A UPDATIAR.. ");
					$itemParam = array();
					$itemResult = array();

					foreach ($param as $key => $value) {
						if (preg_match("/^param_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemParam[$keyName[1]] = $value;
						} elseif (preg_match("/^item_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemResult[$keyName[1]] = $value;
						} elseif (preg_match("/^script_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$ValueScript = explode("\r", $value);
							foreach ($ValueScript as $key => $value) {
								$ValueScriptArray[] = trim($value);
							}
							$itemParam[$keyName[1]] = join(';', $ValueScriptArray);
						}
					}

					$this->logs->error("estas aqui estan los parametros de array itemParam: ");
					foreach ($itemParam as $key => $value) {
							$this->logs->error(" $key : ".$value);	
					}
					$this->logs->error("estas aqui estan los parametros de array itemResult: ");
					foreach ($itemResult as $key => $value) {
							$this->logs->error(" $key : ".$value);	
					}
					//Insertar Monitores

					if (isset($itemResult)) {
						$this->logs->error("... Hay itemes a INSERTAR... ");
						$getItemSQL = 'SELECT `id_item`, PI.`item`, PI.`item_display` ,  `type_result`, `default`, `unit`, PG.`creationMonitor`, PG.`creationMonitorDisplay`,  PG.`class`, PG.`category`, PG.`id_profile`
                                              FROM `bm_profiles_item` PI 
                                            LEFT JOIN `bm_profiles_categories` PG ON PI.`id_categories`=PG.`id_categories` 
                                            WHERE `id_item` IN (' . join(',', array_keys($itemResult)) . ');';

						$getItemRESULT = $this->conexion->queryFetch($getItemSQL);
						$this->logs->error("Hay count en select= filas".count($getItemRESULT));
						
						$this->logs->error("insertar 1er Select getItem: 1era fila:  ");
						foreach ($getItemRESULT[0] as $key => $value) {
								$this->logs->error(" $key : ".$value);	
						}

						
						$profileID = $getItemRESULT[0]['id_profile'];
						
						$this->logs->error("__ profileID: ".$profileID);	
						$this->logs->error("__ profileID: OKKKK....");
						$insertMonitor = 'INSERT INTO `bm_items` 
                            (`name`, `description`, `descriptionLong`, `type_item`, 
                                `delay`, `history`, `trend`, `type_poller`, `unit`, `display`) VALUES ';
						$i4=0;
						foreach ($getItemRESULT as $key => $value) {							
							$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);														
							$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorName);
							//$this->logs->error("moni1: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
							//$this->logs->error("moni2: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
							//$this->logs->error("moni3: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_CODE}", $param->category, $monitorName);
							//$this->logs->error("moni4: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorName);
							//$this->logs->error("moni5: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorName);
							//$this->logs->error("moni6: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{COUNT}", $param->count, $monitorName);
							$this->logs->error("moni7: ID_PROFILE".$profileID."  ->".$monitorName);

							$monitorDisplay = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitorDisplay']);
							//$this->logs->error("moni1: DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorDisplay);
							//$this->logs->error("moni2:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_NAME}", $value['item'], $monitorDisplay);
							//$this->logs->error("moni3:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorDisplay);
							//$this->logs->error("moni4:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_CODE}", $param->category, $monitorDisplay);
							//$this->logs->error("moni5:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorDisplay);
							//$this->logs->error("moni6:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorDisplay);
							//$this->logs->error("moni7:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);
							$monitorDisplay = str_replace("{COUNT}", $param->count, $monitorDisplay);
							$this->logs->error("moni8:DISP ID_PROFILE".$profileID."  ->".$monitorDisplay);

							if ($itemResult[$value['id_item']] == 0) {
								$this->logs->error("entro con ".$i4);
								$insertMonitorValue = "('$monitorName','$monitorName', '$monitorDisplay', '" . $value['type_result'] . "', 600, 90, 365, 'bsw_agent', '" . $value['unit'] . "', 'none');";

								$insertMonitorRESULT = $this->conexion->query($insertMonitor . $insertMonitorValue);

								if (!$insertMonitorRESULT) {
									$this->conexion->rollBack();
									$result['error'] = 'Error insert monitor';
									echo $this->basic->jsonEncode($result);
									//$this->logs->error("ERRRORRRRRR de insert monitor");
									//exit ;
								} else {
									$itemValid[$value['id_item']] = array(
										'idMonitor' => $this->conexion->lastInsertId(),
										'nameMonitor' => $monitorName,
										'nameDisplayMonitor' => $monitorDisplay
									);
									$this->logs->error("insert monitor id_item=0  ".$value['id_item']);
									foreach ($itemValid as $key5 => $value5) {
										$this->logs->error(" $key5 : ".$value5);	
									}
								}

							}
						} 
					} else {
						//Get Profile ID Not Monitor
						$getProfileIDSQL = 'SELECT `id_profile` FROM `bm_profiles_categories` WHERE `id_categories` = ' . $idcategories . ' LIMIT 1';
						$getProfileIDRESULT = $this->conexion->queryFetch($getProfileIDSQL);

						if ($getProfileIDRESULT) {
							$profileID = $getProfileIDRESULT[0]['id_profile'];
						} else {
							$this->conexion->rollBack();
							$result['error'] = 'Error get profile id not monitor';
							echo $this->basic->jsonEncode($result);
							exit ;
						}
					}
					
					$insertItemSQL = 'INSERT INTO `bm_profiles_values` (`id_item`, `id_category`, `id_monitor`, `monitor_name`, `value`) VALUES ';

					foreach ($itemParam as $key => $value) {
						$valueQuote = $this->conexion->quote($value);
						$insertItemVALUE[] = "( $key, $idCategory, 0, null, $valueQuote)";
					}
					
					foreach ($itemValid as $key6 => $value6) {
								$this->logs->error("Hay itemValid  $key6 : ".$value6);	
						}
					
					
					if (isset($itemValid)) {
						foreach ($itemValid as $key => $value) {
							$valueQuote = $this->conexion->quote($value['nameMonitor']);
							$insertItemVALUE[] = "( $key, $idCategory, " . $value['idMonitor'] . ", $valueQuote , null )";
						}
					}

					$insertItemRESULT = $this->conexion->query($insertItemSQL . join(',', $insertItemVALUE) . " ON DUPLICATE KEY UPDATE 
                                        `id_monitor`=VALUES(`id_monitor`),  
                                        `monitor_name`=VALUES(`monitor_name`),
                                        `value`=VALUES(`value`)");
					
					if ($insertItemRESULT) {
						$this->logs->error("Hay Inserts de Itemes");
						if (isset($itemValid)) {
							$this->logs->error("Valida entrOOOOO....");
							$getGroupIdProfileSQL = 'SELECT PP.`groupid` FROM `bm_profiles_permit` PP WHERE `id_profile` = ' . $profileID;

							$getGroupIdProfileRESULT = $this->conexion->queryFetch($getGroupIdProfileSQL);

							if ($getGroupIdProfileRESULT) {
								$this->logs->error("INSERTO GRUPO....");
								$isertItemsGroupsSQL = 'INSERT IGNORE INTO `bm_items_groups` 
                                                                (`id_item`, `groupid`, `status`) VALUES ';

								foreach ($getGroupIdProfileRESULT as $key => $value) {

									$groupid = $value['groupid'];

									foreach ($itemValid as $keyMonitor => $monitor) {
										$isertItemsGroupsValueSQL[] = "(" . $monitor['idMonitor'] . ",$groupid, 1)";
									}

								}

								$isertItemsGroupsRESULT = $this->conexion->query($isertItemsGroupsSQL . join(',', $isertItemsGroupsValueSQL));
								$this->logs->error("INSERTO ITEMES DE GRUPO....");
								
								if ($isertItemsGroupsRESULT) {
									$this->logs->error("TODO OKKKK....");
									$this->conexion->commit();
									$this->fixNameMonitor();
									$result['status'] = true;
									//echo $this->basic->jsonEncode($result);
									//exit ;
								} else {
									$result['error'] = 'Error insert permit group monitor';
									$this->conexion->rollBack();
									$this->logs->error("Eroorrrr  insert GRUPO....");
									//echo $this->basic->jsonEncode($result);
									//exit ;
								}
							}

						} else {
							$this->conexion->commit();
							$this->fixNameMonitor();
							$result['status'] = true;
							$this->logs->error("ERROR NO HAY VALIDOS ItemResult....");
							//echo $this->basic->jsonEncode($result);
							//exit ;
						}

					} else {
						$result['error'] = 'Error insert item profile value';
						$this->conexion->rollBack();
						//echo $this->basic->jsonEncode($result);
						//exit ;
					}	
				}
//				$this->conexion->commit();
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	public function getFormStructureItem() {
		$valida = $this->protect->access_page('CONFIG_PROFILES');

		$getParam = (object)$_POST;

		if ($getParam->TYPE == 'param') {
			$template = 'configItemParamForm';
		} else {
			$template = 'configItemMonitorForm';
		}

		$this->plantilla->loadSec("configuracion/profiles/" . $template, $valida);

		$getProfileSQL = 'SELECT * FROM `bm_profiles_item` WHERE `id_item` = ' . $getParam->ID;

		$getProfileRESULT = $this->conexion->queryFetch($getProfileSQL);

		if ($getProfileRESULT) {

			$profileValue = (object)$getProfileRESULT[0];

			$valueFormNew["name"] = $profileValue->item;
			$valueFormNew["display"] = $profileValue->item_display;
			$valueFormNew["description"] = $profileValue->description;
			$valueFormNew["default"] = $profileValue->default;
			$valueFormNew["unit"] = $profileValue->unit;

			if ($profileValue->report == 'true') {
				$valueFormNew["checkedReport"] = 'checked';
			}

			if ($profileValue->saveIP == 'true') {
				$valueFormNew["saveip"] = 'checked';
			}

			if ($profileValue->thold == 'true') {
				$valueFormNew["checkedThold"] = 'checked';
			}
			
			$valueFormNew["cycles"] = $profileValue->cycles;
			$valueFormNew["maxThold"] = $profileValue->maxTh;
			$valueFormNew["minThold"] = $profileValue->minTh;
			
		} else {
			echo "Internal Error";
			exit ;
		}

		$valueFormNew["groupidList"] = $this->bmonitor->getAllGroupsHost(false, 'option', '-first-');
		
		$typeDate = array(
			0 => array('value' => 'float', 'option' => 'Float'),
			1 => array('value' => 'string', 'option' => 'String'),
			2 => array('value' => 'comment', 'option' => 'Comment'),
			3 => array('value' => 'image', 'option' => 'Image')
		);

		if ($getParam->TYPE == 'param') {
			$valueFormNew["optionTypeData"] = $this->basic->getOptionValue('type_data', $profileValue->type_result);
		} else {
			$valueFormNew["optionTypeData"] = $this->basic->getOption($typeDate, 'value', 'option', $profileValue->type_result); //Monitores
		}
		
		$valueFormNew["formID"] = 'formEditItem';

		$this->plantilla->set($valueFormNew);
		echo $this->plantilla->get();
	}

	public function deleteStructureItem() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->id) && ($getParam->id != '')) {

			if (is_array($getParam->id)) {

				$deleteSQL = "DELETE FROM `bm_profiles_item` WHERE `id_item` IN ";

				$deleteValueSQL = $this->conexion->arrayIN($getParam->id, false, true);

				$deleteRESULT = $this->conexion->query($deleteSQL . $deleteValueSQL);

				if ($deleteRESULT) {
					$result['status'] = true;
				} else {
					$result['error'] = 'Insert failed';
				}

			} else {
				$result['error'] = 'id param is not array';
			}

		} else {
			$result['error'] = 'Invalid param';
		}

		echo $this->basic->jsonEncode($result);
	}

	public function getTableCategoriesValue($idprofile) {
		$getParam = (object)$_POST;

		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_categories');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		//Total rows

		$getTotalRows_sql = 'SELECT COUNT(`id_categories`) As count 
                                FROM `bm_profiles_categories` 
                                WHERE `global` = "false" AND `id_profile` = ' . $idprofile;

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);

		if ($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['count'];
		else {
			$data['total'] = 0;
		}

		//Rows

		$getRows_sql = "SELECT `id_categories`, `category`, `class`
                                FROM `bm_profiles_categories` 
                                WHERE `global` = 'false' AND `id_profile` = $idprofile $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);

		if ($getRows_result) {
			foreach ($getRows_result as $key => $row) {

				//$highlight = '<div id="highlight" style="border-radius: 12px" class="ui-button ui-widget ui-state-default"><span
				// class="ui-button-icon-primary ui-icon ui-icon-star"></span></div>';
				$option = '<button id="categoryValue" onclick="$.getCategoryValue(' . $row['id_categories'] . ')" name="categoryValue">' . $this->language->INTRODUCE . '</button>';

				$data['rows'][] = array(
					'id' => $row['id_categories'],
					'cell' => array(
						'category' => $row['category'],
						'class' => $row['class'],
						'action' => $option
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function getTableCategoryValue($idcategories) {
		$getParam = (object)$_POST;
		
		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_category');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		//Total rows

		$getTotalRows_sql = 'SELECT COUNT(`id_category`) As count 
                                FROM `bm_profiles_category` 
                                WHERE `id_categories` = ' . $idcategories;

		$getTotalRows_result = $this->conexion->queryFetch($getTotalRows_sql);

		if ($getTotalRows_result)
			$data['total'] = $getTotalRows_result[0]['count'];
		else {
			$data['total'] = 0;
		}

		//Rows

		$getRows_sql = "SELECT `id_category`, `category`, `display` , `status`, `view`
                                FROM `bm_profiles_category` 
                                WHERE `id_categories` = $idcategories $var->sortSql $var->limitSql";

		$getRows_result = $this->conexion->queryFetch($getRows_sql);

		if ($getRows_result) {
			foreach ($getRows_result as $key => $row) {

				$option = '<button id="categoryValueEdit" onclick="$.editCategoryValue(' . $row['id_category'] . ')" name="categoryValueEdit">' . $this->language->EDIT . '</button>';

				if ($row['status'] == 'true') {
					$status = '<a onclick="$.CategoryValueStatus(' . $row['id_category'] . ',true)" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button on/off"> <span class="ui-button-icon-primary ui-icon ui-icon-power"></span><span class="ui-button-text">' . $this->language->ENABLED . '</span> </a>';
				} else {
					$status = '<a onclick="$.CategoryValueStatus(' . $row['id_category'] . ',false)" class="ui-button ui-widget ui-state-error ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button on/off"> <span class="ui-button-icon-primary ui-icon ui-icon-power"></span><span class="ui-button-text">' . $this->language->DISABLED . '</span> </a>';
				}

				if ($row['view'] == 'true') {
					$view = '<a onclick="$.CategoryValueView(' . $row['id_category'] . ',true)" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button view"> <span class="ui-button-icon-primary ui-icon ui-icon-search"></span><span class="ui-button-text">' . $this->language->ENABLED . '</span> </a>';
				} else {
					$view = '<a onclick="$.CategoryValueView(' . $row['id_category'] . ',false)" class="ui-button ui-widget ui-state-error ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button view"> <span class="ui-button-icon-primary ui-icon ui-icon-search"></span><span class="ui-button-text">' . $this->language->DISABLED . '</span> </a>';
				}

				$data['rows'][] = array(
					'id' => $row['id_category'],
					'cell' => array(
						'id_category' => $row['id_category'],
						'category' => $row['category'],
						'display' => $row['display'],
						'view' => $view,
						'status' => $status,
						'action' => $option
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function setTableCategoryValueStatus() {
		$valida = $this->protect->access_page('CONFIG_PROFILES');

		$data['status'] = true;

		if ($valida) {
			$getParam = (object)$_POST;

			if ((int)$getParam->status == 1) {
				$status = 'true';
			} else {
				$status = 'false';
			}

			$updateItemGroup = "UPDATE `bm_profiles_category` SET `status` = '$status' WHERE `id_category` = '$getParam->id';";

			$valida = $this->conexion->query($updateItemGroup);

			if (!$valida) {
				$data['status'] = false;
			}

		} else {
			$data['status'] = false;
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function setTableCategoryValueView() {
		$valida = $this->protect->access_page('CONFIG_PROFILES');

		$data['status'] = true;

		if ($valida) {
			$getParam = (object)$_POST;

			if ((int)$getParam->status == 1) {
				$status = 'true';
			} else {
				$status = 'false';
			}

			$updateItemGroup = "UPDATE `bm_profiles_category` SET `view` = '$status' WHERE `id_category` = '$getParam->id';";

			$valida = $this->conexion->query($updateItemGroup);

			if (!$valida) {
				$data['status'] = false;
			}

		} else {
			$data['status'] = false;
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function getFormCategoryValue() {
		$categoriesid = $_POST['id'];

		$valida = $this->protect->access_page('CONFIG_PROFILES_CATEGORY_VALUE');

		$this->plantilla->loadSec("configuracion/profiles/configCategoryValueForm", $valida, false);

		$varSite["formID"] = 'formNewCategoryValue';

		//Get Categories

		$getCategoriesSQL = 'SELECT PI.`id_categories`, PC.`class`, PC.`count`, PC.`creationCategory`, PC.`category`,
            PC.`creationMonitor`, PI.`id_item`, PI.`item`, PI.`item_display` , PI.`type`, PC.`id_profile`,PI.`default`,PI.`type_result`
    FROM `bm_profiles_categories` PC LEFT OUTER JOIN `bm_profiles_item` PI ON PC.`id_categories`=PI.`id_categories` 
WHERE 
    PC.`global` = "false" AND 
    PC.`id_categories` = ' . $categoriesid;

		$getCategoriesRESULT = $this->conexion->queryFetch($getCategoriesSQL);

		if ($getCategoriesRESULT) {

			$categories = (object)$getCategoriesRESULT[0];

			// Get Count Category

			$getCountCategorySQL = 'SELECT `count` FROM `bm_profiles_category` PC 
                                        WHERE PC.`id_categories` = ' . $categoriesid;

			$getCountCategoryRESULT = $this->conexion->queryFetch($getCountCategorySQL);

			if ($getCountCategoryRESULT) {
				$countActive = array();
				foreach ($getCountCategoryRESULT as $key => $value) {
					$countActive[$value['count']] = $value['count'];
				}

				$countAsig = count($getCountCategoryRESULT) + 1;

				for ($i = 1; $i <= count($getCountCategoryRESULT); $i++) {
					if (!isset($countActive[$i])) {
						$countAsig = $i;
					}
				}
			} else {
				$countAsig = 1;
			}

			$category = str_replace("{COUNT}", $countAsig, $categories->creationCategory);

			$category = str_replace("{CATEGORIES_CLASS}", $categories->class, $category);

			$form = '<div class="row">
                <label for="category" class="col1">' . $this->language->CATEGORY . '</label>
                <span class="col2 ">
                    <input readonly type="text" name="category" value="' . $category . '" id="category" class="input verylarge ui-widget-content ui-corner-all" />
                </span>
            </div>';

			$form .= '<input type="hidden" name="count" value="' . $countAsig . '" />';

			$form .= '<div class="row">
                <label for="display" class="col1">' . $this->language->DISPLAY . '</label>
                <span class="col2 ">
                    <input type="text" name="display" value="" id="display" class="input verylarge ui-widget-content ui-corner-all" />
                </span>
            </div>';

			/*$form .= '<div class="row">
			 <label for="default" class="col1">Default</label>
			 <span class="col2 ">
			 <input type="text" name="default" value="" id="default" class="input input-long ui-widget-content
			 ui-corner-all" />
			 </span>
			 </div>';*/

			foreach ($getCategoriesRESULT as $key => $value) {
				if ($value['type'] == 'param') {
					$formType = 'input';
					$classRow = 'row';
					if ($value['type_result'] == 'combobox') {
						$option = array();
						$comboboxOption = explode('|', $value['default']);
						foreach ($comboboxOption as $Ckey => $Cvalue) {
							list($comboboxId, $comboboxValue) = explode(',', $Cvalue);
							$option[] = "<option value='$comboboxId'>$comboboxValue</option>";
						}
						$formType = 'select';
					} elseif ($value['type_result'] == 'script') {
						$formType = 'script';
						$classRow = 'rowC';
					}

					$form .= '<div class="' . $classRow . '">
                        <label for="param_' . $value['id_item'] . '" class="col1">' . $value['item_display'] . '</label>
                        <span class="col2 ">';
					if ($formType == 'input') {
						$form .= '<input type="text" name="param_' . $value['id_item'] . '" value="' . $value['default'] . '" id="' . $value['id_item'] . '" class="input verylarge ui-widget-content ui-corner-all" />';
					} elseif ($formType == 'script') {
						$form .= '<textarea class="textarea verylarge ui-corner-all" name="script_' . $value['id_item'] . '" id="script_' . $value['id_item'] . '" cols="200" wrap="off" rows="15"></textarea>';
					} else {
						$form .= '<select name="param_' . $value['id_item'] . '" id="param_' . $value['id_item'] . '" class="select">
                                    ' . join("\n", $option) . '
                                </select>';
					}
					$form .= '</span></div>';
				}
			}

			$form .= '<div style="display: none" class="title"><span>Containers (INPUT)</span></div>';

			///Get Monitores

			$getMonitoresSQL = 'SELECT I.`id_item`, I.`descriptionLong`
                FROM `bm_items` I 
                    LEFT JOIN `bm_items_groups` IG ON I.`id_item`=IG.`id_item`
                    LEFT JOIN `bm_profiles_permit` PP ON PP.`groupid`= IG.`groupid`
                    LEFT JOIN `bm_profiles_values` PV ON PV.`id_monitor`= I.`id_item`
                WHERE PP.`id_profile` = ?  AND PV.`id_category` IN ( SELECT `id_category`  FROM `bm_profiles_category` WHERE `id_categories` = ?)
                GROUP BY I.`id_item` ORDER BY I.`descriptionLong`';

			$getMonitoresRESULT = $this->conexion->queryFetch($getMonitoresSQL, $categories->id_profile, $categoriesid);

			if ($getMonitoresRESULT) {
				$monitores = $this->basic->getOption($getMonitoresRESULT, 'id_item', 'descriptionLong', '-first-');
			} else {
				$monitores = '';
			}

			foreach ($getCategoriesRESULT as $key => $value) {
				if ($value['type'] == 'result') {

					$display = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);
					$display = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $display);
					$display = str_replace("{ITEM_NAME}", $value['item'], $display);
					$display = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $display);
					$display = str_replace("{COUNT}", $countAsig, $display);

					$form .= '<div style="display: none" class="row">
                        <label for="item_' . $value['id_item'] . '" class="col1">' . $display . '</label>
                        <span class="col2">
                        <select name="item_' . $value['id_item'] . '" id="item_' . $value['id_item'] . '" class="select">
                            <option value="0">Generar</option>' . $monitores . '
                        </select>
                        </span>
                        </div>';
				}
			}

			$varSite["form"] = $form;

		}

		$this->plantilla->set($varSite);

		echo $this->plantilla->get();
	}

	public function getEditFormCategoryValue() {
		$categoryid = $_POST['id'];

		$valida = $this->protect->access_page('CONFIG_PROFILES_CATEGORY_VALUE');

		$this->plantilla->loadSec("configuracion/profiles/configCategoryValueForm", $valida, false);

		$varSite["formID"] = 'formEditCategoryValue';

		//Get Categories

		$getCategorySQL = "SELECT  PCC.`id_profile`, PCC.`class`, PCC.`creationMonitor`, PC.`id_category`,PC.`id_categories`,PC.`category`,PC.`display`,PC.`count`, PI.`id_item`, PI.`item`, PI.`type`,  
			                     PI.`item_display`,  PV.`id_value`, PV.`id_monitor`, PV.`monitor_name`, PV.`value`,PI.`type_result`, PI.`default`
                        FROM `bm_profiles_category`  PC
                        LEFT JOIN `bm_profiles_categories` PCC ON PC.`id_categories`=PCC.`id_categories`
                        LEFT JOIN `bm_profiles_item` PI ON PC.`id_categories`=PI.`id_categories`
                        LEFT JOIN `bm_profiles_values` PV ON (PI.`id_item`=PV.`id_item` AND  PV.`id_category`=PC.`id_category`)
                        WHERE  PC.`id_category` = ? ";

		$getCategoryRESULT = $this->conexion->queryFetch($getCategorySQL, $categoryid);

		if ($getCategoryRESULT) {

			$category = $getCategoryRESULT[0];

			// Get Count Category

			$form = '<div class="row">
                <label for="category" class="col1">' . $this->language->CATEGORY . '</label>
                <span class="col2 ">
                    <input readonly type="text" name="category" value="' . $category['category'] . '" id="category" class="input verylarge ui-widget-content ui-corner-all" />
                </span>
            </div>';

			$form .= '<input type="hidden" name="count" value="' . $category['count'] . '" />';

			$form .= '<div class="row">
                <label for="display" class="col1">' . $this->language->DISPLAY . '</label>
                <span class="col2 ">
                    <input type="text" name="display" value="' . $category['display'] . '" id="display" class="input verylarge ui-widget-content ui-corner-all" />
                </span>
            </div>';

			foreach ($getCategoryRESULT as $key => $value) {
				if ($value['type'] == 'param') {
					$formType = 'input';
					$classRow = 'row';
					if ($value['type_result'] == 'combobox') {
						$option = array();
						$comboboxOption = explode('|', $value['default']);
						foreach ($comboboxOption as $Ckey => $Cvalue) {
							list($comboboxId, $comboboxValue) = explode(',', $Cvalue);
							if ($value['value'] == $comboboxId) {
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
							$option[] = "<option $selected value='$comboboxId'>$comboboxValue</option>";
						}
						$formType = 'select';
					} elseif ($value['type_result'] == 'script') {
						$formType = 'script';
						$classRow = 'rowC';
					}

					$form .= '<div class="' . $classRow . '">
                        <label for="param_' . $value['id_item'] . '" class="col1">' . $value['item_display'] . '</label>
                        <span class="col2 ">';
					if ($formType == 'input') {
						$form .= '<input type="text" name="param_' . $value['id_item'] . '" value="' . $value['value'] . '" id="' . $value['id_item'] . '" class="input verylarge ui-widget-content ui-corner-all" />';
					} elseif ($formType == 'script') {
						$valueScript = str_replace(";", "\r", $value['value']);
						$form .= '<textarea class="textarea verylarge ui-corner-all" name="script_' . $value['id_item'] . '" id="script_' . $value['id_item'] . '" cols="200" wrap="off" rows="15">' . $valueScript . '</textarea>';
					} else {
						$form .= '<select name="param_' . $value['id_item'] . '" id="param_' . $value['id_item'] . '" class="select">
                                    ' . join("\n", $option) . '
                                </select>';
					}
					$form .= '</span></div>';
				}
			}

			///Get Monitores START

			$getMonitoresSQL = 'SELECT I.`id_item`, I.`descriptionLong`
                FROM `bm_items` I 
                    LEFT JOIN `bm_items_groups` IG ON I.`id_item`=IG.`id_item`
                    LEFT JOIN `bm_profiles_permit` PP ON PP.`groupid`= IG.`groupid`
                    LEFT JOIN `bm_profiles_values` PV ON PV.`id_monitor`= I.`id_item`
                WHERE PP.`id_profile` = ?  AND PV.`id_category` IN ( SELECT `id_category`  FROM `bm_profiles_category` WHERE `id_categories` = ?)
                GROUP BY I.`id_item` ORDER BY I.`descriptionLong`';

			$getMonitoresRESULT = $this->conexion->queryFetch($getMonitoresSQL, $category['id_profile'], $category['id_categories']);

			if ($getMonitoresRESULT) {
				$monitores = $this->basic->getOption($getMonitoresRESULT, 'id_item', 'descriptionLong', '-first-');
			} else {
				$monitores = '';
			}

			//Get Monitores END

			$form .= '<div style="display: none" class="title"><span>Monitores</span></div>';

			///Get Monitores
			foreach ($getCategoryRESULT as $key => $value) {
				if ($value['type'] == 'result') {

					if (!is_null($value['id_value'])) {

						$form .= '<div style="display: none" class="row">
                        <label for="item_' . $value['id_item'] . '" class="col1">' . $value['monitor_name'] . ' : </label>
                        <span class="col2">
                        <input readonly type="text" name="item_' . $value['id_item'] . '" value="' . $value['id_monitor'] . '" id="item_' . $value['id_item'] . '" class="input input-long ui-widget-content ui-corner-all" />
                        </span>
                        </div>';

					} else {

						$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);
						$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorName);
						$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
						$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
						$monitorName = str_replace("{COUNT}", $value['count'], $monitorName);

						$form .= '<div style="display: none" class="row">
                            <label for="item_' . $value['id_item'] . '" class="col1">' . $monitorName . '</label>
                            <span class="col2">
                            <select name="item_' . $value['id_item'] . '" id="item_' . $value['id_item'] . '" class="select">
                                <option value="0">Generar</option>' . $monitores . '
                            </select>
                            </span>
                            </div>';

					}

				}
			}

			$varSite["form"] = $form;

		}

		$this->plantilla->set($varSite);

		echo $this->plantilla->get();
	}

	public function createCategoryValue($idcategories) {
		$param = (object)$_POST;

		$result['status'] = false;
		
		if (isset($idcategories) && is_numeric($idcategories)) {

			if (!isset($param->display) && ($param->display == '')) {
				$result['error'] = 'Error invalid param';
			} else {

				if (!isset($param->default) || ($param->default == '')) {
					$param->default = 'NONE';
				}

				$this->conexion->InicioTransaccion();

				$insertCategorySQL = "INSERT INTO `bm_profiles_category` (`id_categories`, `category`, `display`, `default`, `count`) VALUES ";

				$category = $this->conexion->quote($param->category);
				$display = $this->conexion->quote($param->display);

				$insertCategoryVALUE = "($idcategories, $category, $display, NULL, $param->count)";
				

				$insertCategoryRESULT = $this->conexion->query($insertCategorySQL . $insertCategoryVALUE);
				
				
				if ($insertCategoryRESULT) {

					$idCategory = $this->conexion->lastInsertId();

					$itemParam = array();
					$itemResult = array();

					foreach ($param as $key => $value) {
						if (preg_match("/^param_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemParam[$keyName[1]] = $value;
						} elseif (preg_match("/^item_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemResult[$keyName[1]] = $value;
						} elseif (preg_match("/^script_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$ValueScript = explode("\r", $value);
							foreach ($ValueScript as $key => $value) {
								$ValueScriptArray[] = trim($value);
							}
							$itemParam[$keyName[1]] = join(';', $ValueScriptArray);
						}
					}

					//Insertar Monitores

					if (isset($itemResult)) {

						$insertMonitorsDefaultSQL = "INSERT IGNORE INTO `bm_items` (`id_item`, `name`, `description`, `descriptionLong`, `type_item`, `delay`, `history`, `trend`, `type_poller`, `unit`, `snmp_oid`, `snmp_community`, `snmp_port`, `display`, `tags`)
                                    VALUES
                                        (1, 'Availability.bsw', 'Availability.bsw', 'A.Core - Availability', 'float', 600, 90, 365, 'snmp', ' ', '.1.3.6.1.2.1.1.5.0', 'public', 1161, 'none', NULL),
                                        (3, 'DurationOfTheTest.sh', 'DurationOfTheTest.sh', 'A.Core - DurationOfTheTest.sh', 'float', 600, 90, 365, 'bsw_agent', ' ', '', '', 0, 'none', NULL),
                                        (4, 'FinishTest', 'FinishTest', 'A.Core - FinishTest', 'float', 600, 90, 365, 'bsw_agent', ' ', '', '', 0, 'none', NULL);
                                    ";

						$insertMonitorsDefaultRESULT = $this->conexion->query($insertMonitorsDefaultSQL);

						$getItemSQL = 'SELECT `id_item`, PI.`item`, PI.`item_display` ,  `type_result`, `default`, `unit`, PG.`creationMonitor`, PG.`creationMonitorDisplay`,  PG.`class`, PG.`category`, PG.`id_profile`
                                              FROM `bm_profiles_item` PI 
                                            LEFT JOIN `bm_profiles_categories` PG ON PI.`id_categories`=PG.`id_categories` 
                                            WHERE `id_item` IN (' . join(',', array_keys($itemResult)) . ');';

						$getItemRESULT = $this->conexion->queryFetch($getItemSQL);

						$profileID = $getItemRESULT[0]['id_profile'];

						$insertMonitor = 'INSERT INTO `bm_items` 
                            (`name`, `description`, `descriptionLong`, `type_item`, 
                                `delay`, `history`, `trend`, `type_poller`, `unit`, `display`) VALUES ';

						foreach ($getItemRESULT as $key => $value) {

							$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);
							$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorName);
							$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
							$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
							$monitorName = str_replace("{CATEGORY_CODE}", $param->category, $monitorName);
							$monitorName = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorName);
							$monitorName = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorName);
							$monitorName = str_replace("{COUNT}", $param->count, $monitorName);

							$monitorDisplay = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitorDisplay']);
							$monitorDisplay = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_NAME}", $value['item'], $monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_CODE}", $param->category, $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorDisplay);
							$monitorDisplay = str_replace("{COUNT}", $param->count, $monitorDisplay);

							if ($itemResult[$value['id_item']] == 0) {

								$insertMonitorValue = "('$monitorName','$monitorName', '$monitorDisplay', '" . $value['type_result'] . "', 600, 90, 365, 'bsw_agent', '" . $value['unit'] . "', 'none');";

								$insertMonitorRESULT = $this->conexion->query($insertMonitor . $insertMonitorValue);

								if (!$insertMonitorRESULT) {
									$this->conexion->rollBack();
									$result['error'] = 'Error insert monitor';
									echo $this->basic->jsonEncode($result);
									exit ;
								} else {
									$itemValid[$value['id_item']] = array(
										'idMonitor' => $this->conexion->lastInsertId(),
										'nameMonitor' => $monitorName,
										'nameDisplayMonitor' => $monitorDisplay
									);
								}

							} else {
								/// Actualizar el nombre del item

								$itemValid[$value['id_item']] = array(
									'idMonitor' => $itemResult[$value['id_item']],
									'nameMonitor' => $monitorName,
									'nameDisplayMonitor' => $monitorDisplay
								);

							}

						}
					} else {
						//Get Profile ID Not Monitor
						$getProfileIDSQL = 'SELECT `id_profile` FROM `bm_profiles_categories` WHERE `id_categories` = ' . $idcategories . ' LIMIT 1';
						$getProfileIDRESULT = $this->conexion->queryFetch($getProfileIDSQL);

						if ($getProfileIDRESULT) {
							$profileID = $getProfileIDRESULT[0]['id_profile'];
						} else {
							$this->conexion->rollBack();
							$result['error'] = 'Error get profile id not monitor';
							echo $this->basic->jsonEncode($result);
							exit ;
						}

					}

					$insertItemSQL = 'INSERT INTO `bm_profiles_values` (`id_item`, `id_category`, `id_monitor`, `monitor_name`, `value`) VALUES ';

					foreach ($itemParam as $key => $value) {
						$valueQuote = $this->conexion->quote($value);
						$insertItemVALUE[] = "( $key, $idCategory, 0, null, $valueQuote)";
					}

					if (isset($itemValid)) {
						foreach ($itemValid as $key => $value) {
							$valueQuote = $this->conexion->quote($value['nameMonitor']);
							$insertItemVALUE[] = "( $key, $idCategory, " . $value['idMonitor'] . ", $valueQuote , null )";
						}
					}

					$insertItemRESULT = $this->conexion->query($insertItemSQL . join(',', $insertItemVALUE));

					if ($insertItemRESULT) {

						if (isset($itemValid)) {

							$getGroupIdProfileSQL = 'SELECT PP.`groupid` FROM `bm_profiles_permit` PP WHERE `id_profile` = ' . $profileID;

							$getGroupIdProfileRESULT = $this->conexion->queryFetch($getGroupIdProfileSQL);

							if ($getGroupIdProfileRESULT) {

								$isertItemsGroupsSQL = 'INSERT IGNORE INTO `bm_items_groups` 
                                                                (`id_item`, `groupid`, `status`) VALUES ';

								foreach ($getGroupIdProfileRESULT as $key => $value) {

									$groupid = $value['groupid'];

									foreach ($itemValid as $keyMonitor => $monitor) {
										$isertItemsGroupsValueSQL[] = "(" . $monitor['idMonitor'] . ",$groupid, 1)";
									}

								}

								$isertItemsGroupsRESULT = $this->conexion->query($isertItemsGroupsSQL . join(',', $isertItemsGroupsValueSQL));

								if ($isertItemsGroupsRESULT) {
									$this->conexion->commit();
									$result['status'] = true;
									echo $this->basic->jsonEncode($result);
									exit ;
								} else {
									$result['error'] = 'Error insert permit group monitor';
									$this->conexion->rollBack();
									echo $this->basic->jsonEncode($result);
									exit ;
								}
							}

						} else {
							$this->conexion->commit();
							$result['status'] = true;
							echo $this->basic->jsonEncode($result);
							exit ;
						}

					} else {
						$result['error'] = 'Error insert item profile value';
						$this->conexion->rollBack();
						echo $this->basic->jsonEncode($result);
						exit ;
					}

				} else {
					$result['error'] = 'Insert category failed';
					$this->conexion->rollBack();
					echo $this->basic->jsonEncode($result);
					exit ;
				}

			}

		} else {
			$result['error'] = 'Error id category not found';
		}

		$this->conexion->rollBack();
		echo $this->basic->jsonEncode($result);
		exit ;
	}

	public function deleteCategoryValue() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->id) && ($getParam->id != '')) {

			if (is_array($getParam->id)) {

				$inSQL = $this->conexion->arrayIN($getParam->id, false, true);

				$deleteProfileCategorySQL = "DELETE FROM `bm_items` WHERE `id_item` IN (select `id_monitor` from `bm_profiles_values` where `id_monitor` IS NOT NULL AND `id_monitor` > 0 AND `id_category` IN $inSQL);"; 
				//"DELETE FROM `bm_profiles_category` WHERE `id_category` IN " . $inSQL;
		
				$deleteProfileCategoryRESULT = $this->conexion->query($deleteProfileCategorySQL);

				if ($deleteProfileCategoryRESULT) {
					//codigo antiguo
					$deleteProfileCategoryValueItemsSQL ="DELETE FROM `bm_profiles_category` WHERE `id_category` IN " . $inSQL; 
					//"DELETE FROM `bm_items` WHERE `id_item` IN (select `id_monitor` from `bm_profiles_values` where `id_monitor` IS NOT NULL AND `id_monitor` > 0 AND `id_category` IN $inSQL);";
					$deleteProfileCategoryValueItemsRESULT = $this->conexion->query($deleteProfileCategoryValueItemsSQL);
					//nuevo código
					//$selectMonitoresAeliminarSQL= "select `id_monitor` from `bm_profiles_values` where `id_monitor` IS NOT NULL AND `id_monitor` > 0 AND `id_category`= . $inSQL;";
					//try{
					//	$arrayMonitoresAeliminar=$this->conexion->queryFetch($selectMonitoresAeliminarSQL);  //esta mala la funcion de consulta
						
					//}catch(exception $e){
					//	echo "error en select nuevo";
					//}					
					//echo "--> ".count($arrayMonitoresAeliminar);
					$eliminadoTable_BM_ITEMS="OK";
					//foreach ($arrayMonitoresAeliminar as $iD1) {
						
						
						
						//try{
							//$deleteItemsSQL= "DELETE FROM `bm_items` WHERE `id_item`=".$iD1;
						//}catch(exception $e){
							//$eliminadoTable_BM_ITEMS="FALSE";
						//}						 
					//}					
					if($deleteProfileCategoryValueItemsRESULT) {//($eliminadoTable_BM_ITEMS=="OK"){ //
						$deleteProfileCategoryValueSQL = "DELETE FROM `bm_profiles_values` WHERE `id_category` IN " . $inSQL;
						$deleteProfileCategoryValueRESULT = $this->conexion->query($deleteProfileCategoryValueSQL);

						if ($deleteProfileCategoryValueRESULT) {
							$result['status'] = true;
						} else {
							$result['error'] = $this->language->ITERNAL_ERROR;
						}

					} else {
						$result['error'] = $this->language->ITERNAL_ERROR;
					}

				} else {
					$result['error'] = $this->language->ITERNAL_ERROR;
				}

			} else {
				$result['error'] = $this->language->ERROR_INVALID_PARAM;
			}

		} else {
			$result['error'] = $this->language->ERROR_INVALID_PARAM;
		}

		$this->basic->jsonEncode($result);
		exit ;
	}

	public function fixIdMonitor() {
		$deleteIdMonitorGroupsSQL = "DELETE FROM `bm_items_groups` WHERE `id_item` IN (SELECT PV.`id_monitor` as 'id_item'
                    FROM `bm_profiles_values` PV
                    WHERE `id_monitor` > 0);";
		$deleteIdMonitorGroupsRESULT = $this->conexion->query($deleteIdMonitorGroupsSQL);

		$insertIdMonitorGroupsSQL = "INSERT INTO `bm_items_groups` ( `id_item`, `groupid`, `status`) SELECT DISTINCT PV.`id_monitor` as 'id_item',PP.`groupid`, '1' as `status`
    FROM `bm_profiles_values` PV
        LEFT JOIN `bm_profiles_category` PC ON PV.`id_category`=PC.`id_category`
        LEFT JOIN `bm_profiles_categories` PCG ON PC.`id_categories`=PCG.`id_categories`
        LEFT JOIN `bm_profiles_permit` PP ON PCG.`id_profile`=PP.`id_profile`
        INNER JOIN `bm_items` I ON I.`id_item`=PV.`id_monitor`
    WHERE 
        `id_monitor` > 0 AND
        PP.`groupid` IS NOT NULL;";
		$insertIdMonitorGroupsRESULT = $this->conexion->query($insertIdMonitorGroupsSQL);

	}

	public function fixNameMonitor() {
		$this->fixIdMonitor();
		$getProfileItemValueSQL = "SELECT 
                        PC.`creationCategory`, PC.`creationMonitor`, PC.`creationMonitorDisplay`, PC.`category` AS 'categories', PC.`class`,
                        PI.`item`, PI.`item_display`,PCY.`category`,PCY.`display`,PCY.`count`,PV.`id_monitor`,PI.`unit`,PI.`saveIP`
                    FROM
                        `bm_profiles_categories` PC
                            LEFT JOIN `bm_profiles_item` PI ON PC.`id_categories`=PI.`id_categories` 
                            LEFT JOIN `bm_profiles_category` PCY ON PI.`id_categories` =PCY.`id_categories`
                            LEFT JOIN `bm_profiles_values` PV ON (PI.`id_item`=PV.`id_item` AND PCY.`id_category`=PV.`id_category`)
                    WHERE PI.`type`='result'";
		$getProfileItemValueRESULT = $this->conexion->queryFetch($getProfileItemValueSQL);

		if ($getProfileItemValueRESULT) {

			foreach ($getProfileItemValueRESULT as $key => $value) {

				$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);
				$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['categories'], $monitorName);
				$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
				$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
				$monitorName = str_replace("{CATEGORY_CODE}", $value['category'], $monitorName);
				$monitorName = str_replace("{CATEGORY_DESCRIPTION}", $value['display'], $monitorName);
				$monitorName = str_replace("{COUNT}", $value['count'], $monitorName);

				$monitorDisplay = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitorDisplay']);
				$monitorDisplay = str_replace("{CATEGORIES_DESCRIPTION}", $value['categories'], $monitorDisplay);
				$monitorDisplay = str_replace("{ITEM_NAME}", $value['item'], $monitorDisplay);
				$monitorDisplay = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorDisplay);
				$monitorDisplay = str_replace("{CATEGORY_CODE}", $value['category'], $monitorDisplay);
				$monitorDisplay = str_replace("{CATEGORY_DESCRIPTION}", $value['display'], $monitorDisplay);
				$monitorDisplay = str_replace("{COUNT}", $value['count'], $monitorDisplay);

				if (is_numeric($value['id_monitor'])) {
					if ($value['saveIP'] != 'false' || $value['saveIP'] != 'true') {
						$saveIP = 'false';
					} else {
						$saveIP = $value['saveIP'];
					}
					$insertOrUpdateValue[] = "('" . $value['id_monitor'] . "', '$monitorName', '$monitorName', '$monitorDisplay', 
                                                    'float', 600, 90, 365, 'bsw_agent', '" . $value['unit'] . "', NULL, '', 0, 'none', NULL,'$saveIP')";
				}

			}

			$insertOrUpdate = "INSERT INTO `bm_items` (`id_item`, `name`, `description`, `descriptionLong`, 
                                        `type_item`, `delay`, `history`, `trend`, `type_poller`, `unit`, `snmp_oid`, 
                                                `snmp_community`, `snmp_port`, `display`, `tags`,`saveIP`) VALUES ";

			$insertOrUpdateSQL = $insertOrUpdate . join(",", $insertOrUpdateValue) . " ON DUPLICATE KEY UPDATE `name`=VALUES(`name`), 
                            `description`=VALUES(`description`), `descriptionLong`=VALUES(`descriptionLong`), `unit`=VALUES(`unit`) , `saveIP`=VALUES(`saveIP`) ";

			$insertOrUpdateRESULT = $this->conexion->query($insertOrUpdateSQL);

		}
	}

	public function editCategoryValue($idCategory) {
		$param = (object)$_POST;

		$result['status'] = false;

		//prueba de estructura $_POST
		/***VARIABLES POR POST ***/

		$numero2 = count($_POST);
		$this->logs->error("count POST: ".$numero2);
		$tags2 = array_keys($_POST); // obtiene los nombres de las varibles
		$this->logs->error("tags array_keys: ".$tags2);
		$valores2 = array_values($_POST);// obtiene los valores de las varibles
		$this->logs->error("array_values: ".$valores2);
		
		// crea las variables y les asigna el valor
		$this->logs->error("Array POST enviado ACTUALMENTE");
		foreach ($_POST as $key => $value2) {
					$this->logs->error("  $key = ".$value2);	
		}			
		
		//FIN PRUEBA $_POST
		
		
		
		if (isset($idCategory) && is_numeric($idCategory)) {

			if (!isset($param->display) && ($param->display == '')) {
				$result['error'] = 'Error invalid param';
			} else {

				if (!isset($param->default) || ($param->default == '')) {
					$param->default = 'NONE';
				}

				$this->conexion->InicioTransaccion();

				$display = $this->conexion->quote($param->display);

				$updateCategorySQL = "UPDATE `bm_profiles_category` SET `display` = $display WHERE `id_category` = '$idCategory'";

				$updateCategoryRESULT = $this->conexion->query($updateCategorySQL);

				if ($updateCategoryRESULT) {

					$itemParam = array();
					$itemResult = array();

					foreach ($param as $key => $value) {
						if (preg_match("/^param_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemParam[$keyName[1]] = $value;
						} elseif (preg_match("/^item_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$itemResult[$keyName[1]] = $value;
						} elseif (preg_match("/^script_/i", $key)) {
							$keyName = explode("_", $key, 2);
							$ValueScript = explode("\r", $value);
							foreach ($ValueScript as $key => $value) {
								$ValueScriptArray[] = trim($value);
							}
							$itemParam[$keyName[1]] = join(';', $ValueScriptArray);
						}
					}

					//Insertar Monitores

					if (isset($itemResult)) {

						$getItemSQL = 'SELECT `id_item`, PI.`item`, PI.`item_display` ,  `type_result`, `default`, `unit`, PG.`creationMonitor`, PG.`creationMonitorDisplay`,  PG.`class`, PG.`category`, PG.`id_profile`
                                              FROM `bm_profiles_item` PI 
                                            LEFT JOIN `bm_profiles_categories` PG ON PI.`id_categories`=PG.`id_categories` 
                                            WHERE `id_item` IN (' . join(',', array_keys($itemResult)) . ');';

						$getItemRESULT = $this->conexion->queryFetch($getItemSQL);

						$profileID = $getItemRESULT[0]['id_profile'];

						$insertMonitor = 'INSERT INTO `bm_items` 
                            (`name`, `description`, `descriptionLong`, `type_item`, 
                                `delay`, `history`, `trend`, `type_poller`, `unit`, `display`) VALUES ';

						foreach ($getItemRESULT as $key => $value) {

							//$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);
							//$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorName);
							//$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
							//$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
							//$monitorName = str_replace("{CATEGORY_CODE}", $param->category, $monitorName);
							//$monitorName = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorName);
							//$monitorName = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorName);
							//$monitorName = str_replace("{COUNT}", $param->count, $monitorName);
							$monitorName = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitor']);														
							$monitorName = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorName);
							$this->logs->error("moni1: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{ITEM_NAME}", $value['item'], $monitorName);
							$this->logs->error("moni2: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorName);
							$this->logs->error("moni3: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_CODE}", $param->category, $monitorName);
							$this->logs->error("moni4: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorName);
							$this->logs->error("moni5: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorName);
							$this->logs->error("moni6: ID_PROFILE".$profileID."  ->".$monitorName);
							$monitorName = str_replace("{COUNT}", $param->count, $monitorName);
							$this->logs->error("moni7: ID_PROFILE".$profileID."  ->".$monitorName);

							$monitorDisplay = str_replace("{CATEGORIES_CLASS}", $value['class'], $value['creationMonitorDisplay']);
							$monitorDisplay = str_replace("{CATEGORIES_DESCRIPTION}", $value['category'], $monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_NAME}", $value['item'], $monitorDisplay);
							$monitorDisplay = str_replace("{ITEM_DESCRIPTION}", $value['item_display'], $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_CODE}", $param->category, $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DESCRIPTION}", $param->display, $monitorDisplay);
							$monitorDisplay = str_replace("{CATEGORY_DEFAULT}", $param->default, $monitorDisplay);
							$monitorDisplay = str_replace("{COUNT}", $param->count, $monitorDisplay);

							if ($itemResult[$value['id_item']] == 0) {

								$insertMonitorValue = "('$monitorName','$monitorName', '$monitorDisplay', '" . $value['type_result'] . "', 600, 90, 365, 'bsw_agent', '" . $value['unit'] . "', 'none');";

								$insertMonitorRESULT = $this->conexion->query($insertMonitor . $insertMonitorValue);

								if (!$insertMonitorRESULT) {
									$this->conexion->rollBack();
									$result['error'] = 'Error insert monitor';
									echo $this->basic->jsonEncode($result);
									exit ;
								} else {
									$itemValid[$value['id_item']] = array(
										'idMonitor' => $this->conexion->lastInsertId(),
										'nameMonitor' => $monitorName,
										'nameDisplayMonitor' => $monitorDisplay
									);
								}

							}

						}
					} else {
						//Get Profile ID Not Monitor
						$getProfileIDSQL = 'SELECT `id_profile` FROM `bm_profiles_categories` WHERE `id_categories` = ' . $idcategories . ' LIMIT 1';
						$getProfileIDRESULT = $this->conexion->queryFetch($getProfileIDSQL);

						if ($getProfileIDRESULT) {
							$profileID = $getProfileIDRESULT[0]['id_profile'];
						} else {
							$this->conexion->rollBack();
							$result['error'] = 'Error get profile id not monitor';
							echo $this->basic->jsonEncode($result);
							exit ;
						}

					}

					$insertItemSQL = 'INSERT INTO `bm_profiles_values` (`id_item`, `id_category`, `id_monitor`, `monitor_name`, `value`) VALUES ';

					foreach ($itemParam as $key => $value) {
						$valueQuote = $this->conexion->quote($value);
						$insertItemVALUE[] = "( $key, $idCategory, 0, null, $valueQuote)";
					}

					if (isset($itemValid)) {
						foreach ($itemValid as $key => $value) {
							$valueQuote = $this->conexion->quote($value['nameMonitor']);
							$insertItemVALUE[] = "( $key, $idCategory, " . $value['idMonitor'] . ", $valueQuote , null )";
						}
					}

					$insertItemRESULT = $this->conexion->query($insertItemSQL . join(',', $insertItemVALUE) . " ON DUPLICATE KEY UPDATE 
                                        `id_monitor`=VALUES(`id_monitor`),  
                                        `monitor_name`=VALUES(`monitor_name`),
                                        `value`=VALUES(`value`)");

					if ($insertItemRESULT) {

						if (isset($itemValid)) {

							$getGroupIdProfileSQL = 'SELECT PP.`groupid` FROM `bm_profiles_permit` PP WHERE `id_profile` = ' . $profileID;

							$getGroupIdProfileRESULT = $this->conexion->queryFetch($getGroupIdProfileSQL);

							if ($getGroupIdProfileRESULT) {

								$isertItemsGroupsSQL = 'INSERT IGNORE INTO `bm_items_groups` 
                                                                (`id_item`, `groupid`, `status`) VALUES ';

								foreach ($getGroupIdProfileRESULT as $key => $value) {

									$groupid = $value['groupid'];

									foreach ($itemValid as $keyMonitor => $monitor) {
										$isertItemsGroupsValueSQL[] = "(" . $monitor['idMonitor'] . ",$groupid, 1)";
									}

								}

								$isertItemsGroupsRESULT = $this->conexion->query($isertItemsGroupsSQL . join(',', $isertItemsGroupsValueSQL));

								if ($isertItemsGroupsRESULT) {
									$this->conexion->commit();
									$this->fixNameMonitor();
									$result['status'] = true;
									echo $this->basic->jsonEncode($result);
									exit ;
								} else {
									$result['error'] = 'Error insert permit group monitor';
									$this->conexion->rollBack();
									echo $this->basic->jsonEncode($result);
									exit ;
								}
							}

						} else {
							$this->conexion->commit();
							$this->fixNameMonitor();
							$result['status'] = true;
							echo $this->basic->jsonEncode($result);
							exit ;
						}

					} else {
						$result['error'] = 'Error insert item profile value';
						$this->conexion->rollBack();
						echo $this->basic->jsonEncode($result);
						exit ;
					}
				}
			}
		}
	}

	/* ###########################
	 * Clone Profile
	 */ ###########################

	public function cloneProfile($idprofile) {
		$param = (object)$_POST;
		$result['status'] = false;

		$this->conexion->InicioTransaccion();

		//Creado Profile

		$insertProfileSQL = "INSERT INTO `bm_profiles` ( `profile`) VALUES ('$param->name');";
		$insertProfileRESULT = $this->conexion->query($insertProfileSQL);

		if ($insertProfileRESULT) {
			$idProfileNew = $this->conexion->lastInsertId();

			//Insert Categories

			$insertCategoriesSQL = "INSERT INTO `bm_profiles_categories` 
                        ( `id_profile`, `subcategory`, `category`, `class`, `count`, `global`, 
                        `creationCategory`, `creationMonitor`, `creationMonitorDisplay`,`sequenceCode`) 
                        SELECT  '$idProfileNew' as 'id_profile', `subcategory`, `category`, `class`, `count`,`global`,
                        `creationCategory`,`creationMonitor`,`creationMonitorDisplay` , `sequenceCode`
                        FROM `bm_profiles_categories` WHERE `id_profile` = '$idprofile';";

			$insertCategoriesRESULT = $this->conexion->query($insertCategoriesSQL);

			if ($insertCategoriesRESULT) {
				$getCategoriesSQL = "SELECT GROUP_CONCAT(`id_categories` ORDER BY `id_profile` ASC)  as 'categories'
                                            FROM  `bm_profiles_categories` 
                                            WHERE `id_profile` IN ($idprofile,$idProfileNew) GROUP BY `category`,`class`";
				$getCategoriesRESULT = $this->conexion->queryFetch($getCategoriesSQL);

				if ($getCategoriesRESULT) {
					foreach ($getCategoriesRESULT as $key => $value) {
						list($idOld, $idNew) = explode(',', $value['categories']);
						$changeIdCategories[(int)$idOld] = (int)$idNew;
					}

					//Get Items for Categories

					$getItemsCategoriesSQL = " SELECT  `bm_profiles_item`.* FROM `bm_profiles_categories`
                                                 LEFT JOIN `bm_profiles_item` USING(`id_categories`)
                                                  WHERE `id_profile` = '$idprofile' AND `id_item` IS NOT NULL;";

					$getItemsCategoriesRESUL = $this->conexion->queryFetch($getItemsCategoriesSQL);

					if ($getItemsCategoriesRESUL) {

						$insertItemsSQL = "INSERT INTO `bm_profiles_item` (`id_categories`, `item`, 
                                                `item_display`, `type`, `type_result`, 
                                                        `default`, `report`, `unit`, `displayINI`) VALUES ";
						$insertFormat = "( %d , '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
						foreach ($getItemsCategoriesRESUL as $key => $value) {
							$insertItemsValuesSQL[] = sprintf($insertFormat, $changeIdCategories[$value['id_categories']], $value['item'], $value['item_display'], $value['type'], $value['type_result'], $value['default'], $value['report'], $value['unit'], $value['displayINI']);
						}

						$insertItemsSQL = $insertItemsSQL . join(',', $insertItemsValuesSQL);

						$insertItemsRESULT = $this->conexion->query($insertItemsSQL);

						if ($insertItemsRESULT) {

							//Insert Param

							$insertParamSQL = "INSERT INTO `bm_profiles_param` (`id_profile`, `name`, `description`, `type`, `value`, `visible`)
                                                        VALUES
                                                            ($idProfileNew, 'SCHEDULE', 'Horario de pruebas', 'string', '1-7,00:00-23:59', 'true'),
                                                            ($idProfileNew, 'SERIAL', 'Serie de Pruebas', 'string', '', 'true');";

							$this->conexion->query($insertParamSQL);

							//Insert Permit
							$insertPermitSQL = " INSERT INTO `bm_profiles_permit` (`id_profile`, `groupid`) VALUES ";

							foreach ($param->groupid as $key => $value) {
								$insertPermitRowsSQL[] = "( $idProfileNew, $value)";
							}

							$this->conexion->query($insertPermitSQL . join(",", $insertPermitRowsSQL));

							switch ($param->cloneMethod) {
								case '1' :
									$this->conexion->commit();
									$result['status'] = true;
									break;
								case '2' :
									$insertCategorySQL = "INSERT INTO  `bm_profiles_category` SELECT NULL AS 'id_category', PCT.`id_categories`,PC.`category`,PC.`display`,PC.`default`,PC.`count`,PC.`status`
                                                    FROM `bm_profiles_category`   PC
                                                        LEFT JOIN `bm_profiles_categories` PCT ON PCT.`class` = SUBSTRING_INDEX(PC.`category`,'-',1)
                                                    WHERE PC.`id_categories` IN (SELECT `id_categories` FROM `bm_profiles_categories` WHERE `id_profile` = $idprofile) 
                                                        AND PCT.`id_profile` = $idProfileNew;";
									$insertCategoryRESULT = $this->conexion->query($insertCategorySQL);

									if ($insertCategoryRESULT) {

										$getItemsOldToNewSQL = "SELECT GROUP_CONCAT(PI.`id_item` ORDER BY PI.`id_item` ASC)  as OldAndNew
                                                FROM `bm_profiles_item` PI 
                                                LEFT JOIN `bm_profiles_categories`  PC ON PI.`id_categories` = PC.`id_categories`
                                                WHERE `id_profile` IN ($idprofile,$idProfileNew)
                                                GROUP BY PI.`item`,PC.`category`
                                                ORDER BY PI.`id_item` ASC";

										$getItemsOldToNewRESULT = $this->conexion->queryFetch($getItemsOldToNewSQL);

										if ($getItemsOldToNewRESULT) {
											foreach ($getItemsOldToNewRESULT as $key => $value) {
												list($idOld, $idNew) = explode(',', $value['OldAndNew']);
												$changeIdItem[(int)$idOld] = (int)$idNew;
											}

											$getCategoryOldToNewSQL = "SELECT  GROUP_CONCAT(PC.`id_category` ORDER BY PC.`id_category` ASC)  as OldAndNew 
                                                    FROM `bm_profiles_category` PC
                                                    LEFT JOIN 
                                                        `bm_profiles_categories` PCT ON PC.`id_categories`=PCT.`id_categories`
                                                    WHERE `id_profile` IN ($idprofile,$idProfileNew)
                                                    GROUP BY PC.`category`
                                                    ORDER BY PC.`id_category` ASC;";

											$getCategoryOldToNewRESULT = $this->conexion->queryFetch($getCategoryOldToNewSQL);

											foreach ($getCategoryOldToNewRESULT as $key => $value) {
												list($idOld, $idNew) = explode(',', $value['OldAndNew']);
												$changeIdCtegory[(int)$idOld] = (int)$idNew;
											}

											$getValueItemOldSQL = "SELECT PV.*
                                                        FROM `bm_profiles_categories` PC 
                                                            LEFT JOIN `bm_profiles_category` PCT ON PC.`id_categories`=PCT.`id_categories`
                                                            LEFT JOIN `bm_profiles_item` PI ON PC.`id_categories`=PI.`id_categories`
                                                            LEFT JOIN `bm_profiles_values` PV ON PV.`id_category`=PCT.`id_category` AND PV.`id_item`=PI.`id_item`
                                                        WHERE `id_profile` = $idprofile AND PC.`class` != 'GLOBAL';";

											$getValueItemOldRESULT = $this->conexion->queryFetch($getValueItemOldSQL);

											$insertNewValueSQL = "INSERT INTO `bm_profiles_values` ( `id_item`, `id_category`, `id_monitor`, `monitor_name`, `value`) VALUES ";

											foreach ($getValueItemOldRESULT as $key => $value) {
												$insertNewValueRows[] = "(" . $changeIdItem[$value['id_item']] . ", " . $changeIdCtegory[$value['id_category']] . ", " . $value['id_monitor'] . ", '" . $value['monitor_name'] . "', '" . $value['value'] . "')";
											}

											$getItemsOldToNewRESULT = $this->conexion->query($insertNewValueSQL . join(",", $insertNewValueRows));

											if ($getItemsOldToNewRESULT) {
												$this->conexion->commit();
												$result['status'] = true;
											} else {
												$this->conexion->rollBack();
												$result['error'] = $this->language->ITERNAL_ERROR;
											}

										}
									} else {
										$result['error'] = $this->language->ITERNAL_ERROR;
									}

									break;
								case '3' :
									$insertCategorySQL = "INSERT INTO  `bm_profiles_category` SELECT NULL AS 'id_category', PCT.`id_categories`,PC.`category`,PC.`display`,PC.`default`,PC.`count`,PC.`status`
                                                    FROM `bm_profiles_category`   PC
                                                        LEFT JOIN `bm_profiles_categories` PCT ON PCT.`class` = SUBSTRING_INDEX(PC.`category`,'-',1)
                                                    WHERE PC.`id_categories` IN (SELECT `id_categories` FROM `bm_profiles_categories` WHERE `id_profile` = $idprofile) 
                                                        AND PCT.`id_profile` = $idProfileNew;";
									$insertCategoryRESULT = $this->conexion->query($insertCategorySQL);

									if ($insertCategoryRESULT) {

										$this->conexion->commit();
										$result['status'] = true;
									} else {
										$this->conexion->rollBack();
										$result['error'] = $this->language->ITERNAL_ERROR;
									}
									break;
								default :
									$this->conexion->rollBack();
									break;
							}

						} else {
							$this->conexion->rollBack();
						}

					} else {
						$this->conexion->rollBack();
					}

				} else {
					$this->conexion->rollBack();
				}
			} else {
				$this->conexion->rollBack();
				$result['error'] = $this->language->ITERNAL_ERROR;
			}

		} else {
			$this->conexion->rollBack();
			$result['error'] = $this->language->ITERNAL_ERROR;
		}

		$this->basic->jsonEncode($result);
		exit ;

	}

	/**
	 * get Form Sequence Profile
	 *
	 * @param string $idprofile Id Profile
	 *
	 * @return string
	 */

	public function getSequenceForm($idprofile) {
		$valida = $this->protect->access_page('CONFIG_PROFILES_SEQUENCE');

		$return['status'] = false;

		if ($valida) {
			$getCategoryProfileParamSQL = "SELECT `value` 
                        FROM `bm_profiles_param` 
                        WHERE `id_profile` = $idprofile AND `name` = 'SERIAL' LIMIT 1";

			$getCategoryProfileParamRESULT = $this->conexion->queryFetch($getCategoryProfileParamSQL);

			if (!$getCategoryProfileParamRESULT) {
				$insertParamProfileSQL = "INSERT INTO `bm_profiles_param` (`id_profile`, `name`, `description`, `type`, `value`, `visible`)
                                        VALUES
                                            ( $idprofile, 'SCHEDULE', 'Horario de pruebas', 'string', '1-7,00:00-23:59', 'true'),
                                            ( $idprofile, 'SERIAL', 'Serie de Pruebas', 'string', '', 'true');";
				$getCategoryProfileParamRESULT = $this->conexion->query($insertParamProfileSQL);
			}

			if ($getCategoryProfileParamRESULT) {

				$sequence = str_split($getCategoryProfileParamRESULT[0]['value']);

				$getCategoryProfileSQL = "SELECT `id_categories`,`sequenceCode`, `category` 
                                                FROM `bm_profiles_categories` WHERE `id_profile` = $idprofile AND `sequenceCode` != '0'";
				$getCategoryProfileRESULT = $this->conexion->queryFetch($getCategoryProfileSQL);

				if ($getCategoryProfileRESULT) {
					$activeForm = array_fill(0, count($sequence), '');
					foreach ($getCategoryProfileRESULT as $key => $value) {
						if (in_array($value['sequenceCode'], $sequence)) {
							$keySequence = array_search($value['sequenceCode'], $sequence);
							$activeForm[$keySequence] = '<li class="ui-state-default" title="' . $value['sequenceCode'] . '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' . $value['category'] . '</li>';
						} else {
							$inactiveForm[] = '<li class="ui-state-default" title="' . $value['sequenceCode'] . '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' . $value['category'] . '</li>';
						}
					}

					$varForm['activeForm'] = (isset($activeForm)) ? join("\n", $activeForm) : '';
					$varForm['inactiveForm'] = (isset($inactiveForm)) ? join("\n", $inactiveForm) : '';
					$varForm['idprofile'] = $idprofile;
					$return['status'] = true;
					$return['html'] = $this->plantilla->getOne("configuracion/profiles/configSequenceForm", $varForm);
				} else {
					$return['error'] = "Error get profile database";
				}
			} else {
				$return['error'] = "Error get serial database";
			}
		} else {
			$return['error'] = "Access deny";
		}

		$this->basic->jsonEncode($return);
		exit ;

	}

	/**
	 * Change  Sequence Profile
	 *
	 *
	 * @return string
	 */

	public function changeSequence($idprofile) {
		$param = (object)$_POST;
		$return['status'] = false;
		if (is_array($param->activeArray) && is_numeric($idprofile)) {
			$sequence = join('', $param->activeArray);

			$updateSequenceSQL = "UPDATE `bm_profiles_param` SET `value` = '$sequence' WHERE `id_profile` = $idprofile AND `name` = 'SERIAL'";

			$updateSequenceRESULT = $this->conexion->query($updateSequenceSQL);

			if ($updateSequenceRESULT) {
				$return['status'] = true;
			}
		}
		$this->basic->jsonEncode($return);
		exit ;
	}

	/**
	 * get Form Schedule Profile
	 *
	 * @param string $idprofile Id Profile
	 *
	 * @return string
	 */

	public function getScheduleForm($idprofile) {
		$valida = $this->protect->allowed('CONFIG_PROFILES_SCHEDULE');

		$return['status'] = false;

		if ($valida) {

			$getProfileScheduleSQL = " SELECT PP.`name`, PP.`value`  
                            FROM `bm_profiles_param` PP
                            WHERE `id_profile` = $idprofile AND `visible` = 'true' AND  PP.`name` = 'SCHEDULE' LIMIT 1";

			$updateSequenceRESULT = $this->conexion->queryFetch($getProfileScheduleSQL);

			if ($updateSequenceRESULT) {

				$shortDays = $this->language->SHORTS_DAYS;

				$schedule = $updateSequenceRESULT[0]['value'];
				$schedule = explode(';', $schedule);

				foreach ($schedule as $key => $value) {
					list($days, $hoursRange) = explode(',', $value);

					$daysRange = explode('-', $days);

					if (!isset($daysRange[1])) {
						$daysRange[1] = $daysRange[0];
					}

					for ($i = $daysRange[0]; $i <= $daysRange[1]; $i++) {
						$scheduleArray[$i] = $hoursRange;
					}
				}

				foreach ($shortDays as $key => $value) {
					if (isset($scheduleArray[$key])) {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}
					$weekName[] = '<div class="none" id="row"><label for="check' . $value . '" name="check' . $value . '" >' . $value . '</label><input type="checkbox" id="check' . $value . '" name="d' . $key . '" ' . $checked . '></div>';
				}

				$hours = explode('-', $hoursRange);

				list($hourStart, $secondStart) = explode(':', $hours[0]);
				list($hourEnd, $secondEnd) = explode(':', $hours[1]);

				$varForm['weekName'] = join("\n", $weekName);

				$varForm['hourStart'] = $hourStart;
				$varForm['secondStart'] = $secondStart;
				$varForm['hourEnd'] = $hourEnd;
				$varForm['secondEnd'] = $secondEnd;
			}

			$varForm['idprofile'] = $idprofile;
			$return['status'] = true;
			$return['html'] = $this->plantilla->getOne("configuracion/profiles/configScheduleForm", $varForm);

		} else {
			$return['error'] = $this->language->access_deny;
		}

		$this->basic->jsonEncode($return);
		exit ;

	}

	/**
	 * Change  Schedule Profile
	 *
	 * @param string $idprofile Id Profile
	 *
	 * @return string json
	 */

	public function changeSchedule($idprofile) {
		$param = (object)$_POST;
		$return['status'] = false;
		if (is_numeric($idprofile)) {

			//HOUR

			foreach ($param as $key => $value) {
				if (is_numeric($value)) {
					if ($value < 10) {
						$param->$key = "0" . $value;
					}
				}
			}

			$hourSet = $param->hourStart . ":" . $param->secondStart . "-" . $param->hourEnd . ":" . $param->secondEnd;

			$schedule = array();
			$seq = 0;
			$start = false;
			for ($i = 1; $i <= 7; $i++) {
				$select = "d" . $i;
				if (isset($param->$select)) {
					if (!isset($initial)) {
						$initial = $i;
					}
					$finishRange = $i;
					$notSet = false;
					$start = true;
				} else {
					$notSet = true;
				}

				if (isset($initial)) {
					if ($initial === $finishRange) {
						$range = $initial;
					} else {
						$range = $initial . "-" . $finishRange;
					}
					$schedule[$seq] = "$range,$hourSet";
				}

				if ($notSet === true) {
					unset($initial);
					if ($start === true) {
						$seq++;
					}
				}
			}

			$schedule = join(";", $schedule);

			$updateScheduleSQL = "UPDATE `bm_profiles_param` SET `value` = '$schedule' WHERE `id_profile` =
				        $idprofile AND `name` = 'SCHEDULE'";

			$updateScheduleRESULT = $this->conexion->query($updateScheduleSQL);

			if ($updateScheduleSQL) {
				$return['status'] = true;
			} else {
				$return['error'] = $this->language->ITERNAL_ERROR;
			}

		} else {
			$return['error'] = $this->language->ERROR_INVALID_PARAM;
		}
		$this->basic->jsonEncode($return);
		exit ;
	}

	/**
	 * Get Param Profile
	 *
	 * @return string
	 */

	public function getTableParameters($idProfile) {
		$getParam = (object)$_POST;

		//Valores iniciales , libreria flexigrid
		$var = $this->basic->setParamTable($_POST, 'id_param');

		//Parametros Table

		$data = array();

		$data['page'] = $var->page;

		$data['rows'] = array();

		//Total rows

		$getTotalRowsSQL = "SELECT count(`id_param`) as 'Total' 
                FROM `bm_profiles_param` 
                WHERE `id_profile` = $idProfile AND `name` NOT IN ('SCHEDULE','SERIAL','HOLIDAY')";

		$data['sql'] = $getTotalRowsSQL;

		$getTotalRowsRESULT = $this->conexion->queryFetch($getTotalRowsSQL);

		if ($getTotalRowsRESULT)
			$data['total'] = $getTotalRowsRESULT[0]['Total'];
		else {
			$data['total'] = 0;
			$this->basic->jsonEncode($data);
			exit ;
		}

		//Rows

		$getRowsSQL = "SELECT `id_param`,`name`,`description`,`type`,`value`,`visible`
            FROM `bm_profiles_param` 
            WHERE `id_profile` = $idProfile AND `name` NOT IN ('SCHEDULE','SERIAL','HOLIDAY') $var->sortSql $var->limitSql";

		$getRowsRESULT = $this->conexion->queryFetch($getRowsSQL);

		if ($getRowsRESULT) {
			foreach ($getRowsRESULT as $key => $row) {

				$option = '<button id="paramProfileEdit" onclick="$.paramProfileEdit(' . $row['id_param'] . ')" name="paramProfileEdit">' . $this->language->EDIT . '</button>';

				if ($row['visible'] == 'true') {
					$status = '<a onclick="$.paramProfileStatus(' . $row['id_param'] . ',true)" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button on/off"> <span class="ui-button-icon-primary ui-icon ui-icon-power"></span><span class="ui-button-text">' . $this->language->ENABLED . '</span> </a>';
				} else {
					$status = '<a onclick="$.paramProfileStatus(' . $row['id_param'] . ',false)" class="ui-button ui-widget ui-state-error ui-corner-all ui-button-icon-only" role="button" aria-disabled="false" title="Button on/off"> <span class="ui-button-icon-primary ui-icon ui-icon-power"></span><span class="ui-button-text">' . $this->language->DISABLED . '</span> </a>';
				}

				$data['rows'][] = array(
					'id' => $row['id_param'],
					'cell' => array(
						'id_param' => $row['id_param'],
						'name' => $row['name'],
						'description' => $row['description'],
						'status' => $status,
						'option' => $option,
					)
				);
			}
		}

		$this->basic->jsonEncode($data);
		exit ;
	}

	public function createParameterProfile($idprofile) {
		if (is_numeric($idprofile)) {
			$valida = $this->protect->access_page('CONFIG_PROFILES_PARAM_NEW');

			if ($valida) {
				$getParam = (object)$_POST;

				$insertParamProfileSQL = sprintf("INSERT INTO `bm_profiles_param` ( `id_profile`, `name`, `description`, `type`, `value`, `visible`)
                                            VALUE (%d, '%s', '%s', '%s', '%s', 'true')", $idprofile, $getParam->name, $getParam->description, $getParam->type, $getParam->value);

				$insertParamProfileRESULT = $this->conexion->query($insertParamProfileSQL);

				if ($insertParamProfileRESULT) {
					$return['status'] = true;
				} else {
					$return['status'] = false;
					$return['error'] = $this->language->ITERNAL_ERROR;
				}

			} else {
				$return['status'] = false;
				$return['error'] = $this->language->access_deny;
			}

		} else {
			$return['status'] = false;
			$return['error'] = $this->language->ERROR_INVALID_PARAM;
		}

		$this->basic->jsonEncode($return);
		exit ;

	}

	public function deleteParameterProfile() {
		$getParam = (object)$_POST;

		$result['status'] = false;

		if (isset($getParam->id) && ($getParam->id != '')) {

			if (is_array($getParam->id)) {

				$deleteProfileSQL = "DELETE FROM `bm_profiles_param` WHERE `id_param` IN ";

				$deleteProfileValueSQL = $this->conexion->arrayIN($getParam->id, false, true);

				$deleteProfileRESULT = $this->conexion->query($deleteProfileSQL . $deleteProfileValueSQL);

				if ($deleteProfileRESULT) {
					$result['status'] = true;
				} else {
					$result['error'] = $this->language->ITERNAL_ERROR;
				}

			} else {
				$result['error'] = $this->language->ERROR_INVALID_PARAM;
			}

		} else {
			$result['error'] = $this->language->ERROR_INVALID_PARAM;
		}

		$this->basic->jsonEncode($result);
		exit ;
	}

	public function getFormParameterProfile() {
		$paramID = $_POST['id'];

		$valida = $this->protect->access_page('CONFIG_PROFILES_PARAM_EDIT');

		$this->plantilla->loadSec("configuracion/profiles/configProfileParameterForm", $valida, false);

		$varSite["formID"] = 'formProfileParametersEdit';

		//Get Categories

		$getParamProfileSQL = "SELECT `id_profile`,`name`,`description`,`type`,`value`  FROM `bm_profiles_param` WHERE `id_param` =  ? LIMIT 1";

		$getParamProfileRESULT = $this->conexion->queryFetch($getParamProfileSQL, $paramID);

		if ($getParamProfileRESULT) {
			$formValue = $getParamProfileRESULT[0];
			foreach ($formValue as $key => $value) {
				if ($key == 'type') {
					$varSite['optionTypeData'] = $this->basic->getOptionValue('type_data', $value);
				} else {
					$varSite[$key] = $value;
				}

			}

		}

		$this->plantilla->set($varSite);
		$this->plantilla->finalize();
	}

	public function editParameterProfile($idparamProfile) {
		if (is_numeric($idparamProfile)) {
			$valida = $this->protect->access_page('CONFIG_PROFILES_PARAM_EDIT');

			if ($valida) {
				$getParam = (object)$_POST;

				$insertParamProfileSQL = sprintf("UPDATE `bm_profiles_param` 
                    SET `name` = '%s', `description` = '%s', `type` = '%s', `value` = '%s' 
                    WHERE `id_param` = %d;", $getParam->name, $getParam->description, $getParam->type, $getParam->value, $idparamProfile);

				$insertParamProfileRESULT = $this->conexion->query($insertParamProfileSQL);

				if ($insertParamProfileRESULT) {
					$return['status'] = true;
				} else {
					$return['status'] = false;
					$return['error'] = $this->language->ITERNAL_ERROR;
				}

			} else {
				$return['status'] = false;
				$return['error'] = $this->language->access_deny;
			}

		} else {
			$return['status'] = false;
			$return['error'] = $this->language->ERROR_INVALID_PARAM;
		}

		$this->basic->jsonEncode($return);
		exit ;

	}

	public function changeStatusParamProfile($idParam, $status) {
		$valida = $this->protect->access_page('CONFIG_PROFILES_PARAM_STATUS');

		$data['status'] = true;

		if ($valida) {

			if ($status) {
				$status = 'false';
			} else {
				$status = 'true';
			}

			$updateStatusParamProfileSQL = "UPDATE `bm_profiles_param` SET `visible` = '$status' WHERE `id_param` = '$idParam';";

			$valida = $this->conexion->query($updateStatusParamProfileSQL);

			if (!$valida) {
				$data['error'] = $this->language->ITERNAL_ERROR;
				$data['status'] = false;
			}

		} else {
			$data['error'] = $this->language->access_deny;
			$data['status'] = false;
		}

		echo $this->basic->jsonEncode($data);
		exit ;
	}

	public function export() {
		ini_set('display_errors', true);

		if (isset($_POST['probe']) && $_POST['probe'] == '1') {
			$probe = true;
		} else {
			$probe = false;
		}

		if (isset($_POST['idProfile']) && is_numeric($_POST['idProfile'])) {
			$idProfile = $_POST['idProfile'];
		} else {
			$result['status'] = false;
			$this->basic->jsonEncode($result);
			exit ;
		}

		if (isset($_POST['typeFile']) && $_POST['typeFile'] == '0') {
			$file = $this->exportXml($idProfile);
		} else {
			$file = $this->exportExcel($idProfile);
		}

		$path = "/report/download/";

		if ($file !== false) {
			$result['status'] = true;
			$result['file'] = $path . $file;
		} else {
			$result['status'] = false;
		}

		$this->basic->jsonEncode($result);
		exit ;
	}

	private function exportXml($idProfile) {

		$xml = new SimpleXMLElement('<?xml version="1.0" standalone="yes"?><profile></profile>');

		$structure = $xml->addChild('structure');

		$getCategoriesSQL = ' SELECT * FROM `bm_profiles_categories` WHERE `id_profile` = ' . $idProfile;

		$getCategoriesRESULT = $this->conexion->queryFetch($getCategoriesSQL);

		$classes = $structure->addChild('classes');

		if ($getCategoriesRESULT) {
			foreach ($getCategoriesRESULT as $keyClass => $class) {

				$mono = $classes->addChild("class");
				unset($class['id_categories']);
				unset($class['id_profile']);
				$mono->addAttribute('name', $class['class']);

				foreach ($class as $key => $value) {
					$mono->addChild($key, $value);
				}
			}
		} else {
			return false;
		}

		$getItemsSQL = " SELECT PCIES.`class`,PI.* 
                FROM `bm_profiles_categories` PCIES  
                INNER JOIN `bm_profiles_item` PI ON PI.`id_categories`=PCIES.`id_categories`   
                WHERE `id_profile` = $idProfile
                ORDER BY PI.`id_categories`,PI.`type`";

		$getItemsRESULT = $this->conexion->queryFetch($getItemsSQL);

		$items = $structure->addChild('items');

		if ($getItemsRESULT) {
			foreach ($getItemsRESULT as $keyItem => $item) {

				$mono = $items->addChild("item");
				unset($item['id_item']);
				$mono->addAttribute('name', $item['class']);

				foreach ($item as $key => $value) {
					$mono->addChild($key, $value);
				}
			}
		} else {
			return false;
		}

		$getParamSQL = "SELECT * FROM `bm_profiles_param` WHERE `id_profile` = $idProfile";

		$getParamRESULT = $this->conexion->queryFetch($getParamSQL);

		$parameters = $structure->addChild('parameters');

		if ($getParamRESULT) {
			foreach ($getParamRESULT as $keyParam => $param) {

				$mono = $parameters->addChild("parameter");
				unset($param['id_param']);
				unset($param['id_profile']);
				foreach ($param as $key => $value) {
					$mono->addChild($key, $value);
				}
			}
		} else {
			return false;
		}

		$file = 'bMonitorQoETestPlan_' . date("Ymd") . '.xml';

		$path = site_path . '/upload/';

		$content = $xml->asXML($path . $file);

		if ($content !== false) {
			return $file;
		} else {
			return false;
		}
	}

	private function exportExcel($idProfile) {
		$getProfileSQL = "SELECT PCT.`count` , PCIES.`class`, PI.`item`, PI.`item_display`, PI.`description`, PI.`type_result`, PV.`value` 
                FROM 
                    `bm_profiles` P
                LEFT JOIN `bm_profiles_categories`  PCIES ON P.`id_profile` = PCIES.`id_profile`
                LEFT JOIN `bm_profiles_item` PI ON PI.`id_categories`=PCIES.`id_categories`
                LEFT JOIN `bm_profiles_category` PCT ON PCIES.`id_categories`= PCT.`id_categories`
                LEFT JOIN `bm_profiles_values` PV ON (PI.`id_item`=PV.`id_item` AND PCT.`id_category`=PV.`id_category`)
                WHERE P.`id_profile` = $idProfile AND PCT.`status` = 'true' AND PI.`type` = 'param'
                GROUP BY PV.`id_value`
                ORDER BY PCIES.`class`, PCT.`count`, PI.`type`,  PI.`item`";

		$getProfileRESULT = $this->conexion->queryFetch($getProfileSQL);

		if ($getProfileRESULT) {

			include APPS . "plugins/PHPExcel.php";

			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setCreator("Baking software")->setLastModifiedBy("bMonitor")->setTitle($this->language->GENERAL_REPORT)->setSubject("Export")->setDescription("Documento generado por report manager de bMonitor")->setKeywords("report")->setCategory("bMonitor");

			$page = 0;

			$line = 4;

			$letter = 65;

			$objPHPExcel->setActiveSheetIndex($page);

			$objPHPExcel->getActiveSheet()->setTitle($this->language->GENERAL_REPORT);

			$objPHPExcel->getActiveSheet()->mergeCells('A' . $line . ':D' . $line);

			$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, "QoE bMonitor - Test Plan");

			$line++;

			$styleHeader = array(
				'font' => array(
					'bold' => true,
					'size' => '10'
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'rotation' => 90,
					'startcolor' => array('argb' => 'A9A9A9'),
					'endcolor' => array('argb' => 'A9A9A9')
				)
			);

			$styleCell = array(
				'font' => array('size' => '10'),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
				),
				'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
			);

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

			foreach ($getProfileRESULT as $key => $value) {
				$letter = 65;
				if (!isset($monitorPre) || $monitorPre != $value['class']) {
					$objPHPExcel->getActiveSheet()->mergeCells('A' . $line . ':D' . $line);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, "Monitor: " . $value['class']);
					$objPHPExcel->getActiveSheet()->getStyle(chr($letter) . ($line) . ":D" . ($line))->applyFromArray($styleHeader);
					$line++;
					$monitorPre = $value['class'];
				}

				$objPHPExcel->getActiveSheet()->getStyle(chr($letter) . ($line) . ":D" . ($line))->applyFromArray($styleCell);

				$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, $value['count']);
				$letter++;
				$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, $value['item_display']);
				$letter++;
				$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, $value['value']);
				$objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $line)->getAlignment()->setWrapText(true);
				$letter++;
				$objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $line, $value['description']);
				$objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $line)->getAlignment()->setWrapText(true);
				$line++;
			}

			$file = 'bMonitorQoETestPlan_' . date("Ymd") . '.xlsx';

			$path = site_path . '/upload/';

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save($path . $file);

			return $file;

		} else {
			return false;
		}
	}

	public function import($idProfile) {
		$return['status'] = false;

		$var = (object)$_POST;

		if (isset($var->probe) && $var->probe == '1') {
			$probe = true;
		} else {
			$probe = false;
		}

		if (isset($var->method)) {
			$method = $var->method;
		} else {
			$method = 'insert';
		}

		if ($_FILES['fileXML']['error'] == 0) {

			$xml = simplexml_load_file($_FILES['fileXML']['tmp_name']);

			if ($xml !== false) {

				//CLASSES

				if (isset($xml->structure->classes)) {
					$classArry = array();
					foreach ($xml->structure->classes->class as $key => $classes) {
						$name = (string)$classes->attributes()->name;
						$classArry[$name];
						foreach ($classes as $keyc => $valuec) {
							$valuec = (string)$valuec;
							if ($valuec == 'false') {
								$valuec = false;
							} elseif ($valuec == 'true') {
								$valuec = true;
							} elseif (is_numeric($valuec)) {
								$valuec = (integer)$valuec;
							}
							$classArry[$name][$keyc] = $valuec;
						}
					}

				}
				$return['status'] = true;
			}
		}
		$this->basic->jsonEncode($return);
	}

}
?>