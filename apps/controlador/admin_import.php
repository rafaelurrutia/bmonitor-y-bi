<?php 

class admin_import extends Control {
	
	private $lastInsertId;
	static private $PDOInstance;

	private function connect($motor,$host,$user,$pass,$dbname)
	{
		$dsn = $motor.":host=".$host.";dbname=".$dbname;

		$limit = 10;
		$counter = 0;
					
		if(!self::$PDOInstance) {
			while (true) {
				
				try {
					self::$PDOInstance = new PDO($dsn, $user, $pass, array(
	    				PDO::ATTR_PERSISTENT => false
					));
					return true;
				} catch (PDOException $e) {
					$conn = null;
					$counter++;
					$this->logs->error("Error al conectar: ", $e->getMessage()); 
					if ($counter == $limit) {
						throw $e;
						return false;
					}
				}
			
			}
    	}
	}
	
	private function setImport($init,$import,$idimport = 0,$status = 'cargando',$data = null)
	{
		$data = print_r($data,true);
		
		if($init == 'start') {

			$set_import_sql = "INSERT INTO `bm_import` (`id_import`, `groupid`, `import`, `data`, `fechahora_start`, `status`) 
								VALUES (NULL, '$idimport', '$import', '$data', NOW(), '$status');";
								
			$query = $this->conexion->query($set_import_sql);
			
			if($query) {
			
				if($import == 'index') {
					$this->import_index = $this->conexion->lastInsertId();
					return $this->import_index;
				} else {
					return $this->conexion->lastInsertId();
				}
			
			} else {
				return false;
			}
		} elseif ($init == 'end') {
			
			$set_import_sql = "UPDATE `bm_import` SET `fechahora_end` = NOW(), `groupid` = $this->import_index,  `status` = '$status', `data` = '$data' WHERE `id_import` = '$idimport'";
			
			$query = $this->conexion->query($set_import_sql);
			
			return $query;
		}
	}
	
	private function queryFetchColAssoc($statement,$type = 'logs_import')
	{
		$time_start = microtime(true);
		$result = self::$PDOInstance->query($statement);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		if ($result === false){
    		$error = $this->errorInfo();
			$this->logs->error("QUERY_ESTADO: Error TIME: $time QUERY: ",$statement,$type);
			$this->logs->error("QUERY_ERROR: ",$error,$type);
		} else {
			$this->logs->debug("QUERY_ESTADO: OK TIME: $time QUERY: ",$statement,$type);
			return $result->fetchAll(PDO::FETCH_ASSOC);
		}	
		
	}
	
	private function query($statement,$type = 'logs_import')
	{
		$time_start = microtime(true);
		$result = self::$PDOInstance->query($statement);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		if ($result === false){
    		$error = $this->errorInfo();
			$this->logs->error("QUERY_ESTADO: Error TIME: $time QUERY: ",$statement,$type);
			$this->logs->error("QUERY_ERROR: ",$error,$type);
			return false;
		} else {
			$this->logs->debug("QUERY_ESTADO: OK TIME: $time QUERY: ",$statement,$type);
			return $result;
		}
	}
	
	public function import()
	{
		$getParam = (object)$_POST;
		
		//Borrar
		$getParam->motor = 'mysql';
		$getParam->host = 'localhost';
		$getParam->user = 'enlaces';
		$getParam->pass = 'bswenlaces';
		$getParam->dbname = 'bsw_ctr';
		
		$conn = $this->connect($getParam->motor,$getParam->host,$getParam->user,$getParam->pass,$getParam->dbname);
		
		if($conn) {
			
			$marcar = $this->setImport('start', 'index');
			
			//Iniciando import de grupos
			//$grupos = $this->import_groups();

			//Iniciando import de planes
			//$planes = $this->import_planes();
			
			//Iniciando import de planes
			//$host = $this->import_host();

			//Iniciando import stats2 (Neutralidad)
			//$stat1 = $this->import_stat1();
						
			//Iniciando import stats2 (Neutralidad)
			//$stat2= $this->import_stat2();
					
			//Iniciando import history
			$history = $this->import_history();
			
			//Iniciando import neutralidad
			//$neutralidad = $this->import_neutralidad();

		}
	}

	private function import_groups()
	{
		$marcar = $this->setImport('start', 'groups',$this->import_index);
		
		$get_groups_base_start_sql = "/* BSW - IMPORT */ SELECT * FROM `groups` LEFT JOIN `bsw_config` USING(`groupid`)";
		
		$get_groups_base_start = self::$PDOInstance->query($get_groups_base_start_sql);
		
		if($get_groups_base_start) {

			$status = true;
			
			//Borrando datos Actuales:
			
			$delete_groups_end_1 = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_host_groups`';
			$this->conexion->query($delete_groups_end_1);
			
			$delete_groups_end_2 = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_user_host_group`';
			$this->conexion->query($delete_groups_end_2);
			
			$get_groups_base_start =  $get_groups_base_start->fetchAll(PDO::FETCH_ASSOC);
			foreach ($get_groups_base_start as $key => $group) {
				
				$gColum = array();
				$gValue = array();
				
				foreach ($group as $gkey => $gValues) {
					if($gValues == '') {
						$gValues = 'NULL';
					} else {
						$gValues = $gValues;
					}
					$gColum[] = '`'.$gkey.'`';
					$gValue[] = "'".$gValues."'";
				}
				
				if(strpos($group['name'], 'NEUTRALIDAD') !== false) {
					$gV['type'] = 'NEUTRALIDAD';
				} elseif(strpos($group['name'], 'FDT') !== false) {
					$gV['type'] = 'FDT';
				} else {
					$gV['type'] = 'NULL';
				}
				
		
				$insert_group = "INSERT INTO `bm_host_groups` (".join(',', $gColum).") VALUES (".join(',', $gValue).")";
										
				$valid_insert = $this->conexion->query($insert_group);
				
				if(!$valid_insert) {
					$status = false;
				} else {
					if(strpos($group['name'], 'NEUTRALIDAD') !== false) {
						$insert_group = sprintf("INSERT INTO `bm_user_host_group` ( `id_group`, `groupid`)
								VALUES (5, %s)",$group['groupid']);
					} elseif(strpos($group['name'], 'FDT') !== false) {
						$insert_group = sprintf("INSERT INTO `bm_user_host_group` ( `id_group`, `groupid`)
								VALUES (4, %s)",$group['groupid']);
					}
					
					$this->conexion->query($insert_group);
				}
		
			}
	
			if($status) {
				$this->setImport('end', 'groups',$marcar,'ok');
				return true;
			} else {
				$this->setImport('end', 'groups',$marcar,'error');
				exit;
			}
		} else {
			$this->setImport('end', 'groups',$marcar,'error');
			exit;
		}
	}

	private function import_planes()
	{
		$marcar = $this->setImport('start', 'planes',$this->import_index);
		
		$get_planes_base_start_sql = "/* BSW - IMPORT */ SELECT * FROM `bsw_plan`";
		
		$get_planes_base_start = self::$PDOInstance->query($get_planes_base_start_sql);
		
		$planes_id = array();
		
		if($get_planes_base_start) {

			$status = true;
			
			//Borrando datos Actuales:
			
			$delete_planes_end = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_plan_groups`';
			$this->conexion->query($delete_planes_end);
			
			$delete_planes_end = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_plan`';
			$this->conexion->query($delete_planes_end);
			
			//Insertando Sin plan
			$setSinPlan_sql = "INSERT INTO `bm_plan` ( `plan`, `plandesc`, `planname`, `tecnologia`, `nacD`, `nacU`, `locD`, `locU`, `intD`, `intU`, `nacDS`, `nacDT`, `nacUS`, `nacUT`, `locDS`, `locDT`, `locUS`, `locUT`, `intDS`, `intDT`, `intUS`, `intUT`, `sysctl`, `ppp`) VALUES
	('SINPLAN', 'SINPLAN', 'SINPLAN', '', 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'kernel.panic = 3\r\nnet.ipv4.conf.default.arp_ignore = 1\r\nnet.ipv4.conf.all.arp_ignore = 1\r\nnet.ipv4.ip_forward = 1\r\nnet.ipv4.icmp_echo_ignore_broadcasts = 1\r\nnet.ipv4.icmp_ignore_bogus_error_responses = 1\r\nnet.ipv4.tcp_ecn = 0\r\nnet.ipv4.tcp_fin_timeout = 30\r\nnet.ipv4.tcp_keepalive_time = 120\r\nnet.ipv4.tcp_syncookies = 1\r\nnet.ipv4.tcp_sack = 1\r\nnet.ipv4.tcp_fack = 1\r\nnet.ipv4.tcp_window_scaling = 1\r\nnet.ipv4.tcp_timestamps = 0\r\nnet.ipv4.tcp_rfc1337 = 1\r\nnet.ipv4.ip_no_pmtu_disc = 0\r\nnet.netfilter.nf_conntrack_checksum = 0\r\nnet.ipv4.netfilter.ip_conntrack_checksum = 0\r\nnet.ipv4.netfilter.ip_conntrack_max = 16384\r\nnet.ipv4.netfilter.ip_conntrack_tcp_timeout_established = 3600\r\nnet.ipv4.netfilter.ip_conntrack_udp_timeout = 60\r\nnet.ipv4.netfilter.ip_conntrack_udp_timeout_stream = 180\r\nnet.ipv4.route.flush = 1\r\n#net.core.rmem_max = 112500\r\n#net.core.wmem_max = 112500\r\n#net.ipv4.tcp_rmem = 8192 11250 112500\r\n#net.ipv4.tcp_wmem = 4096 11250 112500\r\n', NULL);
			";
			
			$this->conexion->query($setSinPlan_sql);

			$get_planes_base_start_sql =  $get_planes_base_start->fetchAll(PDO::FETCH_ASSOC);
			foreach ($get_planes_base_start_sql as $key => $planes) {
				
				$plan = $planes['plan'];
				$nacD = $planes['nacD'];  
				$nacU = $planes['nacU'];
				$locD = $planes['locD'];
				$locU = $planes['locU'];
				$intD = $planes['intD'];
				$intU = $planes['intU'];
				$groupid = $planes['groupid'];
				$nacDS = $planes['nacDS'];
				$nacDT = $planes['nacDT'];
				$nacUS = $planes['nacUS'];
				$nacUT = $planes['nacUT'];
				$locDS = $planes['locDS'];
				$locDT = $planes['locDT'];
				$locUS = $planes['locUS'];
				$locUT = $planes['locUT'];
				$intDS = $planes['intDS'];
				$intDT = $planes['intDT'];
				$intUS = $planes['intUS'];
				$intUT = $planes['intUT'];
				$sysctl = $planes['sysctl'];
				$plandesc = $planes['plandesc'];
				$planname = $planes['planname'];
				
				$setPlan_end_sql = "INSERT INTO `bm_plan` (`plan`, `plandesc`, `planname`, `tecnologia`, `nacD`, `nacU`, `locD`, `locU`, `intD`, `intU`, `nacDS`, `nacDT`, `nacUS`, `nacUT`, `locDS`, `locDT`, `locUS`, `locUT`, `intDS`, `intDT`, `intUS`, `intUT`, `sysctl`)
										VALUES 
											('$plan', '$plandesc', '$planname', '', $nacD, $nacU, $locD, $locU, $intD, $intU, $nacDS, $nacDT, $nacUS, $nacUT, $locDS, $locDT, $locUS, $locUT, $intDS, $intDT, $intUS, $intUT, '$sysctl')";
											
				$setPlan_end = $this->conexion->query($setPlan_end_sql);
				
				if(!$setPlan_end) {
						$status = false;
				} else {
					
					$id_new_plan = $this->conexion->lastInsertId();
					
					$planes_id[] = $id_new_plan;
					
					$insert_group_plan_sql = "/* BSW - IMPORT */ INSERT INTO `bm_plan_groups` (`id_plan`, `groupid`) VALUES ('$id_new_plan', '$groupid');";
					$valid = $this->conexion->query($insert_group_plan_sql);
					
					if(!$valid) {
						$status = false; 
					}
				}
			}
	
			if($status) {
				$this->setImport('end', 'planes',$marcar,'ok',$planes_id);
				return true;
			} else {
				$this->setImport('end', 'planes',$marcar,'error');
				exit;
			}
		} else {
			$this->setImport('end', 'planes',$marcar,'error');
			exit;
		}
	}

	private function import_host() {
		$marcar = $this->setImport('start', 'host',$this->import_index);
		
		$get_host_base_start_sql = "/* BSW - IMPORT */ SELECT * 
											FROM `hosts`  H
												LEFT JOIN `hosts_groups` HG ON H.`hostid`=HG.`hostid` 
												LEFT JOIN `hosts_profiles` HP ON H.`hostid`=HP.`hostid`
											WHERE HP.`hostid` IS NOT NULL";
		
		$get_host_base_start = $this->queryFetchColAssoc($get_host_base_start_sql);
		
		$host_id = array();
		
		$status_valid = true;
		
		if($get_host_base_start) {
			
			//Borrando datos Actuales:
			
			$delete_host_end = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_host`';
			$this->conexion->query($delete_host_end);

			$delete_host_end = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_host_detalle`';
			$this->conexion->query($delete_host_end);

			foreach ($get_host_base_start as $key => $host) {
				
				$host_id = $host['hostid'];
				$host_name = $host['host'];		
				$host_dns = $host['dns'];
				$ip_wan = $host['ip'];
				$mac = $host['macaddress'];
				$ip_lan = $host['os'];
				$netmask_lan = $host['serialno'];
				$planb_name = $host['plan'];
				$groupid = $host['groupid'];
				$mac_lan = $host['hardware'];
				
				//Valores Feature
				$rdb = $host['port'];
				$idservicio = $host['idservicio'];
				$idvivienda = $host['idvivienda'];
				$maccm = $host['maccm'];
				$intancia = $host['software']; 
				$coordenada = $host['ipmi_password']; 
				
				$wl_key = $host['ipmi_username'];
				$wl_ssid = $host['notes'];
				
				$cp =  $host['devicetype'];
				$wifi = $host['contact'];
				
				if(isset($planb_name)) {
					$get_id_plan_sql = "SELECT `id_plan` FROM `bm_plan` LEFT JOIN `bm_plan_groups` USING(`id_plan`)
							WHERE `plan` LIKE '$planb_name'  AND `groupid`=$groupid;";
					
					$get_id_plan = $this->conexion->queryFetch($get_id_plan_sql,'logs_import');
					
					if(count($get_id_plan) === 1) {
						$id_plan = $get_id_plan[0]['id_plan'];
					} else {
						$this->logs->error("Error al obtener el id del plan:",$planb_name,'logs_import');
						exit;
					}				
				} else {
					$id_plan = 1;
				}
				
				if(!isset($groupid)) {
					continue;
				}
				
				$status = (int)$host['status'];
				
				if($status === 0) {
					$status = 1;
				} else {
					$status = 0;
				}
				
				$availability = $host['available'];
				
				$insert_host_end_sql = "INSERT INTO `bm_host` (`id_host`, `groupid`, `host`, `dns`, `mac`, `use_ip`, `ip_wan`, `ip_lan`, `netmask_lan`,  `mac_lan` , `id_plan`, `status`, `availability`)
										VALUES
										($host_id, $groupid, '$host_name', '$host_dns', '$mac', '', '$ip_wan', '$ip_lan', '$netmask_lan', '$mac_lan', $id_plan , '$status', '$availability');";
										
				$insert_host_end_result =  $this->conexion->query($insert_host_end_sql,false,'logs_import');
				
				if(!$insert_host_end_result) {
					$this->logs->error("Falla la insercion del host ",$host_name,'logs_import');
					$status_valid = false; 
				} else {
					$id_new_host = $this->conexion->lastInsertId();
					
					$this->host_id_start_end[$host_id] = $id_new_host;
					
					$insert_feature_sql = "INSERT INTO `bm_host_detalle` (`id_host`, `id_feature`, `value`)
											VALUES
												($id_new_host, 1, '$rdb'),
												($id_new_host, 25,'$idservicio'),
												($id_new_host, 20, '$maccm'),
												($id_new_host, 2, '$idvivienda'),
												($id_new_host, 6, '$intancia'),
												($id_new_host, 7, '$coordenada'),
												($id_new_host, 11, '$wl_key'),
												($id_new_host, 4, '$wl_ssid'),
												($id_new_host, 5, '$cp'),
												($id_new_host, 3, '$wifi'),
												($id_new_host, 12, '')";

					$insert_feature_result =  $this->conexion->query($insert_feature_sql,false,'logs_import');
					
					if(!$insert_feature_result) {
						$this->logs->error("Falla al insertar dettale del host:",$id_new_host,'logs_import');
						$status_valid = false;
					}
				}
				
			}
			if($status_valid) {
				$this->genMonitores();
				$this->setImport('end', 'host',$marcar,'ok');
				return true;
			} else {
				$this->setImport('end', 'host',$marcar,'error');
				exit;
			}
			
		} else {
			$this->setImport('end', 'host',$marcar,'error');
			exit;
		}
	}

	private function genMonitores()
	{
		$getGroupsMonitores_sql = "SELECT HG.`groupid`, HG.`type` FROM `bm_host_groups` HG";
		
		$getGroupsMonitores_result = $this->conexion->queryFetch($getGroupsMonitores_sql);
		
		if($getGroupsMonitores_result) {
				
			$getGroupsMonitores = (object)$getGroupsMonitores_result[0];
	

			foreach ($getGroupsMonitores_result as $key => $groups) {
				if($groups['type'] == 'NEUTRALIDAD') {
					$getINItem = $this->parametro->get('IMPORT_ITEM_NEUTRALIDAD');
				} elseif ($groups['type'] == 'ENLACES') {
					$getINItem = $this->parametro->get('IMPORT_ITEM_FDT');
				}
				
				
				$setGroupsMonitor_sql = "INSERT INTO `bm_items_groups` (`id_item`,`groupid`) SELECT I.`id_item`,'".$groups['groupid']."' FROM `bm_items` I WHERE I. `id_item` IN (".$getINItem.")";
				$setGroupsMonitor_result = $this->conexion->query($setGroupsMonitor_sql);
	
			}
			
			$getMonitores_sql = "SELECT H.`id_host`,IG.`id_item`,I.`snmp_community`, I.`snmp_oid`, I.`snmp_port`
						FROM `bm_host` H
						LEFT JOIN `bm_items_groups` IG USING(`groupid`) 
						LEFT JOIN `bm_items` I ON  I.`id_item`=IG.`id_item`
						LEFT OUTER JOIN `bm_item_profile` IP ON IP.`id_host`=H.`id_host` AND IP.`id_item`=IG.`id_item`
						WHERE IP.`id_item` IS NULL";
						
			$getMonitores_result = $this->conexion->queryFetch($getMonitores_sql);
			
			if($getMonitores_result) {
				
				$insert_monitor_profile = "INSERT INTO `bm_item_profile` (`id_item`, `id_host`, `snmp_oid`, `snmp_port`, `snmp_community`, `nextcheck`) VALUES ";
				
				foreach ($getMonitores_result as $key => $host) {
					$insert_monitor_value[] = '('.$host['id_item'].','.$host['id_host'].',\''.$host['snmp_oid'].'\',\''.$host['snmp_port'].'\',\''.$host['snmp_community'].'\', NOW())';
				}
				
				$insert_monitor_profile_f = join(",", $insert_monitor_value);
				$insert_monitor_profile_f = $insert_monitor_profile.$insert_monitor_profile_f;
				$result_monitor_profile = $this->conexion->query($insert_monitor_profile_f);
				
			}
		} else {
			return false;
		}
		
	}
	
	public function import_history($value='')
	{
		$marcar = $this->setImport('start', 'history',$this->import_index);
		
		$LIMIT_GROUPS = $this->parametro->get('GROUPS_IMPORT_HISTORY',100000);
		
		/* $get_total_history_sql = "SELECT COUNT(*) as Total
				FROM `hosts` H
				INNER JOIN `items` I ON I.`hostid`=H.`hostid`
				INNER JOIN `history` HI ON HI.`itemid`=I.`itemid`
				WHERE H.`dns` LIKE 'BSW%';"; */
			
		//Borramos History
			
		$delete_history_end_sql = '/*  BSW - IMPORT */ TRUNCATE TABLE `bm_history`';
		$delete_history_end_result =  $this->conexion->query($delete_history_end_sql);

		if($delete_history_end_result) { 
			
			//get_item_id 
			$get_item_id_sql = "SELECT `id_item`,`description` FROM `bm_items` ORDER BY `id_item`";
			$get_item_id_result = $this->conexion->queryFetch($get_item_id_sql);
			
			$items = array();
			foreach ($get_item_id_result as $key => $value) {
				$items[$value['description']] = $value['id_item'];
			}
			
			//Generado tabla temporal
			
			$settmpTable = "CREATE TEMPORARY TABLE `tmp_import_history` (
			  `key_item` int(11) unsigned NOT NULL DEFAULT '0',
			  `hostid` int(11) unsigned NOT NULL DEFAULT '0',
			  `itemid` int(11) unsigned NOT NULL DEFAULT '0',
			  PRIMARY KEY (`itemid`),
			  KEY `HOST_KEY` (`hostid`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1";
			
			$query = self::$PDOInstance->query($settmpTable);
			
			$setKeyItemValue = "SELECT I.`description`,H.`hostid`,I.`itemid`						
							FROM `hosts` H
							INNER JOIN `items` I ON I.`hostid`=H.`hostid`
							WHERE H.`dns` LIKE 'BSW%'
							ORDER BY H.`hostid` ASC;";
							
			$setKeyItemValue_result = $this->queryFetchColAssoc($setKeyItemValue);
			
			$insert_tmp = "INSERT INTO `tmp_import_history` (`key_item`, `hostid`, `itemid`) VALUES ";
			$insert_tmp_value = array();
			foreach ($setKeyItemValue_result as $key => $value) {
				if(isset($items[$value['description']])){
							$idItem = $items[$value['description']];
				} else {
							$idItem = 'A';
				}
				if(is_numeric($idItem)){
					$insert_tmp_value[] = "($idItem, ".$value['hostid'].",  ".$value['itemid'].")";
				}
			}
			
			$insert_tmp_final = $insert_tmp.join(',',$insert_tmp_value);
			$query = self::$PDOInstance->query($insert_tmp_final);
			
			
			$get_history_rows_sql = "SELECT TH.`key_item`,TH.`hostid`,HI.`clock`,HI.`value`,HI.`valid` FROM tmp_import_history TH LEFT JOIN `history` HI USING(`itemid`) INTO OUTFILE '/tmp/historyBM.txt' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'";
			
			$query = self::$PDOInstance->query($get_history_rows_sql);
			
			$limpia =  exec('uniq /tmp/historyBM.txt > /tmp/historyBMU.txt ; chcon -R -t mysqld_tmp_t /tmp/historyBMU.txt');
			
			$import_history_end = "LOAD DATA INFILE '/tmp/historyBMU.txt'
				INTO TABLE bm_history 
				FIELDS TERMINATED BY ','
				ENCLOSED BY '\"' (`id_item`,`id_host`,`clock`,`value`,`valid`)";
				
			$insert_history_result = $this->conexion->query($import_history_end); 
			
			if($insert_history_result) {
				$status = true;
			} else {
				$status = false;
			}
			
			/*$settmpTable = "CREATE TEMPORARY TABLE idCOsas	SELECT I.`description`,H.`hostid`,I.`itemid`						
							FROM `hosts` H
							INNER JOIN `items` I ON I.`hostid`=H.`hostid`
							WHERE H.`dns` LIKE 'BSW%'
							ORDER BY H.`hostid` ASC;"; */
			//Insertando
			/*
			for ($i=1; $i <= $bucles; $i++) {

				$pageStart = ($i-1)*$LIMIT_GROUPS;
		
				$limitSql = " LIMIT $pageStart, $LIMIT_GROUPS";
				
				
			/***	$get_history_rows_sql = "SELECT I.`description`,H.`hostid`,HI.`clock`,HI.`value`,HI.`valid`
			/***				FROM `hosts` H
			/***				INNER JOIN `items` I ON I.`hostid`=H.`hostid`
			/***				INNER JOIN `history` HI ON HI.`itemid`=I.`itemid`
			/***				WHERE H.`dns` LIKE 'BSW%' ORDER BY HI.`itemid` ASC $limitSql;";
					
					
							
				
				$get_history_rows_sql = "SELECT * FROM idCOsas LEFT JOIN `history` USING(`itemid`) ORDER BY idCOsas.`hostid` ASC $limitSql";
							
				$get_history_rows_result = $this->queryFetchColAssoc($get_history_rows_sql);
	
				if($get_history_rows_result) {
					$insert_history = "INSERT INTO `bm_history` ( `id_item`, `id_host`, `clock`, `value`, `valid`) VALUES ";
					
					$insert_value = array();
					
					foreach ($get_history_rows_result as $key => $value) {
						if(isset($items[$value['description']])){
							$idItem = $items[$value['description']];
						} else {
							$idItem = 'A';
						}
						if(is_numeric($idItem) && (is_numeric($value['value']))) {
							$insert_value[] = "($idItem, ".$value['hostid'].", ".$value['clock'].", '".$value['value']."', ".$value['valid'].")";
						}
					}
					
					$insert_final = $insert_history.join(',', $insert_value);
					
					$insert_history_result = $this->conexion->query($insert_final);
					
					if(!$insert_history_result) {
						$status = false;
					}

				}
				
	
			} */	
			
		} else {
			$status =false;
		}	
		if($status) {
			$this->setImport('end', 'history',$marcar,'ok');
			return true;
		} else {
			$this->setImport('end', 'history',$marcar,'error');
			exit;
		}

	}

	public function import_stat1()
	{
		//Get Id de plan
		
		$marcar = $this->setImport('start', 'stat1',$this->import_index);
		
		$select_sql = "SELECT  DISTINCT P.`id_plan`, P.`plan`
					FROM `bm_plan` P
					LEFT JOIN `bm_plan_groups` PG ON P.`id_plan`=PG.`id_plan`
					LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=PG.`groupid`
					WHERE HG.`type`='NEUTRALIDAD'";
					
		$select_result = $this->conexion->queryFetch($select_sql);
		
		if($select_result) {
		
			foreach ($select_result as $key => $value) {
				$planes[$value['plan']]	 = $value['id_plan'];
			}
			
			$select_sql = "SELECT DISTINCT I.`id_item`,I.`name`, I.`description` 
							FROM `bm_items`  I
							LEFT JOIN `bm_items_groups` IG ON IG.`id_item`=I.`id_item`
							LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=IG.`groupid`
							WHERE HG.`type`='NEUTRALIDAD';";
	
			$select_result = $this->conexion->queryFetch($select_sql);

			if($select_result) {
				
				foreach ($select_result as $key => $value) {
					$items[$value['description']] =  $value['id_item'];
				}
				
				$select_sql = "SELECT * FROM `bsw_stat1`";
										
				$select_result = $this->queryFetchColAssoc($select_sql);
				
				if($select_result) {
					
					//Limpiando tabla bm_stat2
					
					$truncate_sql = "TRUNCATE TABLE `bm_stat1`";
					$this->conexion->query($truncate_sql);
						
					foreach ($select_result as $key => $value) {
						if(isset($items[$value['description']]) && is_numeric($items[$value['description']])) {
							$value['id_item'] = $items[$value['description']];
							
							if(isset($planes[$value['plan']]) && is_numeric($planes[$value['plan']])) {
								
								$value['id_plan'] = $planes[$value['plan']];
								unset($value['plan']);
								
								$insert_value_start[] = $value;
							}
						}
					}
					
					foreach ($insert_value_start as $key => $value) {
						
						$insert_value[] = "('".join("','", $value)."')";
					
					}
					
					$insert_sql = "INSERT IGNORE INTO `bm_stat1` (`groupid`, `description`, `clock`, `exitosa`, `fallida`, `cumple`,`id_item`,`id_plan`)";
					$insert_sql .= " VALUES ".join(",", $insert_value);
					
					$insert_result = $this->conexion->query($insert_sql);
					
					if($insert_result) {
						$status = true;
					} else {
						$status = false;
					}
 					
				} else {
					$status = false;
				}				
				
			} else {
				$status = false;
			}
		} else {
			$status = false;
		}
		
		if($status) {
			$this->setImport('end', 'stat2',$marcar,'ok');
			return true;
		} else {
			$this->setImport('end', 'stat2',$marcar,'error');
			exit;
		}
	}

	public function import_stat2()
	{
		//Get Id de plan
		
		$marcar = $this->setImport('start', 'stat2',$this->import_index);
		
		$select_sql = "SELECT  DISTINCT P.`id_plan`, P.`plan`
					FROM `bm_plan` P
					LEFT JOIN `bm_plan_groups` PG ON P.`id_plan`=PG.`id_plan`
					LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=PG.`groupid`
					WHERE HG.`type`='NEUTRALIDAD'";
					
		$select_result = $this->conexion->queryFetch($select_sql);
		
		if($select_result) {
		
			foreach ($select_result as $key => $value) {
				$planes[$value['plan']]	 = $value['id_plan'];
			}
			
			$select_sql = "SELECT DISTINCT I.`id_item`,I.`name`, I.`description` 
							FROM `bm_items`  I
							LEFT JOIN `bm_items_groups` IG ON IG.`id_item`=I.`id_item`
							LEFT JOIN `bm_host_groups` HG ON HG.`groupid`=IG.`groupid`
							WHERE HG.`type`='NEUTRALIDAD';";
	
			$select_result = $this->conexion->queryFetch($select_sql);

			if($select_result) {
				
				foreach ($select_result as $key => $value) {
					$items[$value['description']] =  $value['id_item'];
				}
				
				$select_sql = "SELECT * FROM `bsw_stat3`";
										
				$select_result = $this->queryFetchColAssoc($select_sql);
				
				if($select_result) {
						
						//Limpiando tabla bm_stat2
						
						$truncate_sql = "TRUNCATE TABLE `bm_stat2`";
						$this->conexion->query($truncate_sql);
							
						foreach ($select_result as $key => $value) {
							if(isset($items[$value['description']]) && is_numeric($items[$value['description']])) {
								$value['id_item'] = $items[$value['description']];
								
								if(isset($planes[$value['plan']]) && is_numeric($planes[$value['plan']])) {
									
									$value['id_plan'] = $planes[$value['plan']];
									unset($value['plan']);
									
									$insert_value_start[] = $value;
								}
							}
						}
						
						foreach ($insert_value_start as $key => $value) {
							
							$insert_value[] = "('".join("','", $value)."')";
 							
 						}
						$insert_sql = "INSERT IGNORE INTO `bm_stat2` (`groupid`, `description`, `clock`, `vavg`, `vmin`, `vmax`, `std_samp`, `std`, `exitosa`, `fallida`, `intervalo`, `error`, `p5`, `p95`, `valid`, `p80`,`id_item`,`id_plan`)";
						$insert_sql .= " VALUES ".join(",", $insert_value);
						
						$insert_result = $this->conexion->query($insert_sql);
						
						if($insert_result) {
							$status = true;
						} else {
							$status = false;
						}
 					
				} else {
					$status = false;
				}

			} else {
				$status = false;
			}
			
		} else {
			$status = false;
		}
		
		if($status) {
			$this->setImport('end', 'stat2',$marcar,'ok');
			return true;
		} else {
			$this->setImport('end', 'stat2',$marcar,'error');
			exit;
		}
	}

	public function import_neutralidad()
	{
		
		$marcar = $this->setImport('start', 'neutralidad',$this->import_index);
		
		$val['200']=100;
		$val['205']=101;
		$val['210']=102;
		$val['211']=103;
		$val['212']=104;
		$val['213']=105;
		$val['214']=106;
		$val['220']=107;
		$val['221']=108;
		$val['222']=109;
		$val['223']=110;
		$val['224']=111;
		$val['230']=112;
		$val['231']=113;
		$val['232']=114;
		$val['240']=115;
		$val['241']=116;
		$val['300']=200;
		$val['305']=201;
		$val['310']=202;
		$val['311']=203;
		$val['312']=204;
		$val['313']=205;
		$val['314']=206;
		$val['320']=207;
		$val['321']=208;
		$val['322']=209;
		$val['323']=210;
		$val['324']=211;
		$val['330']=212;
		$val['331']=213;
		$val['332']=214;
		$val['340']=215;
		$val['341']=216;
		$val['400']=300;
		$val['405']=301;
		$val['410']=302;
		$val['411']=303;
		$val['412']=304;
		$val['413']=305;
		$val['414']=306;
		$val['420']=307;
		$val['421']=308;
		$val['422']=309;
		$val['423']=310;
		$val['424']=311;
		$val['430']=312;
		$val['431']=313;
		$val['432']=314;
		$val['440']=315;
		$val['441']=316;
				
		$select_sql = "select * from `bsw_neutralidad` WHERE `clock` IS NOT NULL";
								
		$select_result = $this->queryFetchColAssoc($select_sql);
		
		if($select_result) {
						
			$truncate_sql = "TRUNCATE TABLE `bm_neutralidad`";
			$this->conexion->query($truncate_sql);
				
			foreach ($select_result as $keyPlan => $planesValue) {
				unset($planesValue['item']);
				$idItem = (int)$planesValue['id'];
				unset($planesValue['id']);
				$planesValue['id_item'] = $val[$idItem];
				$insert_value_start[] = $planesValue;
			}
			
			foreach ($insert_value_start as $key => $value) {
				
				$insert_value[] = "('".join("','", $value)."')";
				
			}
			
			$insert_sql = "INSERT INTO `bm_neutralidad` (`clock`, `DSL1024`, `DSL1300`, `DSL512`, `WA512`, `WC1300`, `FTTH10Mbpssync`,`id_item`)";
			$insert_sql .= " VALUES ".join(",", $insert_value);
			
			$insert_result = $this->conexion->query($insert_sql);
			
			if($insert_result) {
				$status = true;
			} else {
				$status = false;
			}

		} else {
			$status = false;
		}
		
		if($status) {
			$this->setImport('end', 'neutralidad',$marcar,'ok');
			return true;
		} else {
			$this->setImport('end', 'neutralidad',$marcar,'error');
			exit;
		}
	}		
	function __destruct() {
		$marcar = $this->setImport('end', 'index',$this->import_index,'ok');
	}
}
?>